#!/bin/bash
set -e

export RUSTFLAGS="-C target-cpu=skylake"
cargo build --release
strip target/release/rust
rsync target/release/rust $ENDOR:skylake_langevin

# scp -r -oProxyJump=weissmann@endor.theo2.physik.uni-stuttgart.de weissmann@himalia.theo2.physik.uni-stuttgart.de:/data/weissmann/precious/* ~/Sync/data/from_itp2/

#!/bin/bash
set -e

if [ -d /home/weissmann ] ; then
cargo build --release
#cargo rustc --release -- -C target-cpu=znver1
strip target/release/rust
echo "itp2"
cp target/release/rust ~/hydra_langevin
else
#export RUSTFLAGS="-C target-cpu=znver1"
cargo build --release
strip target/release/rust
echo "at home"
rsync target/release/rust $ENDOR:hydra_langevin
fi

# scp -r -oProxyJump=weissmann@endor.theo2.physik.uni-stuttgart.de weissmann@hydra.theo2.physik.uni-stuttgart.de:/data/weissmann/precious/* ~/Sync/data/from_itp2/


# scp -r hydra:/data/weissmann/precious/* ~/Sync/data/from_itp2/

use crate::brüssel_force;
use crate::expr_string;
use crate::langevin::*;
use crate::limit_cycle::*;
use crate::rand::distributions::Distribution;
use crate::rand::Rng;
use crate::rand::SeedableRng;
use crate::BrüsselConf;
use crate::*;
use statrs::distribution::Normal;
use std::f64::consts::PI;
use std::fs::File;

pub fn mail_10_12_theta() {
    let Omega = 1e3;
    for b in [0., 1e-1, 1e-2, 1e-3] {
        for omega_i in -30..30 {
            let omega = omega_i as f64 * 0.1;
            let a = 1.;
            let mu = 1.;
            let Force =
                expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mail_10_12_b_{}_paromega_{:.1}.json", b, omega),
                Omega,
                false,
                [1., 0.],
                4000.,
                2_usize.pow(16),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[1].atan2(q[0]),
                vec![
                    ("mu".to_string(), mu),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                    ("a".to_string(), a),
                ],
            );
        }
    }
}

pub fn mail_10_12_x() {
    let Omega = 1e3;
    for b in [0., 1., 2., 1e-1, 1e-2, 1e-3] {
        for omega_i in -30..30 {
            let omega = omega_i as f64 * 0.1;
            let a = 1.;
            let mu = 1.;
            let Force =
                expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mail_10_12_x_b_{}_paromega_{:.1}.json", b, omega),
                Omega,
                false,
                [1., 0.],
                500.,
                2_usize.pow(14),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[0],
                vec![
                    ("mu".to_string(), mu),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                    ("a".to_string(), a),
                ],
            );
        }
    }
}

pub fn mail_10_12_x_x0() {
    let Omega = 1e3;
    for b in [0., 1., 2., 1e-1, 1e-2, 1e-3] {
        for omega_i in -30..30 {
            let omega = omega_i as f64 * 0.1;
            let a = 1.;
            let mu = 1.;
            let Force =
                expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mail_10_12_x_x0_b_{}_paromega_{:.1}.json", b, omega),
                Omega,
                false,
                [(mu / a).sqrt(), 0.],
                500.,
                2_usize.pow(14),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[0],
                vec![
                    ("mu".to_string(), mu),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                    ("a".to_string(), a),
                ],
            );
        }
    }
}

pub fn mail_19_01_x_x0() {
    let Omega = 1e3;
    for b in [0., 1., 2., 1e-1, 1e-2, 1e-3] {
        for mu_i in 0..21 {
            let mu = mu_i as f64 * 0.1;
            let omega = 1.;
            let a = 1.;
            let Force =
                expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mail_19_01_x_x0_b_{}_mu_{:.1}.json", b, mu),
                Omega,
                false,
                [(mu / a).sqrt(), 0.],
                500.,
                2_usize.pow(14),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[0],
                vec![
                    ("mu".to_string(), mu),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                    ("a".to_string(), a),
                ],
            );
        }
    }
}

pub fn mexican_hat_theta() {
    let Omega = 1e3;
    for b in [0., 1e-1, 1e-2, 1e-3] {
        for omega_i in -30..30 {
            let omega = omega_i as f64 * 0.1;
            let sigma = 1.;
            let Force = expr_string!(move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mexican_hat_b_{}_paromega_{:.1}.json", b, omega),
                Omega,
                false,
                [1., 0.],
                500.,
                2_usize.pow(14),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[1].atan2(q[0]),
                vec![
                    ("sigma".to_string(), sigma),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                ],
            );
        }
    }
}

pub fn mexican_hat_x() {
    let Omega = 1e3;
    let b = 0.;
    // /home/volker/Sync/data/from_itp2/mexican_hat_x_sigma_0_paromega_-3.0.json"
    for sigma in [0., 1., 2., 1e-1, 1e-2, 1e-3] {
        for omega_i in -30..30 {
            if sigma == 0. {
                // mexican_hat_force divides by sigma
                continue;
            }
            let omega = omega_i as f64 * 0.1;
            let Force = expr_string!(move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b));
            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("mexican_hat_x_sigma_{}_paromega_{:.1}.json", sigma, omega),
                Omega,
                false,
                [sigma * 3_f64.sqrt(), 0.],
                500.,
                2_usize.pow(14),
                Force,
                |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                |q, _t| q[0],
                vec![
                    ("sigma".to_string(), sigma),
                    ("omega".to_string(), omega),
                    ("b".to_string(), b),
                ],
            );
        }
    }
}

pub fn mexican_hat_verify() {
    let Omega = 1e3;
    let b = 0.;
    let omega = 2.9;
    let sigma = 1.;
    let Force = expr_string!(move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b));
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "verify_mexican_hat_r.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(14),
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[0].powi(2) + q[1].powi(2)).sqrt(),
        vec![
            ("sigma".to_string(), sigma),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

pub fn mexican_hat_meld_4() {
    let Omega = 1e3;
    let b = 0.;
    let omega = 2.9;
    let sigma = 1.;
    let sombrero = expr_string!(move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b));
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_sombrero_4.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(14),
        sombrero.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[0],
        vec![
            ("sigma".to_string(), sigma),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

pub fn mexican_hat_meld() {
    let Omega = 1e3;
    let b = 0.;
    let omega = 2.9;
    let sigma = 1. / 3_f64.sqrt();
    let sombrero = expr_string!(move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b));
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_sombrero_1.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(14),
        sombrero.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[1].atan2(q[0]),
        vec![
            ("sigma".to_string(), sigma),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_sombrero_3.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(10),
        sombrero.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[0],
        vec![
            ("sigma".to_string(), sigma),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "verify_mexican_hat_r.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(14),
        sombrero,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[0].powi(2) + q[1].powi(2)).sqrt(),
        vec![
            ("sigma".to_string(), sigma),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    let a = 1.;
    let mu = 1.;
    let cubic = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a));
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_sombrero_2.json",
        Omega,
        false,
        [1., 0.],
        500.,
        2_usize.pow(14),
        cubic,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[0].powi(2) + q[1].powi(2)).sqrt(),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
            ("a".to_string(), a),
        ],
    );
}

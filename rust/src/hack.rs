// #[inline(never)]
// #[no_mangle]
// fn get_num() -> i32 {
//     std::hint::black_box(23)
// }

// #[no_mangle]
// pub fn volkermarker(x: i32) -> i32 {
//     x + get_num()
// }

use crate::cubic_hopf_bifurcation_force;
use crate::cubic_hopf_bifurcation_force_a;
use crate::langevin::*;
use crate::misc::between_pm_pi;

use crate::rand::distributions::Distribution;
use rand::SeedableRng;
use serde::{Deserialize, Serialize};
use statrs::distribution::Normal;
use std::f64::consts::PI;
use std::fs::File;

pub fn back_vary_omega() {
    let Omega = 1e3;
    for b in [0., 0.5, 1., 2., 3.] {
        for omega in [-3., 0., 0.5, 1., 2., 3.] {
            for a in [0.5, 1., 2., 10.] {
                if omega == -3. && b != 3. {
                    continue;
                }

                let mu = a;

                let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(
                    q, mu, omega, b, a
                ));

                let theta_0: f64 = 0.;
                let omega_0 = omega + b * mu / a;
                save_ensemble_average(
                    StandardMulticore::<StandardEulerMaruyama>::default(),
                    &format!("back_paromega_{:.1}_b_{:.1}_a_{:.1}.json", omega, b, a),
                    Omega,
                    false,
                    [theta_0.cos(), theta_0.sin()],
                    500.,
                    2_usize.pow(12),
                    Force,
                    |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                    //q[1].atan2(q[0]) - theta_0,
                    |q, t| between_pm_pi(q[1].atan2(q[0]) - theta_0 - omega_0 * t).powi(2),
                    vec![
                        ("mu".to_string(), mu),
                        ("omega".to_string(), omega),
                        ("b".to_string(), b),
                        ("a".to_string(), a),
                    ],
                );
            }
        }
    }
}

pub fn prob_3m3() {
    let Omega = 1e3;
    let mu = 1.;
    let b = 3.;
    let omega = -3.;
    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    let theta_0: f64 = 0.;
    let omega_0 = omega + b * mu;

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_xdif.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 0.]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[1].atan2(q[0]).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_ydif.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[0., 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[1].atan2(q[0]).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_theta^2.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[1].atan2(q[0]).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_theta.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[1].atan2(q[0]),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_r.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| (q[0].powi(2) + q[1].powi(2)).sqrt(),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_r^2.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[0].powi(2) + q[1].powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "3m3_(r-1)^2.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        100.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| ((q[0].powi(2) + q[1].powi(2)).sqrt() - 1.).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

pub fn theta_backtravo() {
    let Omega = 1e3;
    let omega = 1.;
    let mu = 1.;
    let b = 1.;

    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    let theta_0: f64 = 0.;
    let omega_0 = omega + b * mu;
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "temp_back_theta_var.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        500.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| between_pm_pi(q[1].atan2(q[0]) - theta_0 - omega_0 * t).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "temp_theta_avg.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        500.,
        2_usize.pow(8),
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        //q[1].atan2(q[0]) - theta_0,
        |q, t| q[1].atan2(q[0]),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

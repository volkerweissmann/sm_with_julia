use crate::plot::*;
use crate::rand::distributions::Distribution;
use crate::*;
use plotters::prelude::*;
use rand::Rng;
use rand::SeedableRng;
use statrs::distribution::Normal;
use std::f64::NAN;
use std::marker::PhantomData;

fn sample_limit_cycle(
    rng: &mut rand::rngs::SmallRng,
    bc: BrüsselConf,
    lc0: [f64; 2],
    period: f64,
) -> [f64; 2] {
    let rand: f64 = rng.gen();
    let t = rand * period;
    let dt = 1e-3;
    let mut q = lc0;
    for u in 0..(t / dt) as usize {
        let old_q = q;
        let F = brüssel_force(&bc, q);
        q[0] += F[0] * dt;
        q[1] += F[1] * dt;
    }
    q
}

#[derive(Clone, Copy)]
pub struct BrüsselSampledEnsemble<S: SingleTrajectory> {
    bc: BrüsselConf,
    lc0: [f64; 2],
    period: f64,
    phantom: PhantomData<S>,
}

impl<S: SingleTrajectory> Multicore<2> for BrüsselSampledEnsemble<S> {
    fn multicore<A, B, C>(
        &self,
        Omega: f64,
        wrap_2pi: bool,
        _evil_x0: [f64; 2],
        N: usize,
        multi: usize,
        Force_func: A,
        diffusion_C: B,
        out_trafo: C,
        ensembles: usize,
        langevin_step_repeat: usize,
        trajectory_writeout_timestep: f64,
        sum_or_avg: &mut Vec<f64>,
    ) where
        A: Fn([f64; 2]) -> [f64; 2] + std::marker::Send + Copy,
        B: Fn([f64; 2]) -> [[f64; 2]; 2] + std::marker::Send + Copy,
        C: Fn([f64; 2], f64) -> f64 + std::marker::Send + Copy,
    {
        let mut probably_irrelevant = 0.;
        let mut pool = scoped_threadpool::Pool::new(num_cpus::get() as u32);
        println!("Starting multithreaded part");
        let (tx, rx) = std::sync::mpsc::channel();
        //let core = StandardEulerMaruyama {};
        let bc = self.bc;
        let lc0 = self.lc0;
        let period = self.period;
        pool.scoped(|scope| {
            for _ in 0..ensembles {
                let tx = tx.clone();
                scope.execute(move || {
                    let mut rng = rand::rngs::SmallRng::from_entropy();
                    let x0 = sample_limit_cycle(&mut rng, bc, lc0, period);
                    let single = S::simulate_single(
                        N,
                        trajectory_writeout_timestep,
                        Omega,
                        wrap_2pi,
                        x0,
                        langevin_step_repeat,
                        Force_func,
                        diffusion_C,
                        out_trafo,
                    );
                    tx.send((out_trafo(x0, 0.), single))
                        .expect("channel will be there waiting for the pool");
                });
            }
            rx.iter()
                .take(ensembles)
                .enumerate()
                .for_each(|(i, (ox0, single))| {
                    if i % 2_usize.pow(10) == 0 {
                        dbg!(i);
                    }
                    probably_irrelevant += ox0;
                    sum_or_avg
                        .iter_mut()
                        .zip(single.iter())
                        .for_each(|(sum, single)| *sum += ox0 * single);
                });
        });
        println!("Finished multithreaded part");

        dbg!(probably_irrelevant);
        vec_times_scalar(sum_or_avg, 1_f64 / multi as f64);
    }
}

#[derive(Clone, Copy)]
pub struct Brüsselxtx0mxtx0<S: SingleTrajectory> {
    bc: BrüsselConf,
    lc0: [f64; 2],
    period: f64,
    phantom: PhantomData<S>,
}

impl<S: SingleTrajectory> Multicore<2> for Brüsselxtx0mxtx0<S> {
    fn multicore<A, B, C>(
        &self,
        Omega: f64,
        wrap_2pi: bool,
        _evil_x0: [f64; 2],
        N: usize,
        multi: usize,
        Force_func: A,
        diffusion_C: B,
        out_trafo: C,
        ensembles: usize,
        langevin_step_repeat: usize,
        trajectory_writeout_timestep: f64,
        sum_or_avg: &mut Vec<f64>,
    ) where
        A: Fn([f64; 2]) -> [f64; 2] + std::marker::Send + Copy,
        B: Fn([f64; 2]) -> [[f64; 2]; 2] + std::marker::Send + Copy,
        C: Fn([f64; 2], f64) -> f64 + std::marker::Send + Copy,
    {
        sum_or_avg.iter().for_each(|x| assert!(*x == 0.));
        assert!(sum_or_avg.len() == N);
        assert!(multi == ensembles);

        let mut soa_xtx0 = vec![0.; N];
        let mut soa_xt = vec![0.; N];
        let mut soa_x0 = 0.;

        let mut pool = scoped_threadpool::Pool::new(num_cpus::get() as u32);
        println!("Starting multithreaded part");
        let (tx, rx) = std::sync::mpsc::channel();
        //let core = StandardEulerMaruyama {};
        let bc = self.bc;
        let lc0 = self.lc0;
        let period = self.period;

        pool.scoped(|scope| {
            for i in 0..ensembles {
                let tx = tx.clone();
                let mut rng = rand::rngs::SmallRng::from_entropy();
                scope.execute(move || {
                    let q0 = sample_limit_cycle(&mut rng, bc, lc0, period);
                    let single = S::simulate_single(
                        N,
                        trajectory_writeout_timestep,
                        Omega,
                        wrap_2pi,
                        q0,
                        langevin_step_repeat,
                        Force_func,
                        diffusion_C,
                        out_trafo,
                    );
                    if i % 2_usize.pow(10) == 0 {
                        dbg!(i);
                    }
                    tx.send((out_trafo(q0, 0.), single))
                        .expect("channel will be there waiting for the pool");
                });
            }
            rx.iter()
                .take(ensembles)
                .enumerate()
                .for_each(|(i, (x0, single))| {
                    if i % 2_usize.pow(10) == 0 {
                        dbg!(i);
                    }
                    soa_xtx0
                        .iter_mut()
                        .zip(single.iter())
                        .for_each(|(sum, single)| *sum += x0 * single);
                    soa_xt
                        .iter_mut()
                        .zip(single.iter())
                        .for_each(|(sum, single)| *sum += single);
                    soa_x0 += x0;
                });
        });
        println!("Finished multithreaded part");

        sum_or_avg
            .iter_mut()
            .zip(soa_xtx0.iter())
            .for_each(|(total, xtx0)| *total += xtx0);
        sum_or_avg
            .iter_mut()
            .zip(soa_xt.iter())
            .for_each(|(total, xt)| *total -= xt * soa_x0 / multi as f64);

        vec_times_scalar(sum_or_avg, 1_f64 / multi as f64);
    }
}

pub fn brüsel_sampled_ensemble() {
    let Omega = 10000.;
    let mu = 9.0;
    let bc = BrüsselConf::like_basile(mu);
    let (lc0, period) = find_point_on_limit_cycle(bc).unwrap();
    let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

    let core = BrüsselSampledEnsemble {
        bc,
        lc0,
        period,
        phantom: PhantomData::<StandardEulerMaruyama>::default(),
    };

    save_ensemble_average(
        core,
        &format!("brüssel_sampled_mu_{}.json", mu),
        Omega,
        false,
        [NAN, NAN],
        1000.,
        2_usize.pow(20),
        Force,
        |q| brüssel_C_benedikt(&bc, q),
        |x, _t| x[0],
        vec![("mu".to_string(), mu)],
    );
}

pub fn brüssel_xtx0mxtx0() {
    for Omega in [1e2, 1e3, 1e4] {
        let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
        deltamus.push(6.0); // Style: This is terrible
        deltamus.push(6.5);
        deltamus.push(7.0);
        deltamus.push(7.5);
        deltamus.push(8.0);
        for mu in deltamus {
            let bc = BrüsselConf::like_basile(mu);
            let temp = find_point_on_limit_cycle(bc);
            if temp.is_none() {
                println!(
                "No limit cycle found for deltamu = {}, so no langevin simulation will be done.",
                mu
            );
                continue;
            }
            let (lc0, period) = temp.unwrap();
            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let core = Brüsselxtx0mxtx0 {
                bc,
                lc0,
                period,
                phantom: PhantomData::<StandardEulerMaruyama>::default(),
            };

            save_ensemble_average(
                core,
                &format!(
                    "brüssel_xtx0mxtx0_ct_long_Omega_{}_mu_{:.1}.json",
                    Omega, mu
                ),
                Omega,
                false,
                [NAN, NAN],
                Omega,
                2_usize.pow(20),
                Force.clone(),
                |q| brüssel_C_benedikt(&bc, q),
                |x, _t| x[0],
                vec![("mu".to_string(), mu)],
            );

            save_ensemble_average(
                core,
                &format!(
                    "tang_brüssel_xtx0mxtx0_ct_long_Omega_{}_mu_{:.1}.json",
                    Omega, mu
                ),
                Omega,
                false,
                [NAN, NAN],
                Omega,
                2_usize.pow(20),
                Force,
                |q| tang_brüssel_C(&bc, q),
                |x, _t| x[0],
                vec![("mu".to_string(), mu)],
            );
        }
    }
}

pub fn show_sampling() {
    let mu = 5.3;
    let bc = BrüsselConf::like_basile(mu);
    let (lc0, period) = find_point_on_limit_cycle(bc).unwrap();
    let mut rng = rand::rngs::SmallRng::from_entropy();

    let mut random_points = Vec::new();
    for u in 0..2_usize.pow(14) {
        let q0 = sample_limit_cycle(&mut rng, bc, lc0, period);
        random_points.push((q0[0], q0[1]));
    }

    scatter_hist(random_points).unwrap();
}

// #[inline(never)]
// #[no_mangle]
// fn get_num() -> i32 {
//     std::hint::black_box(23)
// }

// #[no_mangle]
// pub fn volkermarker(x: i32) -> i32 {
//     x + get_num()
// }

use crate::rand::distributions::Distribution;
use rand::SeedableRng;
use serde::{Deserialize, Serialize};
use statrs::distribution::Normal;
use std::f64::consts::PI;
use std::fs::File;
use std::marker::PhantomData;

pub trait SingleTrajectory: Copy {
    fn simulate_single<C1, C2, C3, const DIM: usize>(
        N: usize,
        trajectory_writeout_timestep: f64,
        Omega: f64,
        wrap_2pi: bool,
        x0: [f64; DIM],
        langevin_step_repeat: usize,
        Force: C1,
        diffusion_C: C2,
        out_trafo: C3,
    ) -> Vec<f64>
    where
        C1: Fn([f64; DIM]) -> [f64; DIM],
        C2: Fn([f64; DIM]) -> [[f64; DIM]; DIM],
        C3: Fn([f64; DIM], f64) -> f64;
}

#[derive(Clone, Copy, Default)]
pub struct StandardEulerMaruyama {}

impl SingleTrajectory for StandardEulerMaruyama {
    fn simulate_single<C1, C2, C3, const DIM: usize>(
        N: usize,
        trajectory_writeout_timestep: f64,
        Omega: f64,
        wrap_2pi: bool,
        x0: [f64; DIM],
        langevin_step_repeat: usize,
        Force: C1,
        diffusion_C: C2,
        out_trafo: C3,
    ) -> Vec<f64>
    where
        C1: Fn([f64; DIM]) -> [f64; DIM],
        C2: Fn([f64; DIM]) -> [[f64; DIM]; DIM],
        C3: Fn([f64; DIM], f64) -> f64,
    {
        let mut rng = rand::rngs::SmallRng::from_entropy();
        let n = Normal::new(0.0, 1.0).unwrap();

        let space_period = 2. * PI;

        let mut ret = Vec::with_capacity(N);

        let dt = trajectory_writeout_timestep / langevin_step_repeat as f64;

        //let mut t = 0.;
        let mut x = x0;
        for step in 0..N {
            ret.push(out_trafo(x, dt * langevin_step_repeat as f64 * step as f64));

            // let U_1: f32 = rng.gen();
            // let U_2: f32 = rng.gen();
            // // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
            // let Z_0 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).cos();
            // //let Z_1 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).sin();
            // let W = Z_0;

            for _ in 0..langevin_step_repeat {
                let F = Force(x);
                let C = diffusion_C(x);
                let mut zetas = [0.; DIM];
                for j in 0..DIM {
                    zetas[j] = n.sample(&mut rng);
                }
                for i in 0..DIM {
                    // The optimizer should unroll this loop and remove all bounds check. Otherwise, performance is fucked.
                    // C=sqrt(2Q)
                    // Q = 1/2 C^2
                    let f = F[i] * dt;
                    x[i] += f;
                    for j in 0..DIM {
                        // The optimizer should unroll this loop and remove all bounds check. Otherwise, performance is fucked.
                        let g = C[i][j] * (dt / Omega).sqrt();
                        x[i] += g * zetas[j];
                    }
                    if wrap_2pi && x[i] >= space_period {
                        x[i] -= space_period;
                    } else if wrap_2pi && x[i] < 0. {
                        x[i] += space_period;
                    }
                }
            }
        }
        ret
    }
}

// The difference between version 3 and version 4 is that in version 3, simulate_single_trajectory is broken if diffusion_C is not diagonal.
#[derive(Serialize, Deserialize)]
pub struct Output {
    version: i32,
    trajectory_writeout_timestep: f64,
    langevin_step_repeat: usize,
    Omega: f64, // used to be omega in version 1
    multi: usize,
    avg: Vec<f64>,
    Force: String,              // used to be &'static str in version 2
    coeffs: Vec<(String, f64)>, // used to be &'static str in version 2
}

fn add_assign<const N: usize>(a: &mut [f64; N], b: &[f64; N]) {
    a.iter_mut().zip(b.iter()).for_each(|(a, b)| *a += *b);
}

fn nearly_equal(a: f64, b: f64) -> bool {
    (a - b).abs() <= (a.abs() + b.abs()) * 1e-6
}

// todo style why do I have to implement something like this myself
pub fn vec_times_scalar(vec: &mut [f64], a: f64) {
    vec.iter_mut().for_each(|v| *v *= a);
}

pub trait Multicore<const DIM: usize> {
    fn multicore<A, B, C>(
        &self,
        Omega: f64,
        wrap_2pi: bool,
        x0: [f64; DIM],
        N: usize,
        multi: usize,
        Force_func: A,
        diffusion_C: B,
        out_trafo: C,
        ensembles: usize,
        langevin_step_repeat: usize,
        trajectory_writeout_timestep: f64,
        sum_or_avg: &mut Vec<f64>,
    ) where
        A: Fn([f64; DIM]) -> [f64; DIM] + std::marker::Send + Copy,
        B: Fn([f64; DIM]) -> [[f64; DIM]; DIM] + std::marker::Send + Copy,
        C: Fn([f64; DIM], f64) -> f64 + std::marker::Send + Copy;
}

#[derive(Default)]
pub struct StandardMulticore<S: SingleTrajectory> {
    phantom: PhantomData<S>,
}

impl<S: SingleTrajectory, const DIM: usize> Multicore<DIM> for StandardMulticore<S> {
    fn multicore<A, B, C>(
        &self,
        Omega: f64,
        wrap_2pi: bool,
        x0: [f64; DIM],
        N: usize,
        multi: usize,
        Force_func: A,
        diffusion_C: B,
        out_trafo: C,
        ensembles: usize,
        langevin_step_repeat: usize,
        trajectory_writeout_timestep: f64,
        sum_or_avg: &mut Vec<f64>,
    ) where
        A: Fn([f64; DIM]) -> [f64; DIM] + std::marker::Send + Copy,
        B: Fn([f64; DIM]) -> [[f64; DIM]; DIM] + std::marker::Send + Copy,
        C: Fn([f64; DIM], f64) -> f64 + std::marker::Send + Copy,
    {
        let mut pool = scoped_threadpool::Pool::new(num_cpus::get() as u32);
        println!("Starting multithreaded part");
        let (tx, rx) = std::sync::mpsc::channel();
        //let core = StandardEulerMaruyama {};
        pool.scoped(|scope| {
            for i in 0..ensembles {
                let tx = tx.clone();
                scope.execute(move || {
                    let single = S::simulate_single(
                        N,
                        trajectory_writeout_timestep,
                        Omega,
                        wrap_2pi,
                        x0,
                        langevin_step_repeat,
                        Force_func,
                        diffusion_C,
                        out_trafo,
                    );
                    if i % 2_usize.pow(10) == 0 {
                        dbg!(i);
                    }
                    tx.send(single)
                        .expect("channel will be there waiting for the pool");
                });
            }
            rx.iter()
                .take(ensembles)
                .enumerate()
                .for_each(|(i, single)| {
                    if i % 2_usize.pow(10) == 0 {
                        dbg!(i);
                    }
                    sum_or_avg
                        .iter_mut()
                        .zip(single.iter())
                        .for_each(|(sum, single)| *sum += single);
                });
        });
        println!("Finished multithreaded part");

        vec_times_scalar(sum_or_avg, 1_f64 / multi as f64);
    }
}

pub fn save_ensemble_average<S, A, B, C, const DIM: usize>(
    core: S,
    filename: &str,
    Omega: f64,
    wrap_2pi: bool,
    x0: [f64; DIM],
    time: f64,
    multi: usize,
    Force: (A, String),
    diffusion_C: B,
    out_trafo: C,
    coeffs: Vec<(String, f64)>,
) where
    S: Multicore<DIM>,
    A: Fn([f64; DIM]) -> [f64; DIM] + std::marker::Send + Copy,
    B: Fn([f64; DIM]) -> [[f64; DIM]; DIM] + std::marker::Send + Copy,
    C: Fn([f64; DIM], f64) -> f64 + std::marker::Send + Copy,
{
    let (Force_func, Force_str) = Force;
    let trajectory_writeout_timestep = 1. / 100_f64;
    let langevin_step_repeat = 10;
    let N = (time / trajectory_writeout_timestep).ceil() as usize;

    let preferred_path = "/home/volker/Sync/data/from_itp2";
    let outdir = if std::path::Path::new(preferred_path).exists() {
        preferred_path
    } else {
        "/data/weissmann/precious"
    };
    let outpath = format!("{}/{}", outdir, filename);
    let outpath = std::path::Path::new(&outpath);
    let mut sum_or_avg = vec![0.; N];
    let mut ensembles = multi;
    if outpath.exists() {
        let olddata: Result<Output, _> = serde_json::from_reader(File::open(outpath).unwrap());
        if let Ok(olddata) = olddata {
            let equal_time = olddata.avg.len() == N;
            let equal_coeffs = olddata
                .coeffs
                .iter()
                .zip(coeffs.iter())
                .all(|(a, b)| a.0 == b.0 && nearly_equal(a.1, b.1));
            let equal_trajectory_writeout_timestep =
                olddata.trajectory_writeout_timestep == trajectory_writeout_timestep;
            let equal_langevin_step_repeat = olddata.langevin_step_repeat == langevin_step_repeat;
            let equal_Omega = olddata.Omega == Omega;
            let equal_Force = olddata.Force == Force_str;

            #[allow(clippy::float_cmp)]
            let partially_equal = equal_time
                && equal_coeffs
                && equal_trajectory_writeout_timestep
                && equal_langevin_step_repeat
                && equal_Omega
                && equal_Force;
            // todo: wrap_2pi needs to be compared too
            // todo: out_travo needs to be compared too
            // GREP_MARKER_READ_OUTPUT
            // See GREP_MARKER_WRITE_OUTPUT
            if partially_equal && olddata.multi == multi {
                println!("{} is already up to date", outpath.to_str().unwrap());
                return;
            } else if partially_equal && olddata.multi > multi {
                println!(
                    "{} is already more than up to date",
                    outpath.to_str().unwrap()
                );
                return;
            } else if !partially_equal {
                println!("{} holds different data:", outpath.to_str().unwrap());
                println!(
                    "{} vs. {}\n{} vs. {}\n{} vs. {}\n{} vs. {}\n{} vs. {}\n{} vs. {}\n{:#?} vs. {:#?}\n",
                    olddata.trajectory_writeout_timestep,
                    trajectory_writeout_timestep,
                    olddata.avg.len(),
                    N,
                    olddata.langevin_step_repeat,
                    langevin_step_repeat,
                    olddata.Omega,
                    Omega,
                    olddata.multi,
                    multi,
                    olddata.Force,
                    Force_str,
                    olddata.coeffs,
                    coeffs
                );
                if !equal_time {
                    println!("time: {} vs {}", olddata.avg.len(), N);
                }
                if !equal_coeffs {
                    println!("coeffs: {:#?} vs {:#?}", olddata.coeffs, coeffs);
                }
                if !equal_trajectory_writeout_timestep {
                    println!(
                        "trajectory_writeout_timestep: {} vs {}",
                        olddata.trajectory_writeout_timestep, trajectory_writeout_timestep
                    );
                }
                if !equal_langevin_step_repeat {
                    println!(
                        "langevin_step_repeat: {} vs {}",
                        olddata.langevin_step_repeat, langevin_step_repeat
                    );
                }
                if !equal_Omega {
                    println!("Omega: {} vs {}", olddata.Omega, Omega);
                }
                if !equal_Force {
                    println!("Force: {} vs {}", olddata.Force, Force_str);
                }
                return;
            }
            assert!(partially_equal && olddata.multi < multi);
            let additional_ensembles = multi - olddata.multi;
            println!(
                "{} needs an additional {} ensembles",
                outpath.to_str().unwrap(),
                additional_ensembles
            );
            ensembles = additional_ensembles;
            sum_or_avg = olddata.avg;
            vec_times_scalar(&mut sum_or_avg, olddata.multi as f64);
        } else {
            println!("warning: couldn't parse {}", outpath.display());
        }
    } else {
        println!("{} does not already exist", outpath.to_str().unwrap());
    }

    core.multicore(
        Omega,
        wrap_2pi,
        x0,
        N,
        multi,
        Force_func,
        diffusion_C,
        out_trafo,
        ensembles,
        langevin_step_repeat,
        trajectory_writeout_timestep,
        &mut sum_or_avg,
    );

    // GREP_MARKER_WRITE_OUTPUT:
    // Don't change this without looking at GREP_MARKER_READ_OUTPUT
    let output = Output {
        version: 4,
        trajectory_writeout_timestep,
        langevin_step_repeat,
        Omega,
        multi,
        avg: sum_or_avg,
        Force: Force_str,
        coeffs,
    };

    let num_tries = 5;
    for i in 0..num_tries {
        let outfile = File::create(outpath);
        if let Ok(outfile) = outfile {
            serde_json::to_writer(&outfile, &output).unwrap();
            break;
        }
        if i == num_tries - 1 {
            println!(
                "unable to save data to {}, discarding data",
                outpath.display()
            );
        } else {
            println!(
                "failed to save data to {}, trying again in 5 seconds...",
                outpath.display()
            );
            std::thread::sleep(std::time::Duration::from_secs(5));
        }
    }
}

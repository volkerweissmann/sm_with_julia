use plotters::prelude::*;

use rand::SeedableRng;
use rand_distr::{Distribution, Normal};
use rand_xorshift::XorShiftRng;

pub fn scatter_hist(random_points: Vec<(f64, f64)>) -> Result<(), Box<dyn std::error::Error>> {
    dbg!();
    let xmin = random_points
        .iter()
        .map(|(x, y)| x)
        .fold(f64::INFINITY, |a, &b| a.min(b));
    let xmax = random_points
        .iter()
        .map(|(x, y)| x)
        .fold(f64::NEG_INFINITY, |a, &b| a.max(b));
    let ymin = random_points
        .iter()
        .map(|(x, y)| y)
        .fold(f64::INFINITY, |a, &b| a.min(b));
    let ymax = random_points
        .iter()
        .map(|(x, y)| y)
        .fold(f64::NEG_INFINITY, |a, &b| a.max(b));
    dbg!();
    dbg!(xmin, xmax);
    dbg!(ymin, ymax);

    let OUT_FILE_NAME = "out.png";
    let root = BitMapBackend::new(OUT_FILE_NAME, (1024, 768)).into_drawing_area();
    root.fill(&WHITE)?;

    let areas = root.split_by_breakpoints([1024 - 200], [200]);

    let mut x_hist_ctx = ChartBuilder::on(&areas[0])
        .y_label_area_size(40)
        .build_cartesian_2d(
            (xmin..xmax).step(0.01).use_round().into_segmented(),
            0..200 * 2_usize.pow(4),
        )?;
    let mut y_hist_ctx = ChartBuilder::on(&areas[3])
        .x_label_area_size(40)
        .build_cartesian_2d(0..50 * 2_usize.pow(4), (ymin..ymax).step(0.01).use_round())?;

    let mut scatter_ctx = ChartBuilder::on(&areas[2])
        .x_label_area_size(40)
        .y_label_area_size(40)
        .build_cartesian_2d(xmin..xmax, ymin..ymax)?;
    scatter_ctx
        .configure_mesh()
        .disable_x_mesh()
        .disable_y_mesh()
        .draw()?;
    scatter_ctx.draw_series(
        random_points
            .iter()
            .map(|(x, y)| Circle::new((*x, *y), 1, RED.filled())),
    )?;
    let x_hist = Histogram::vertical(&x_hist_ctx)
        .style(BLUE.filled())
        .margin(0)
        .data(random_points.iter().map(|(x, _)| (*x, 1)));
    let y_hist = Histogram::horizontal(&y_hist_ctx)
        .style(BLUE.filled())
        .margin(0)
        .data(random_points.iter().map(|(_, y)| (*y, 1)));
    x_hist_ctx.draw_series(x_hist)?;
    y_hist_ctx.draw_series(y_hist)?;

    // To avoid the IO failure being ignored silently, we manually call the present function
    root.present().expect("Unable to write result to file, please make sure 'plotters-doc-data' dir exists under current dir");
    println!("Result has been saved to {}", OUT_FILE_NAME);

    Ok(())
}

use crate::linalg::*;
use crate::*;

pub fn project_diffusion(C2d: [[f64; 2]; 2], mut e: [f64; 2]) -> f64 {
    let norm = abs(e);
    e[0] /= norm;
    e[1] /= norm;

    let c0 = C2d[0][0] * e[0] + C2d[1][0] * e[1];
    let c1 = C2d[0][1] * e[0] + C2d[1][1] * e[1];

    (c0.powi(2) + c1.powi(2)).sqrt()
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::distributions::Distribution;
    use rand::SeedableRng;
    use statrs::distribution::Normal;

    #[test]
    fn verify_diffusion_projection() {
        let Omega = 10.;
        let dt: f64 = 1e-3;
        let q = [1.3, 2.1];
        let deltamu = 8.;
        let F = [2., 1.];
        let bc = BrüsselConf::like_basile(deltamu);

        let F = [0.26415221620399043, -0.9917593953452473];
        let q = [1.1254499059039325, 2.1435802956779444];
        let dt: f64 = 1.;
        let Omega = 1.;

        let mut rng = rand::rngs::SmallRng::from_entropy();
        let n = Normal::new(0.0, 1.0).unwrap();

        let C = brüssel_C_benedikt(&bc, q);
        let its = 1000000;

        let mut sum_a = 0.;
        for _ in 0..its {
            let mut x = [0.; 2];
            let mut zetas = [0.; 2];
            for j in 0..2 {
                zetas[j] = n.sample(&mut rng);
            }
            for i in 0..2 {
                for j in 0..2 {
                    let g = C[i][j] * (dt / Omega).sqrt();
                    x[i] += g * zetas[j];
                }
            }
            let proj = scalar_prod(x, F) / abs(F);
            sum_a += proj.powi(2);
        }

        let mut sum_b = 0.;
        for _ in 0..its {
            let mut x = 0.;
            let zetas = n.sample(&mut rng);

            let g = project_diffusion(C, F) * (dt / Omega).sqrt();
            x += g * zetas;

            sum_b += x.powi(2);
        }

        let mut sum_c = 0.;

        let x = q[0];
        let y = q[1];
        let sigmas = [
            [(bc.k1p * bc.A + bc.k1m * x).sqrt(), 0.],
            [0., (bc.k2p * bc.B + bc.k2m * y).sqrt()],
            [
                (bc.k3p * x.powi(2) * y + bc.k3m * x.powi(3)).sqrt(),
                -(bc.k3p * x.powi(2) * y + bc.k3m * x.powi(3)).sqrt(),
            ],
        ];

        for _ in 0..its {
            let mut x = [0.; 2];

            for sigma in sigmas {
                let zeta = n.sample(&mut rng);
                x[0] += sigma[0] * (dt / Omega).sqrt() * zeta;
                x[1] += sigma[1] * (dt / Omega).sqrt() * zeta;
            }

            let proj = scalar_prod(x, F) / abs(F);
            sum_c += proj.powi(2);
        }

        let effective_C = |sum: f64| -> f64 { (sum / its as f64).sqrt() };
        dbg!(effective_C(sum_a));
        dbg!(effective_C(sum_b));
        dbg!(effective_C(sum_c));
        dbg!(sum_a, sum_b, sum_c);

        assert!((1. - sum_b / sum_a).abs() < 0.03);
        assert!((1. - sum_c / sum_a).abs() < 0.03);
    }
}

pub fn calc_1D_int() {
    let deltamu = 8.0;
    let bc = BrüsselConf::like_basile(deltamu);
    let stat = stationary_point(bc);

    dbg!(rust_theta_travo([1., 2.], [3., 4.]));

    let (q0, period) = find_point_on_limit_cycle(bc).unwrap();
    let dt = 1e-3;

    let mut q = q0;

    let mut periodint = 0.;
    let mut otherint = 0.;

    loop {
        let old_q = q;
        let F = brüssel_force(&bc, q);
        q[0] += F[0] * dt;
        q[1] += F[1] * dt;
        if q == old_q {
            // q == old_q means that F is near 0 and q is therefore the
            // stationary point. The stationary point seems to be attractive,
            // because we started in [0., 0.] and ended up here. So there is
            // probably no limit cycle.
            // But this is only reached if find_point_on_limit_cycle returns Some(_).
            unreachable!();
        }

        let dist = ((old_q[0] - q[0]).powi(2) + (old_q[1] - q[1]).powi(2)).sqrt();
        periodint += dist / abs(F);

        let C2d = brüssel_C_benedikt(&bc, q);
        let C = project_diffusion(C2d, F);
        let Q = C.powi(2) / 2.;

        otherint += dist * Q / abs(F).powi(3);

        let theta1 = rust_theta_travo(stat, old_q);
        let theta2 = rust_theta_travo(stat, q);
        let target = 0.;
        // theta is decreasing
        if theta1 > target && theta2 < target {
            let error = (q0[0] - q[0]).powi(2) + (q0[1] - q[1]).powi(2);
            assert!(error < 1e-6);
            break;
        }
    }

    dbg!(periodint);
    dbg!(otherint);
    let omega = 2. * PI / periodint;
    let gamma = (2. * PI).powi(2) * otherint / periodint.powi(3);
    dbg!(omega, 1. / gamma, omega / gamma);
    assert!((period - periodint).abs() < 1e-9);
}

use crate::expr_string;
use crate::langevin::*;

fn limit_line_func(q: [f64; 2], mu: f64, omega: f64, b: f64, a: f64) -> [f64; 2] {
    let x = q[0];
    let y = q[1];

    let r0 = (mu / a).sqrt();

    let dxdt = -2. * mu * x;
    let dydt = omega + b * r0.powi(2) + x * (b * 2. * r0);

    [dxdt, dydt]
}

pub fn limit_line() {
    let Omega = 1e3;
    for b in [0., 0.5, 1., 2., 3.] {
        for omega in [-3., 0., 0.5, 1., 2., 3.] {
            for a in [0.5, 1., 2., 10.] {
                if omega == -3. && b != 3. {
                    continue;
                }
                let mu = a;

                let Force = expr_string!(move |q: [f64; 2]| limit_line_func(q, mu, omega, b, a));

                let omega_0 = omega + b * mu / a;
                save_ensemble_average(
                    StandardMulticore::<StandardEulerMaruyama>::default(),
                    &format!("line_paromega_{:.1}_b_{:.1}_a_{:.1}.json", omega, b, a),
                    Omega,
                    false,
                    [0., 0.],
                    500.,
                    2_usize.pow(12),
                    Force,
                    |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                    //q[1].atan2(q[0]) - theta_0,
                    |q, t| (q[1] - omega_0 * t).powi(2),
                    vec![
                        ("mu".to_string(), mu),
                        ("omega".to_string(), omega),
                        ("b".to_string(), b),
                        ("a".to_string(), a),
                    ],
                );
            }
        }
    }
}

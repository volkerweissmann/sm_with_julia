use crate::brüssel_force;
use crate::expr_string;
use crate::langevin::*;
use crate::limit_cycle::*;
use crate::misc::*;
use crate::rand::distributions::Distribution;
use crate::rand::Rng;
use crate::rand::SeedableRng;
use crate::BrüsselConf;
use crate::*;
use statrs::distribution::Normal;
use std::f64::consts::PI;
use std::fs::File;

fn cubic_hopf_meld() {
    let Omega = 1e3;
    let omega = 1.;
    let mu = 1.;
    let b = 1.;
    let multi = 2_usize.pow(16);
    let time = 1000.;

    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));
    // let Force_jt =
    //     expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force_just_theta(q, mu, omega, b));

    let Force_1d = expr_string!(move |_: [f64; 1]| [omega + b]);

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_theta.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[1].atan2(q[0]),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_1d.json",
        Omega,
        true,
        [PI],
        time,
        multi,
        Force_1d,
        |_| [[2_f64.sqrt()]],
        |q, _t| q[0],
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_r.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[0].powi(2) + q[1].powi(2)).sqrt(),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_x.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[0],
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_y.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[1],
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

fn sin_0() {
    for omega in [1e2, 1e3, 1e4] {
        let a = 0_f64;
        let multi = 2_usize.pow(18);
        let time = 2. * PI * 400.;
        let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("meld_sin_{}.json", omega),
            omega,
            true,
            [0_f64],
            time,
            multi,
            Force,
            |_| [[2_f64.sqrt()]],
            |q, _t| q[0],
            vec![("a".to_string(), a)],
        );
    }
}

fn melding() {
    for a in [0.5, 1., 2., 3.] {
        let Omega = 1e3;
        let multi = 2_usize.pow(14);
        let time = 400.;
        let Force = expr_string!(move |_: [f64; 1]| [a]);
        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("meld_a_{}.json", a),
            Omega,
            true,
            [0_f64],
            time,
            multi,
            Force,
            |_| [[2_f64.sqrt()]],
            |q, _t| q[0],
            vec![("a".to_string(), a)],
        );
    }
    for a in [2.] {
        let Omega = 1e4;
        let multi = 2_usize.pow(16);
        let time = 1000.;
        let Force = expr_string!(move |_: [f64; 1]| [a]);
        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("meld_2_a_{}.json", a),
            Omega,
            true,
            [0_f64],
            time,
            multi,
            Force,
            |_| [[2_f64.sqrt()]],
            |q, _t| q[0],
            vec![("a".to_string(), a)],
        );
    }

    for a in [2.] {
        let Omega = 1e4;
        let multi = 2_usize.pow(16);
        let time = 10000.;
        let Force = expr_string!(move |_: [f64; 1]| [a]);
        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("meld_3_a_{}.json", a),
            Omega,
            true,
            [0_f64],
            time,
            multi,
            Force,
            |_| [[2_f64.sqrt()]],
            |q, _t| q[0],
            vec![("a".to_string(), a)],
        );
    }
}

fn long_x() {
    let Omega = 1e3;
    let omega = 1.;
    let mu = 1.;
    let b = 1.;
    let multi = 2_usize.pow(20);
    let time = 1e4;

    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_long_x.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[0],
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

fn long_theta() {
    let Omega = 1e3;
    let omega = 1.;
    let mu = 1.;
    let b = 1.;
    let multi = 2_usize.pow(20);
    let time = 2500.;

    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "meld_long_theta.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[1].atan2(q[0]),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

fn verify_1() {
    let Omega = 1e2;
    let multi = 2_usize.pow(10);
    let time = 1e3;

    let Force = expr_string!(move |_: [f64; 2]| [0., 0.]);

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "verify_1.json",
        Omega,
        false,
        [0., 0.],
        time,
        multi,
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[0].powi(2),
        vec![],
    );
}

fn free_theta() {
    let Omega = 1e3;
    let omega = 0.;
    let mu = 1.;
    let b = 0.;
    let multi = 2_usize.pow(12);
    let time = 1000.;

    // b and omega are 0, so there is no tangential force
    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    let theta_0: f64 = 1.;
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "temp_free_theta_var.json",
        Omega,
        false,
        [theta_0.cos(), theta_0.sin()],
        500.,
        2_usize.pow(12),
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[1].atan2(q[0]) - theta_0).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "verify_free_theta_avg.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force.clone(),
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| q[1].atan2(q[0]),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "verify_free_theta_var.json",
        Omega,
        false,
        [1., 0.],
        2000.,
        2_usize.pow(16),
        Force,
        |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
        |q, _t| (q[1].atan2(q[0])).powi(2),
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

fn different_theta_diff() {
    for theta_0 in [-1.2_f64, -2., 2.3] {
        let Omega = 1e3;
        let omega = 0.;
        let mu = 1.;
        let b = 0.;
        let multi = 2_usize.pow(12);
        let time = 200.;

        // b and omega are 0, so there is no tangential force
        let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("free_theta_{}_var.json", theta_0),
            Omega,
            false,
            [theta_0.cos(), theta_0.sin()],
            500.,
            2_usize.pow(12),
            Force,
            |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
            |q, _t| between_pm_pi(q[1].atan2(q[0]) - theta_0).powi(2),
            vec![
                ("mu".to_string(), mu),
                ("omega".to_string(), omega),
                ("b".to_string(), b),
            ],
        );
    }
}

pub fn curwork() {
    // different_theta_diff();
    // verify_1();
    // long_theta();
    // free_theta();
    // hack::theta_backtravo();
    limit_line::limit_line();
    //hack::back_vary_omega();
    //hack::prob_3m3();
}

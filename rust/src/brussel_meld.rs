use crate::brüssel_C_maybe_wrong;
use crate::brüssel_force;
use crate::expr_string;
use crate::langevin::*;
use crate::limit_cycle::*;
use crate::rand::distributions::Distribution;
use crate::rand::Rng;
use crate::rand::SeedableRng;
use crate::BrüsselConf;
use statrs::distribution::Normal;
use std::f64::consts::PI;
use std::fs::File;

fn brüssel_projected() -> Vec<f64> {
    let mu = 8.;
    let mut inp: LimitCycleShape = serde_json::from_reader(
        File::open(format!("/data/weissmann/brüssel_shape_mu_{:.1}.json", mu)).unwrap(),
    )
    .unwrap();
    inp.fix_format();

    let max_x = inp
        .shape
        .iter()
        .enumerate()
        .max_by(|(_, q), (_, b)| q.0.partial_cmp(&b.0).expect("nan"))
        .unwrap();

    let omega = 10000.;
    let multi = 2_usize.pow(13);
    let time = 100.;

    let mut period = 0.;
    let mut otherint = 0.;
    for i in 0..inp.theta.len() - 1 {
        let delta_theta = inp.theta[i + 1] - inp.theta[i];
        if delta_theta.abs() > 1. {
            continue;
        }
        assert!(
            (1. - inp.F_theta[i] / lookup(&inp.theta, &inp.F_theta, inp.theta[i])).abs() < 0.01
        );
        period += delta_theta / inp.F_theta[i];
        // 1 / Omega *
        // (2 * pi)^2 *
        // quadgk(x -> 1 / F(x), 0, Length)[1]^-3 *
        // quadgk(x -> Q(x) / F(x)^3, 0, Length)[1]
        otherint += delta_theta * inp.C_theta[i].powi(2) / 2. / inp.F_theta[i].powi(3);
    }
    let theo_omega = 2. * PI / period;
    let theo_gamma_Omega = (2. * PI).powi(2) * otherint / period.powi(3);
    let robustness = theo_omega / theo_gamma_Omega;
    dbg!(robustness);

    let rinp = &inp;

    let Force = expr_string!(move |x: [f64; 1]| [lookup(&rinp.theta, &rinp.F_theta, x[0])]);

    let N = 10000;
    let dx = 2. * PI / N as f64;
    let mut period = 0.;
    for i in 0..N {
        let x = i as f64 * dx;
        period += dx / Force.0([x])[0];
    }
    dbg!(period);

    let (Force_func, _Force_str) = Force;
    let trajectory_writeout_timestep = 1. / 100_f64;
    let langevin_step_repeat = 10;
    let N = (time / trajectory_writeout_timestep).ceil() as usize;
    let mut sum_or_avg = vec![0.; N];
    dbg!(inp.theta[max_x.0]);
    dbg!(lookup(&rinp.theta, &rinp.shape, 1.9874835327024665));

    StandardMulticore::<StandardEulerMaruyama>::default().multicore(
        omega,
        true,
        [1.9874835327024665],
        N,
        multi,
        Force_func,
        |x| [[lookup(&rinp.theta, &rinp.C_theta, x[0])]],
        |x, _t| lookup(&rinp.theta, &rinp.shape, x[0]).0,
        multi,
        langevin_step_repeat,
        trajectory_writeout_timestep,
        &mut sum_or_avg,
    );
    sum_or_avg
}

fn brüssel_2d() -> Vec<f64> {
    let deltamu = 8.;
    let Omega = 1e4;
    let bc = BrüsselConf::like_basile(deltamu);

    let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

    let multi = 2_usize.pow(13);
    let time = 100.;

    let (Force_func, _Force_str) = Force;
    let trajectory_writeout_timestep = 1. / 100_f64;
    let langevin_step_repeat = 10;
    let N = (time / trajectory_writeout_timestep).ceil() as usize;
    let mut sum_or_avg = vec![0.; N];
    StandardMulticore::<StandardEulerMaruyama>::default().multicore(
        Omega,
        false,
        [1.154288047122632, 1.945407053355939],
        N,
        multi,
        Force_func,
        |q| brüssel_C_maybe_wrong(&bc, q),
        |x, _t| x[0],
        multi,
        langevin_step_repeat,
        trajectory_writeout_timestep,
        &mut sum_or_avg,
    );
    sum_or_avg
}

fn save_projected() {
    let mu = 8.;
    let mut inp: LimitCycleShape = serde_json::from_reader(
        File::open(format!("/data/weissmann/brüssel_shape_mu_{:.1}.json", mu)).unwrap(),
    )
    .unwrap();
    inp.fix_format();

    let max_x = inp
        .shape
        .iter()
        .enumerate()
        .max_by(|(_, q), (_, b)| q.0.partial_cmp(&b.0).expect("nan"))
        .unwrap();
    dbg!(max_x);

    let omega = 10000.;
    let multi = 2_usize.pow(20);
    let time = 4000.;

    let mut period = 0.;
    let mut otherint = 0.;
    for i in 0..inp.theta.len() - 1 {
        let delta_theta = inp.theta[i + 1] - inp.theta[i];
        if delta_theta.abs() > 1. {
            continue;
        }
        assert!(
            (1. - inp.F_theta[i] / lookup(&inp.theta, &inp.F_theta, inp.theta[i])).abs() < 0.01
        );
        period += delta_theta / inp.F_theta[i];
        // 1 / Omega *
        // (2 * pi)^2 *
        // quadgk(x -> 1 / F(x), 0, Length)[1]^-3 *
        // quadgk(x -> Q(x) / F(x)^3, 0, Length)[1]
        otherint += delta_theta * inp.C_theta[i].powi(2) / 2. / inp.F_theta[i].powi(3);
    }
    let theo_omega = 2. * PI / period;
    let theo_gamma_Omega = (2. * PI).powi(2) * otherint / period.powi(3);
    let robustness = theo_omega / theo_gamma_Omega;
    dbg!(robustness);

    let rinp = &inp;

    let Force = expr_string!(move |x: [f64; 1]| [lookup(&rinp.theta, &rinp.F_theta, x[0])]);

    let N = 10000;
    let dx = 2. * PI / N as f64;
    let mut period = 0.;
    for i in 0..N {
        let x = i as f64 * dx;
        period += dx / Force.0([x])[0];
    }
    dbg!(period);

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "brüssel_3_meld_proj.json",
        omega,
        true,
        [1.9874835327024665],
        time,
        multi,
        Force,
        |x| [[lookup(&rinp.theta, &rinp.C_theta, x[0])]],
        |x, _t| lookup(&rinp.theta, &rinp.shape, x[0]).0,
        vec![("deltamu".to_string(), mu)],
    );
}

const STAT: (f64, f64) = (0.3977055399961657, 2.2798962914592367);
fn coord_travo(x: f64, y: f64) -> (f64, f64) {
    let rel_x = x - STAT.0;
    let rel_y = y - STAT.1;
    let r = (rel_x.powi(2) + rel_y.powi(2)).sqrt();
    let mut theta = rel_x.atan2(rel_y);
    if theta < 0. {
        theta += 2. * PI;
    }
    (r, theta)
}
fn back_coord_travo(r: f64, theta: f64) -> (f64, f64) {
    let x = r * theta.sin() + STAT.0;
    let y = r * theta.cos() + STAT.1;
    (x, y)
}

fn meld_2d() {
    let deltamu = 8.;
    let bc = BrüsselConf::like_basile(deltamu);

    let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

    let multi = 2_usize.pow(20);
    let time = 4000.;

    let Omega = 1e4;
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "brüssel_3_meld.json",
        Omega,
        false,
        [1.154288047122632, 1.945407053355939],
        time,
        multi,
        Force,
        |q| brüssel_C_maybe_wrong(&bc, q),
        |x, _t| x[0],
        vec![("deltamu".to_string(), deltamu)],
    );
}

pub fn curwork() {
    //save_projected();

    let mu = 8.;
    let mut inp: LimitCycleShape = serde_json::from_reader(
        File::open(format!("/data/weissmann/brüssel_shape_mu_{:.1}.json", mu)).unwrap(),
    )
    .unwrap();
    inp.fix_format();

    let rinp = &inp;

    // {
    //     let mut rng = rand::rngs::SmallRng::from_entropy();
    //     let n = Normal::new(0.0, 1.0).unwrap();
    //     let int = rng.gen::<f64>() * 2. * PI;
    //     let x = lookup(&rinp.theta, &rinp.shape, int);
    //     let x = [x.0, x.1];

    //     let (r, theta) = coord_travo(x[0], x[1]);
    //     dbg!(int, theta);
    //     let ideal = lookup(&rinp.theta, &rinp.shape, theta);
    //     dbg!(x, ideal);
    //     let r_ideal = ((ideal.0 - STAT.0).powi(2) + (ideal.1 - STAT.1).powi(2)).sqrt();
    //     dbg!(r - r_ideal);
    // }

    // return;

    let deltamu = 8.;
    let bc = BrüsselConf::like_basile(deltamu);

    let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

    let multi = 2_usize.pow(20);
    //let time = 4000.;
    let time = 20.;

    let Omega = 1e4;
    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        //&format!("brüssel_3_meld.json"),
        "brüssel_3_offsets.json",
        Omega,
        false,
        [1.154288047122632, 1.945407053355939],
        time,
        multi,
        Force,
        |q| brüssel_C_maybe_wrong(&bc, q),
        //|x| x[0],
        |x, _t| -> f64 {
            let (r, theta) = coord_travo(x[0], x[1]);
            let ideal = lookup(&rinp.theta, &rinp.shape, theta);
            let r_ideal = ((ideal.0 - STAT.0).powi(2) + (ideal.1 - STAT.1).powi(2)).sqrt();
            r - r_ideal
        },
        vec![("deltamu".to_string(), deltamu)],
    );
    // julia> maximum(js["avg"])
    // 0.007322177176293816

    // let d1 = brüssel_projected();
    // let d2 = brüssel_2d();
    // d1.iter()
    //     .zip(d2.iter())
    //     .for_each(|(a, b)| println!("{:.3}\t{:.3}", a, b));
}

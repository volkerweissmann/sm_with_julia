use crate::rand::distributions::Distribution;
use crate::*;
use rand::SeedableRng;
use statrs::distribution::Normal;

pub struct LimitCycle {
    points: Vec<[f64; 2]>, // points on the limit cycle
}
pub fn sq_dist(a: [f64; 2], b: [f64; 2]) -> f64 {
    (a[0] - b[0]).powi(2) + (a[1] - b[1]).powi(2)
}
impl LimitCycle {
    fn find_closest(&self, q: [f64; 2], init_guess: usize) -> usize {
        let mut cursor = init_guess;
        if sq_dist(q, self.points[cursor + 1]) < sq_dist(q, self.points[cursor]) {
            cursor += 1;
        }
        if sq_dist(q, self.points[cursor - 1]) < sq_dist(q, self.points[cursor]) {
            cursor -= 1;
        }
        todo!()
    }
}

pub fn get_limit_cycle_shape(bc: BrüsselConf) -> Option<LimitCycle> {
    let stat = stationary_point(bc);
    let (q0, _period) = find_point_on_limit_cycle(bc)?;
    let mut q = q0;
    let dt = 1e-3;
    let mut lc = LimitCycle { points: Vec::new() };
    loop {
        lc.points.push(q);
        let old_q = q;
        let F = brüssel_force(&bc, q);
        q[0] += F[0] * dt;
        q[1] += F[1] * dt;
        if q == old_q {
            // q == old_q means that F is near 0 and q is therefore the
            // stationary point. The stationary point seems to be attractive,
            // because we started in [0., 0.] and ended up here. So there is
            // probably no limit cycle.
            // But this is only reached if find_point_on_limit_cycle returns Some(_).
            unreachable!();
        }
        let theta1 = rust_theta_travo(stat, old_q);
        let theta2 = rust_theta_travo(stat, q);
        let target = 0.;
        // theta is decreasing
        if theta1 > target && theta2 < target {
            let error = (q0[0] - q[0]).powi(2) + (q0[1] - q[1]).powi(2);
            assert!(error < 1e-6);
            break;
        }
    }
    Some(lc)
}

fn simulate_1_5_D_trajectory<C1, C2, C3, const DIM: usize>(
    N: usize,
    trajectory_writeout_timestep: f64,
    Omega: f64,
    wrap_2pi: bool,
    x0: [f64; DIM],
    langevin_step_repeat: usize,
    Force: C1,
    diffusion_C: C2,
    out_trafo: C3,
) -> Vec<f64>
where
    C1: Fn([f64; DIM]) -> [f64; DIM],
    C2: Fn([f64; DIM]) -> [[f64; DIM]; DIM],
    C3: Fn([f64; DIM], f64) -> f64,
{
    let mut rng = rand::rngs::SmallRng::from_entropy();
    let n = Normal::new(0.0, 1.0).unwrap();
    let space_period = 2. * PI;
    let mut ret = Vec::with_capacity(N);
    let dt = trajectory_writeout_timestep / langevin_step_repeat as f64;
    let mut x = x0;
    for step in 0..N {
        ret.push(out_trafo(x, dt * langevin_step_repeat as f64 * step as f64));
        for _ in 0..langevin_step_repeat {
            let F = Force(x);
            let C = diffusion_C(x);
            let mut zetas = [0.; DIM];
            for j in 0..DIM {
                zetas[j] = n.sample(&mut rng);
            }
            for i in 0..DIM {
                let f = F[i] * dt;
                x[i] += f;
                for j in 0..DIM {
                    let g = C[i][j] * (dt / Omega).sqrt();
                    x[i] += g * zetas[j];
                }
                if wrap_2pi && x[i] >= space_period {
                    x[i] -= space_period;
                } else if wrap_2pi && x[i] < 0. {
                    x[i] += space_period;
                }
            }
        }
    }
    ret
}

pub fn move_to_cycle() {
    let deltamu = 5.0;

    let bc = BrüsselConf::like_basile(deltamu);
    let lc = get_limit_cycle_shape(bc);
}

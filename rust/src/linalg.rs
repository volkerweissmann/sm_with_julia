pub fn abs<const DIM: usize>(v: [f64; DIM]) -> f64 {
    v.iter().map(|x| x.powi(2)).sum::<f64>().sqrt()
}

pub fn sqabs<const DIM: usize>(v: [f64; DIM]) -> f64 {
    v.iter().map(|x| x.powi(2)).sum::<f64>()
}

pub fn scalar_prod<const DIM: usize>(v: [f64; DIM], w: [f64; DIM]) -> f64 {
    v.iter().zip(w.iter()).map(|(x, y)| x * y).sum()
}

pub fn scale<const DIM: usize>(a: f64, mut v: [f64; DIM]) -> [f64; DIM] {
    v.iter_mut().for_each(|x| *x *= a);
    v
}

pub fn add_assign<const DIM: usize>(v: &mut [f64; DIM], w: [f64; DIM]) {
    v.iter_mut().zip(w.iter()).for_each(|(v, w)| *v += w);
}

pub fn transpose<const DIM: usize>(A: [[f64; DIM]; DIM]) -> [[f64; DIM]; DIM] {
    let mut ret = [[0.0; DIM]; DIM];
    for i in 0..DIM {
        for j in 0..DIM {
            ret[i][j] = A[j][i];
        }
    }
    ret
}

pub fn matrix_matrix<const DIM: usize>(
    A: [[f64; DIM]; DIM],
    B: [[f64; DIM]; DIM],
) -> [[f64; DIM]; DIM] {
    let mut ret = [[0.0; DIM]; DIM];
    for i in 0..DIM {
        for j in 0..DIM {
            for k in 0..DIM {
                ret[i][j] += A[i][k] * B[k][j];
            }
        }
    }
    ret
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_scale() {
        let mut v = [2., 3.];
        let w = scale(3.0, v);
        assert_eq!(v[0], 2.);
        assert_eq!(v[1], 3.);
        assert_eq!(w[0], 2. * 3.);
        assert_eq!(w[1], 3. * 3.);
    }
    #[test]
    fn test_add_assign() {
        let mut v = [2., 3.];
        let w = [4., 5.];
        add_assign(&mut v, w);
        assert_eq!(v[0], 6.);
        assert_eq!(v[1], 8.);
    }
}

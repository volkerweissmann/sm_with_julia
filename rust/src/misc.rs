use std::f64::consts::PI;

#[macro_export]
macro_rules! expr_string {
    ($expr:expr) => {
        ($expr, String::from(stringify!($expr)))
    };
}

pub fn between_pm_pi(x: f64) -> f64 {
    (x + PI).rem_euclid(2. * PI) - PI
    //(x + PI) % (2. * PI) - PI
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_between_pm_pi() {
        for i in -10..10 {
            let x = i as f64 * PI / 2.;
            dbg!(x, between_pm_pi(x));
        }
    }
}

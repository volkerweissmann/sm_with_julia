use crate::linalg::*;
use crate::rand::distributions::Distribution;
use crate::*;
use rand::SeedableRng;
use statrs::distribution::Normal;

// Simulates a trajectory with a two dimensional state, two dimensional force, but one dimensional diffusion
pub fn project_diffusion<C1, C2>(Force: C1, diffusion_C: C2, q: [f64; 2]) -> [[f64; 2]; 2]
where
    C1: Fn([f64; 2]) -> [f64; 2],
    C2: Fn([f64; 2]) -> [[f64; 2]; 2],
{
    let F = Force(q);
    let C2D = diffusion_C(q);

    // C1D F = C2D F
    // C1D  (-y, x) = 0

    todo!();
}

#[derive(Clone, Copy, Default)]
struct Force2D_Diffusion1D {}

impl SingleTrajectory for Force2D_Diffusion1D {
    fn simulate_single<C1, C2, C3, const DIM: usize>(
        N: usize,
        trajectory_writeout_timestep: f64,
        Omega: f64,
        _wrap_2pi: bool,
        x0: [f64; DIM],
        langevin_step_repeat: usize,
        Force: C1,
        diffusion_C: C2,
        out_trafo: C3,
    ) -> Vec<f64>
    where
        C1: Fn([f64; DIM]) -> [f64; DIM],
        C2: Fn([f64; DIM]) -> [[f64; DIM]; DIM],
        C3: Fn([f64; DIM], f64) -> f64,
    {
        let mut rng = rand::rngs::SmallRng::from_entropy();
        let n = Normal::new(0.0, 1.0).unwrap();
        let mut ret = Vec::with_capacity(N);
        let dt = trajectory_writeout_timestep / langevin_step_repeat as f64;
        let mut x = x0;
        for step in 0..N {
            ret.push(out_trafo(x, dt * langevin_step_repeat as f64 * step as f64));
            for _ in 0..langevin_step_repeat {
                let F = Force(x);
                let C = diffusion_C(x);
                let mut zetas = [0.; DIM];
                for j in 0..DIM {
                    zetas[j] = n.sample(&mut rng);
                }
                let mut diff_delta = [0.; DIM];
                for i in 0..DIM {
                    let f = F[i] * dt;
                    x[i] += f;
                    for j in 0..DIM {
                        let g = C[i][j] * (dt / Omega).sqrt();
                        diff_delta[i] += g * zetas[j];
                    }
                }
                add_assign(&mut x, scale(scalar_prod(diff_delta, F) / sqabs(F), F));
            }
        }
        ret
    }
}

pub fn brüssel_1_p_5_D() {
    let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
    deltamus.push(6.0); // Style: This is terrible
    deltamus.push(6.5);
    deltamus.push(7.0);
    deltamus.push(7.5);
    deltamus.push(8.0);

    for deltamu in deltamus.iter() {
        let bc = BrüsselConf::like_basile(*deltamu);
        let q0 = find_point_on_limit_cycle(bc);
        if q0.is_none() {
            println!(
                "No limit cycle found for deltamu = {}, so no langevin simulation will be done.",
                deltamu
            );
            continue;
        }
        let (q0, period) = q0.unwrap();

        for Omega in [1e2, 1e3, 1e4] {
            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let multi = 2_usize.pow(16);
            let time = 1. * Omega;

            save_ensemble_average(
                StandardMulticore::<Force2D_Diffusion1D>::default(),
                &format!(
                    "brüssel_1p5_Omega_{:.0}_deltamu_{:.1}.json",
                    Omega, *deltamu
                ),
                Omega,
                false,
                q0,
                time,
                multi,
                Force,
                |q| brüssel_C_benedikt(&bc, q),
                |x, _t| x[0],
                vec![("deltamu".to_string(), *deltamu)],
            );
        }
    }
}

pub fn brüssel_1_p_5_D_theta() {
    let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
    deltamus.push(6.0); // Style: This is terrible
    deltamus.push(6.5);
    deltamus.push(7.0);
    deltamus.push(7.5);
    deltamus.push(8.0);

    for deltamu in deltamus.iter() {
        let bc = BrüsselConf::like_basile(*deltamu);
        let stat = stationary_point(bc);
        let q0 = find_point_on_limit_cycle(bc);
        if q0.is_none() {
            println!(
                "No limit cycle found for deltamu = {}, so no langevin simulation will be done.",
                deltamu
            );
            continue;
        }
        let (q0, period) = q0.unwrap();

        for Omega in [1e2, 1e3, 1e4] {
            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let multi = 2_usize.pow(16);
            let time = 1. * Omega;

            save_ensemble_average(
                StandardMulticore::<Force2D_Diffusion1D>::default(),
                &format!(
                    "brüssel_1p5_theta_Omega_{:.0}_deltamu_{:.1}.json",
                    Omega, *deltamu
                ),
                Omega,
                false,
                q0,
                time,
                multi,
                Force,
                |q| brüssel_C_benedikt(&bc, q),
                |x, _t| rust_theta_travo(stat, x),
                vec![("deltamu".to_string(), *deltamu)],
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_1p5() {
        let mut x = [0., 0.];
        let F = [7., 7.];
        let diff_delta = [2., 1.];
        add_assign(&mut x, scale(scalar_prod(diff_delta, F) / sqabs(F), F));
        dbg!(x);
    }
}

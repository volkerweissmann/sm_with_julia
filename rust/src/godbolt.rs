#![feature(test)]
#![feature(is_sorted)]
#![feature(bench_black_box)]
#![allow(non_snake_case)]
extern crate test;

use std::hint::black_box;

struct MyStruct(i32);

#[inline(never)]
fn sample(x: &mut MyStruct) -> f64 {
    x.0 = black_box(x.0);
    black_box(123.456)
}

use std::f64::consts::PI;
use std::fs::File;

fn simulate_single_trajectory<C_1, C_2, C_3>(
    N: usize,
    trajectory_writeout_timestep: f64,
    omega: f64,
    x0: f64,
    langevin_step_repeat: usize,
    Force: C_1,
    diffusion_C: C_2,
    out_trafo: C_3,
) -> Vec<f64>
where
    C_1: Fn(f64) -> f64,
    C_2: Fn(f64) -> f64,
    C_3: Fn(f64) -> f64,
{
    let mut rng = MyStruct(0);

    let space_period = 2. * PI;

    let mut ret = Vec::with_capacity(N);

    let dt = trajectory_writeout_timestep / langevin_step_repeat as f64;

    //let mut t = 0.;
    let mut x = x0;
    for _ in 0..N {
        ret.push(out_trafo(x));

        // let U_1: f32 = rng.gen();
        // let U_2: f32 = rng.gen();
        // // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
        // let Z_0 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).cos();
        // //let Z_1 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).sin();
        // let W = Z_0;

        for _ in 0..langevin_step_repeat {
            let F = Force(x);
            let f = F * dt;
            let C = diffusion_C(x);
            // C=sqrt(2Q)
            // Q = 1/2 C^2
            let g = C * (dt / omega).sqrt();
            //let g = (2. * Q / omega * dt).sqrt();
            let W = sample(&mut rng);
            //x += f + g * W;
            x += g.mul_add(W, f);
            if x >= space_period {
                x -= space_period;
            } else if x < 0. {
                x += space_period;
            }
        }
    }
    ret
}

pub fn main() {
    black_box(simulate_single_trajectory(
        100000,
        0.01,
        1e4,
        1.3,
        10,
        |x| 1. + 0.5 * x.sin(),
        |_| 2_f64.sqrt(),
        std::convert::identity,
    ));
}

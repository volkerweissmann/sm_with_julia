use serde::{Deserialize, Serialize};
use std::f64::consts::PI;

#[derive(Deserialize)]
pub struct LimitCycleShape {
    period: f64,
    mu: f64,
    pub shape: Vec<(f64, f64)>,
    pub theta: Vec<f64>,
    pub F_theta: Vec<f64>,
    pub C_theta: Vec<f64>,
}
fn cycle_vec<T>(vec: &mut Vec<T>, ind: usize) -> Vec<T> {
    let mut new = vec.split_off(ind);
    new.append(vec);
    new
}
impl LimitCycleShape {
    pub fn fix_format(&mut self) {
        assert!(self.theta.iter().all(|x| *x >= 0. && *x <= 2. * PI));
        let ind = self.theta.iter().position(|x| *x < 1.).unwrap();
        // self.theta.iter_mut().for_each(|x| *x += PI);

        self.theta = cycle_vec(&mut self.theta, ind);
        self.F_theta = cycle_vec(&mut self.F_theta, ind);
        self.C_theta = cycle_vec(&mut self.C_theta, ind);
        self.shape = cycle_vec(&mut self.shape, ind);

        self.F_theta.push(self.F_theta[0]);
        self.C_theta.push(self.C_theta[0]);
        self.shape.push(self.shape[0]);

        let mut res = 0.;
        for i in 0..self.theta.len() - 1 {
            res += (self.theta[i] - self.theta[i + 1]).max(0.);
        }
        assert!(res < 1e-6); // res should be zero, but we allow a tiny derivation because of numerical errors in self.theta

        for i in 0..self.theta.len() - 1 {
            if self.theta[i + 1] < self.theta[i] {
                self.theta[i + 1] = self.theta[i];
            }
        }

        assert!(self.theta.iter().is_sorted());
    }
}

pub fn lookup<T: Copy>(theta: &[f64], dat: &[T], x: f64) -> T {
    let ind = theta.binary_search_by(|a| a.partial_cmp(&x).unwrap());
    let ind = match ind {
        Ok(ind) => ind,
        Err(ind) => ind,
    };
    // are we off by one?
    dat[ind]
}

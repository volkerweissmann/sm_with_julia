#![feature(test)]
#![feature(is_sorted)]
#![feature(bench_black_box)]
#![feature(generic_const_exprs)]
#![feature(adt_const_params)]
#![allow(non_snake_case)]
#![allow(incomplete_features)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(clippy::too_many_arguments)]
#![allow(clippy::many_single_char_names)]
extern crate mathru;
extern crate num_cpus;
extern crate plotters;
extern crate rand;
extern crate rand_distr;
extern crate rand_xorshift;
extern crate scoped_threadpool;
extern crate serde;
extern crate serde_json;
extern crate test;

mod brussel_meld;
mod hopf_meld;
mod langevin;
mod limit_cycle;
mod limit_line;
mod post_radial;
#[macro_use]
mod misc;
mod Force_2D_Diff_1D;
mod brussel_1D;
mod brussel_diff;
mod hack;
mod linalg;
mod move_to_cycle;
mod plot;
mod sample;

use crate::brussel_diff::rust_theta_travo;
use crate::langevin::*;
use crate::limit_cycle::*;
use std::f64::consts::PI;
use std::fs::File;

fn gen_data_a() {
    // assert_eq!(num_cpus::get(), 128, "Not running on the hydra");

    for ind in 0..21 {
        let omega = 10000. * (10_f64).powf(-ind as f64 / 10.);
        let multi = 2_usize.pow(20);
        let time = 2. * PI * 400.;
        let Force = expr_string!(|x: [f64; 1]| [2. + x[0].sin()]);
        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("omega_{}.json", omega),
            omega,
            true,
            [1.3_f64],
            time,
            multi,
            Force,
            |_| [[2_f64.sqrt()]],
            |q, _t| q[0],
            vec![],
        );
    }
}

fn gen_data_b() {
    // assert_eq!(num_cpus::get(), 128, "Not running on the hydra");

    for omega in [1e2, 1e3, 1e4, 1e5] {
        for ai in 1..100 {
            let a = -1. + ai as f64 / 50.;
            let multi = 2_usize.pow(16);
            let time = 2. * PI * 400.;
            let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("omega_{}_a_{:.2}.json", omega, a),
                omega,
                true,
                [1.3_f64],
                time,
                multi,
                Force,
                |_| [[2_f64.sqrt()]],
                |q, _t| q[0],
                vec![("a".to_string(), a)],
            );
        }
    }
}

fn gen_data_1d_long() {
    // assert_eq!(num_cpus::get(), 128, "Not running on the hydra");

    for omega in [1e4] {
        for ai in 40..60 {
            let a = -1. + ai as f64 / 50.;
            let multi = 2_usize.pow(20);
            let time = 2. * PI * 4000.;
            let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("long_omega_{}_a_{:.2}.json", omega, a),
                omega,
                true,
                [1.3_f64],
                time,
                multi,
                Force,
                |_| [[2_f64.sqrt()]],
                |q, _t| q[0],
                vec![("a".to_string(), a)],
            );
        }
    }
}

fn gen_data_1d_long_middle() {
    // assert_eq!(num_cpus::get(), 128, "Not running on the hydra");

    for omega in [1e4] {
        for ai in 1..100 {
            let a = -1. + ai as f64 / 50.;
            let multi = 2_usize.pow(20);
            let time = 2. * PI * 4000.;
            let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("long_x0_PI_omega_{}_a_{:.2}.json", omega, a),
                omega,
                true,
                [PI],
                time,
                multi,
                Force,
                |_| [[2_f64.sqrt()]],
                |q, _t| q[0],
                vec![("a".to_string(), a)],
            );
        }
    }
}

fn gen_data_1d_symm() {
    for omega in [1e2, 1e3, 1e4] {
        for ai in 1..100 {
            let a = -1. + ai as f64 / 50.;
            let multi = 2_usize.pow(18);
            let time = 2. * PI * 400.;
            let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("omega_x0_0_{}_a_{:.2}.json", omega, a),
                omega,
                true,
                [0_f64],
                time,
                multi,
                Force,
                |_| [[2_f64.sqrt()]],
                |q, _t| q[0],
                vec![("a".to_string(), a)],
            );
        }
    }
}

fn gen_data_1d_middle() {
    for omega in [1e4] {
        for ai in 40..61 {
            // for omega in [1e2, 1e3, 1e4] {
            //     for ai in 1..100 {
            let a = -1. + ai as f64 / 50.;
            let multi = 2_usize.pow(18);
            let time = 2. * PI * 400.;
            let Force = expr_string!(move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]);

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("omega_x0_PI_{}_a_{:.2}.json", omega, a),
                omega,
                true,
                [PI],
                time,
                multi,
                Force,
                |_| [[2_f64.sqrt()]],
                |q, _t| q[0],
                vec![("a".to_string(), a)],
            );
        }
    }
}

fn cubic_hopf_bifurcation_force(q: [f64; 2], mu: f64, omega: f64, b: f64) -> [f64; 2] {
    let x = q[0];
    let y = q[1];

    let r = (x.powi(2) + y.powi(2)).sqrt();
    let theta = y.atan2(x);

    let drdt = mu * r - r.powi(3);
    let dthetadt = omega + b * r.powi(2);

    let dxdr = theta.cos();
    let dxdtheta = -theta.sin() * r;
    let dydr = theta.sin();
    let dydtheta = theta.cos() * r;

    let dxdt = drdt * dxdr + dthetadt * dxdtheta;
    let dydt = drdt * dydr + dthetadt * dydtheta;

    [dxdt, dydt]
}

fn cubic_hopf_bifurcation_force_a(q: [f64; 2], mu: f64, omega: f64, b: f64, a: f64) -> [f64; 2] {
    let x = q[0];
    let y = q[1];

    let r = (x.powi(2) + y.powi(2)).sqrt();
    let theta = y.atan2(x);

    let drdt = mu * r - a * r.powi(3);
    let dthetadt = omega + b * r.powi(2);

    let dxdr = theta.cos();
    let dxdtheta = -theta.sin() * r;
    let dydr = theta.sin();
    let dydtheta = theta.cos() * r;

    let dxdt = drdt * dxdr + dthetadt * dxdtheta;
    let dydt = drdt * dydr + dthetadt * dydtheta;

    [dxdt, dydt]
}

fn mexican_hat_force(q: [f64; 2], sigma: f64, omega: f64, b: f64) -> [f64; 2] {
    let x = q[0];
    let y = q[1];

    let r = (x.powi(2) + y.powi(2)).sqrt();
    let theta = y.atan2(x);

    // drdt is 0 if r = sigma * 3_f64.sqrt()
    let drdt = 2. / (3. * sigma).sqrt()
        * PI.powf(-1. / 4.)
        * (-r.powi(2) / (2. * sigma.powi(2))).exp()
        * (3. - (r / sigma).powi(2))
        * r
        / sigma.powi(2);
    let dthetadt = omega + b * r.powi(2);

    let dxdr = theta.cos();
    let dxdtheta = -theta.sin() * r;
    let dydr = theta.sin();
    let dydtheta = theta.cos() * r;

    let dxdt = drdt * dxdr + dthetadt * dxdtheta;
    let dydt = drdt * dydr + dthetadt * dydtheta;
    [dxdt, dydt]
}

fn gen_data_cubic_hopf_bifurcation() {
    let mu = 2.;
    let omega = 1.;
    let b = 1.;
    let Force = expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

    let Omega = 1000.;
    let multi = 2_usize.pow(16);
    let time = 100.;

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        "cubic_hopf.json",
        Omega,
        false,
        [1., 0.],
        time,
        multi,
        Force,
        |q| {
            let r = (q[0].powi(2) + q[1].powi(2)).sqrt();
            [[r, 0.], [0., r]]
        },
        |x, _t| x[1],
        vec![
            ("mu".to_string(), mu),
            ("omega".to_string(), omega),
            ("b".to_string(), b),
        ],
    );
}

fn gen_data_cubic_hopf_bifurcation_2() {
    for Omega in [1e2, 1e3, 1e4] {
        for omega in [-2., -1., 1., 2., 3.] {
            for i_mu in 0..20 {
                let mu = 0.4 + i_mu as f64 * 0.2;
                let b = 1.;
                let Force =
                    expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

                let multi = 2_usize.pow(12);
                let time = 100.;

                save_ensemble_average(
                    StandardMulticore::<StandardEulerMaruyama>::default(),
                    &format!(
                        "cubic_hopf_Omega_{:.0}_omega_{:.2}_mu_{:.2}.json",
                        Omega, omega, mu
                    ),
                    Omega,
                    false,
                    [1., 0.],
                    time,
                    multi,
                    Force,
                    |q| {
                        let r = (q[0].powi(2) + q[1].powi(2)).sqrt();
                        [[r, 0.], [0., r]]
                    },
                    |x, _t| x[1],
                    vec![
                        ("mu".to_string(), mu),
                        ("omega".to_string(), omega),
                        ("b".to_string(), b),
                    ],
                );
            }
        }
    }
}

fn gen_data_cubic_hopf_bifurcation_3() {
    for Omega in [1e2, 1e3, 1e4] {
        for omega in [-2., -1., 1., 2., 3.] {
            for i_mu in 0..20 {
                let mu = 0.4 + i_mu as f64 * 0.2;
                let b = 1.;
                let Force =
                    expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

                let multi = 2_usize.pow(12);
                let time = 100.;
                save_ensemble_average(
                    StandardMulticore::<StandardEulerMaruyama>::default(),
                    &format!(
                        "cubic_hopf_Q_1_Omega_{:.0}_omega_{:.2}_mu_{:.2}.json",
                        Omega, omega, mu
                    ),
                    Omega,
                    false,
                    [1., 0.],
                    time,
                    multi,
                    Force,
                    |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                    |x, _t| x[1],
                    vec![
                        ("mu".to_string(), mu),
                        ("omega".to_string(), omega),
                        ("b".to_string(), b),
                    ],
                );
            }
        }
    }
}

fn gen_data_cubic_hopf_bifurcation_4() {
    for Omega in [1e2, 1e3, 1e4] {
        for omega in [-2., -1., 1., 2., 3.] {
            for i_mu in 0..20 {
                let mu = 0.4 + i_mu as f64 * 0.2;
                let b = 1.;
                let Force =
                    expr_string!(move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b));

                let multi = 2_usize.pow(12);
                let time = 100.;
                save_ensemble_average(
                    StandardMulticore::<StandardEulerMaruyama>::default(),
                    &format!(
                        "cubic_hopf_theta_Q_1_Omega_{:.0}_omega_{:.2}_mu_{:.2}.json",
                        Omega, omega, mu
                    ),
                    Omega,
                    false,
                    [1., 0.],
                    time,
                    multi,
                    Force,
                    |_| [[2_f64.sqrt(), 0.], [0., 2_f64.sqrt()]],
                    |q, _t| q[1].atan2(q[0]),
                    vec![
                        ("mu".to_string(), mu),
                        ("omega".to_string(), omega),
                        ("b".to_string(), b),
                    ],
                );
            }
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct BrüsselConf {
    A: f64,
    B: f64,
    k1p: f64,
    k1m: f64,
    k2p: f64,
    k2m: f64,
    k3p: f64,
    k3m: f64,
}

impl BrüsselConf {
    fn like_basile(deltamu: f64) -> BrüsselConf {
        let A = 1.0;
        let B = 3.0;
        let k1p = 0.1;
        let k1m = 1.0;
        let k2p = 0.1;
        let k2m = (-deltamu).exp() * k1m * k2p * B / (k1p * A);
        BrüsselConf {
            A,
            B,
            k1p,
            k1m,
            k2p,
            k3p: 1.0,
            k3m: 1.0,
            k2m,
        }
    }
}

fn brüssel_force(p: &BrüsselConf, q: [f64; 2]) -> [f64; 2] {
    [
        p.k1p * p.A - p.k1m * q[0] + p.k3p * q[0].powi(2) * q[1] - p.k3m * q[0].powi(3),
        p.k2p * p.B - p.k2m * q[1] - p.k3p * q[0].powi(2) * q[1] + p.k3m * q[0].powi(3),
    ]
}

fn brüssel_C_maybe_wrong(p: &BrüsselConf, q: [f64; 2]) -> [[f64; 2]; 2] {
    let x = q[0];
    let y = q[1];

    let a_sq = p.k1p * p.A + p.k1m * x;
    let b_sq = p.k2p * p.B + p.k2m * y;
    let c_sq = p.k3p * x.powi(2) * y + p.k3m * x.powi(3);

    let C_x1 = (a_sq + (b_sq * c_sq) / (b_sq + c_sq)).sqrt();
    let C_x2 = c_sq / (b_sq + c_sq).sqrt();
    let C_y1 = 0.;
    let C_y2 = (b_sq + c_sq).sqrt();

    [[C_x1, C_x2], [C_y1, C_y2]]
}

fn brüssel_C_benedikt(p: &BrüsselConf, q: [f64; 2]) -> [[f64; 2]; 2] {
    let x = q[0];
    let y = q[1];

    // Source: julia function our_brüssel_Q(p, q)
    let Q_xx = (p.k1p * p.A + p.k1m * x + p.k3p * x.powi(2) * y + p.k3m * x.powi(3)) / 2.;
    let Q_dia = (-p.k3p * x.powi(2) * y - p.k3m * x.powi(3)) / 2.;
    let Q_yy = (p.k2p * p.B + p.k2m * y + p.k3p * x.powi(2) * y + p.k3m * x.powi(3)) / 2.;

    let m = (2. / Q_xx).sqrt();
    let C_x1 = m * Q_xx;
    let C_x2 = 0.;
    let C_y1 = m * Q_dia;
    let C_y2 = m * (Q_xx * Q_yy - Q_dia.powi(2)).sqrt();

    [[C_x1, C_x2], [C_y1, C_y2]]
}

fn tang_brüssel_C(p: &BrüsselConf, q: [f64; 2]) -> [[f64; 2]; 2] {
    let x = q[0];
    let y = q[1];

    // Source: julia function tang_our_brüssel_Q(p, q)

    let Fx = p.k1p * p.A - p.k1m * x + p.k3p * x.powi(2) * y - p.k3m * x.powi(3);
    let Fy = p.k2p * p.B - p.k2m * y - p.k3p * x.powi(2) * y + p.k3m * x.powi(3);

    let N = (Fx.powi(2) + Fy.powi(2)).sqrt();

    let fx = Fx / N;
    let fy = Fy / N;

    let a = fx * (p.k1p * p.A + p.k1m * x + p.k3p * x.powi(2) * y + p.k3m * x.powi(3))
        + fy * (p.k2p * p.B + p.k2m * y + p.k3p * x.powi(2) * y + p.k3m * x.powi(3));

    let a = a.abs();

    let Q_xx = fx.powi(2) * a / 2.;
    let Q_dia = fx * fy * a / 2.;
    let Q_yy = fy.powi(2) * a / 2.;

    let m = (2. / Q_xx).sqrt();
    let C_x1 = m * Q_xx;
    let C_x2: f64 = 0.;
    let C_y1 = m * Q_dia;
    let C_y2 = 0.;
    // You may have heard that `C_y2 = m * (Q_xx * Q_yy - Q_dia.powi(2)).sqrt()` , but we use C_y2 = 0.` instead, because in our case `Q_xx * Q_yy - Q_dia.powi(2)` should be zero anyway. If we wouldn't do this, numerical inaccuracies could result in `C_y2 =  m * (-1e-20).sqrt () = NaN`

    [[C_x1, C_x2], [C_y1, C_y2]]
}

fn gen_data_brüsselator_1() {
    for deltamu in [8.] {
        let bc = BrüsselConf::like_basile(deltamu);

        let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

        let Omega = 1000.;
        let multi = 2_usize.pow(17);
        let time = 4000.;

        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("brüssel_deltamu_{:.1}.json", deltamu),
            Omega,
            false,
            [1.0, 0.2],
            time,
            multi,
            Force,
            |q| brüssel_C_maybe_wrong(&bc, q),
            |x, _t| x[1],
            vec![("deltamu".to_string(), deltamu)],
        );
    }
}

fn gen_data_brüsselator_4() {
    let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
    deltamus.push(6.0); // Style: This is terrible
    deltamus.push(6.5);
    deltamus.push(7.0);
    deltamus.push(7.5);
    deltamus.push(8.0);

    for Omega in [1e2, 1e3, 1e4] {
        for deltamu in deltamus.iter() {
            let bc = BrüsselConf::like_basile(*deltamu);

            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let multi = 2_usize.pow(20);
            let time = 4000.;

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("brüssel_4_Omega_{:.0}_deltamu_{:.1}.json", Omega, *deltamu),
                Omega,
                false,
                [1.0, 0.2],
                time,
                multi,
                Force,
                |q| brüssel_C_maybe_wrong(&bc, q),
                |x, _t| x[1],
                vec![("deltamu".to_string(), *deltamu)],
            );
        }
    }
}

fn stationary_point(bc: BrüsselConf) -> [f64; 2] {
    // xvar = variable()
    // yvar = (p.k1p * p.A + p.k2p * p.B - p.k1m * xvar) / p.k2m
    // xstatic::Float64 = Roots.find_zero(p.k1p * p.A - p.k1m * xvar + p.k3p * xvar^2 * yvar - p.k3m * xvar^3, 1.0)
    // ystatic::Float64 = (p.k1p * p.A + p.k2p * p.B - p.k1m * xstatic) / p.k2m
    // @assert isapprox([0,0], limit_dgl([xstatic, ystatic], p, nothing), atol=1e-10)

    // 0 = a + b x + c x^2 + d x^3
    let a = bc.k1p * bc.A;
    let b = -bc.k1m;
    let c = bc.k3p * (bc.k1p * bc.A + bc.k2p * bc.B) / bc.k2m;
    let d = bc.k3p * (-bc.k1m) / bc.k2m - bc.k3m;
    let x0 = roots::find_roots_cubic(d, c, b, a);
    let x0 = if let roots::Roots::One(x0) = x0 {
        x0[0]
    } else {
        panic!()
    };
    let y0 = (bc.k1p * bc.A + bc.k2p * bc.B - bc.k1m * x0) / bc.k2m;

    let F = brüssel_force(&bc, [x0, y0]);
    assert!(F[0].powi(2) + F[1].powi(2) < 1e-10);

    [x0, y0]
}

/// Finds an arbitrary point on the limit cycle
/// And when I mean arbitrary, I mean the one with `theta = 0`.
fn find_point_on_limit_cycle(bc: BrüsselConf) -> Option<([f64; 2], f64)> {
    let stat = stationary_point(bc);

    let mut q: [f64; 2] = [0., 0.];
    let dt = 1e-3;

    let mut last: Option<[f64; 2]> = None;
    let mut last_u = None;
    let mut period = None;
    let mut count = 0;
    for u in 0..2000000 {
        let old_q = q;
        let F = brüssel_force(&bc, q);
        q[0] += F[0] * dt;
        q[1] += F[1] * dt;
        if q == old_q {
            // q == old_q means that F is near 0 and q is therefore the
            // stationary point. The stationary point seems to be attractive,
            // because we started in [0., 0.] and ended up here. So there is
            // probably no limit cycle.
            return None;
        }
        let theta1 = rust_theta_travo(stat, old_q);
        let theta2 = rust_theta_travo(stat, q);
        let target = 0.;
        // theta is decreasing
        if theta1 > target && theta2 < target {
            if let Some(last_u) = last_u {
                period = Some((u - last_u) as f64 * dt);
            }
            count += 1;
            if let Some(last) = last {
                let error = (last[0] - q[0]).powi(2) + (last[1] - q[1]).powi(2);
                if count > 20 {
                    assert!(error < 1e-6);
                    return Some((q, period.unwrap()));
                }
            }
            last = Some(q);
            last_u = Some(u);
        }
    }
    unreachable!();
}

fn gen_data_brüsselator() {
    let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
    deltamus.push(6.0); // Style: This is terrible
    deltamus.push(6.5);
    deltamus.push(7.0);
    deltamus.push(7.5);
    deltamus.push(8.0);

    for deltamu in deltamus.iter() {
        let bc = BrüsselConf::like_basile(*deltamu);
        let q0 = find_point_on_limit_cycle(bc);
        if q0.is_none() {
            println!(
                "No limit cycle found for deltamu = {}, so no langevin simulation will be done.",
                deltamu
            );
            continue;
        }
        let (q0, _period) = q0.unwrap();

        for Omega in [1e2, 1e3, 1e4] {
            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let multi = 2_usize.pow(20);
            let time = 4000.;

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("brüssel_6_Omega_{:.0}_deltamu_{:.1}.json", Omega, *deltamu),
                Omega,
                false,
                q0,
                time,
                multi,
                Force,
                |q| brüssel_C_benedikt(&bc, q),
                |x, _t| x[0],
                vec![("deltamu".to_string(), *deltamu)],
            );
        }
    }
}

fn gen_data_brüsselator_5() {
    let mut deltamus = (38..55).map(|i| (i as f64) / 10.).collect::<Vec<_>>();
    deltamus.push(6.0); // Style: This is terrible
    deltamus.push(6.5);
    deltamus.push(7.0);
    deltamus.push(7.5);
    deltamus.push(8.0);

    for deltamu in deltamus.iter() {
        let bc = BrüsselConf::like_basile(*deltamu);

        for Omega in [1e2, 1e3, 1e4] {
            let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

            let multi = 2_usize.pow(20);
            let time = 4000.;

            save_ensemble_average(
                StandardMulticore::<StandardEulerMaruyama>::default(),
                &format!("brüssel_5_Omega_{:.0}_deltamu_{:.1}.json", Omega, *deltamu),
                Omega,
                false,
                [1.0, 0.2],
                time,
                multi,
                Force,
                |q| brüssel_C_benedikt(&bc, q),
                |x, _t| x[1],
                vec![("deltamu".to_string(), *deltamu)],
            );
        }
    }
}

fn brüssel_projected(mu: f64) {
    let mut inp: LimitCycleShape = serde_json::from_reader(
        File::open(format!("/data/weissmann/brüssel_shape_mu_{:.1}.json", mu)).unwrap(),
    )
    .unwrap();
    inp.fix_format();

    let max_x = inp
        .shape
        .iter()
        .enumerate()
        .max_by(|(_, q), (_, b)| q.0.partial_cmp(&b.0).expect("nan"))
        .unwrap();

    let omega = 1000.;
    let multi = 2_usize.pow(16);
    let time = 4000.;

    let mut period = 0.;
    let mut otherint = 0.;
    for i in 0..inp.theta.len() - 1 {
        let delta_theta = inp.theta[i + 1] - inp.theta[i];
        if delta_theta.abs() > 1. {
            continue;
        }
        assert!(
            (1. - inp.F_theta[i] / lookup(&inp.theta, &inp.F_theta, inp.theta[i])).abs() < 0.01
        );
        period += delta_theta / inp.F_theta[i];
        // 1 / Omega *
        // (2 * pi)^2 *
        // quadgk(x -> 1 / F(x), 0, Length)[1]^-3 *
        // quadgk(x -> Q(x) / F(x)^3, 0, Length)[1]
        otherint += delta_theta * inp.C_theta[i].powi(2) / 2. / inp.F_theta[i].powi(3);
    }
    let theo_omega = 2. * PI / period;
    let theo_gamma_Omega = (2. * PI).powi(2) * otherint / period.powi(3);
    let robustness = theo_omega / theo_gamma_Omega;
    dbg!(robustness);

    let rinp = &inp;

    let Force = expr_string!(move |x: [f64; 1]| [lookup(&rinp.theta, &rinp.F_theta, x[0])]);

    let N = 10000;
    let dx = 2. * PI / N as f64;
    let mut period = 0.;
    for i in 0..N {
        let x = i as f64 * dx;
        period += dx / Force.0([x])[0];
    }
    dbg!(period);

    save_ensemble_average(
        StandardMulticore::<StandardEulerMaruyama>::default(),
        &format!("basile_proj_mu_{:.1}.json", mu),
        omega,
        true,
        [inp.theta[max_x.0]],
        time,
        multi,
        Force,
        |x| [[lookup(&rinp.theta, &rinp.C_theta, x[0])]],
        |x, _t| lookup(&rinp.theta, &rinp.shape, x[0]).0,
        vec![("deltamu".to_string(), mu)],
    );
}

fn reproduce_basile_plots() {
    //for mu in [3.5, 3.8, 3.9, 4.0]
    {
        let mu = 3.9;
        brüssel_projected(mu);
    }
}

fn explain_atan() {
    for y in -10..10 {
        for x in -10..10 {
            if x == 0 && y == 0 {
                continue;
            }
            let x = x as f64 / 10.;
            let y = y as f64 / 10.;
            let r = (x.powi(2) + y.powi(2)).sqrt();

            let theta = y.atan2(x);

            assert!((x - r * theta.cos()).abs() < 1e-9);
            assert!((y - r * theta.sin()).abs() < 1e-9);
        }
    }
}

fn main() {
    //explain_atan();
    //gen_data_brüsselator();
    //brussel_1D::calc_1D_int();
    //sample::brüssel_xtx0mxtx0();
    //sample::show_sampling();
    //sample::brüsel_sampled_ensemble();
    //Force_2D_Diff_1D::brüssel_1_p_5_D();
    //Force_2D_Diff_1D::brüssel_1_p_5_D_theta();s
    //move_to_cycle::move_to_cycle();
    //brussel_diff::brüssel_diffusion();
    //gen_data_brüsselator();
    //post_radial::mail_10_12_x_x0();
    post_radial::mail_19_01_x_x0();
    //post_radial::mexican_hat_x();
    //gen_data_1d_long_middle();
    //post_radial::mexican_hat_verify();
    //post_radial::mexican_hat_meld();
    //post_radial::mexican_hat_meld_4();
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::linalg::*;
    use rand::distributions::Distribution;
    use rand::Rng;
    use rand::SeedableRng;
    use statrs::distribution::Normal;

    #[test]
    fn coord_travo() {
        for x in -10..10 {
            for y in -10..10 {
                let x = x as f64;
                let y = y as f64;

                let r = (x.powi(2) + y.powi(2)).sqrt();
                let theta = y.atan2(x);

                let nx = r * theta.cos();
                let ny = r * theta.sin();

                assert!((x - nx).abs() < 1e-3);
                assert!((y - ny).abs() < 1e-3);
            }
        }
    }

    #[test]
    fn travo_cubic_normal() {
        for _ in 0..1000 {
            let mut rng = rand::rngs::SmallRng::from_entropy();
            let n = Normal::new(0.0, 1.0).unwrap();
            let x = n.sample(&mut rng);
            let y = n.sample(&mut rng);
            let mu = n.sample(&mut rng);
            let omega = n.sample(&mut rng);
            let b = n.sample(&mut rng);
            let ret = cubic_hopf_bifurcation_force([x, y], mu, omega, b);
            let r = (x.powi(2) + y.powi(2)).sqrt();
            let dxdt = x * (mu - r.powi(2)) - y * (omega + b * r.powi(2));
            let dydt = y * (mu - r.powi(2)) + x * (omega + b * r.powi(2));
            assert!((ret[0] - dxdt).abs() < 1e-3);
            assert!((ret[1] - dydt).abs() < 1e-3);
        }
    }

    #[test]
    fn brüssel_C_Q() {
        let bc = BrüsselConf::like_basile(5.3);
        let q = [0.4, 2.1];
        let C = brüssel_C_benedikt(&bc, q);
        let Csq_1 = matrix_matrix(transpose(C), C);
        let Csq_2 = matrix_matrix(C, transpose(C));

        // Source: julia function our_brüssel_Q(p, q)

        let p = bc;
        let x = q[0];
        let y = q[1];

        let Q_xx = (p.k1p * p.A + p.k1m * x + p.k3p * x.powi(2) * y + p.k3m * x.powi(3)) / 2.;
        let Q_dia = (-p.k3p * x.powi(2) * y - p.k3m * x.powi(3)) / 2.;
        let Q_yy = (p.k2p * p.B + p.k2m * y + p.k3p * x.powi(2) * y + p.k3m * x.powi(3)) / 2.;

        dbg!(2. * Q_xx, 2. * Q_dia, 2. * Q_yy);
        dbg!(Csq_1);
        dbg!(Csq_2);
    }

    #[test]
    fn check_for_flipped_C() {
        let bc = BrüsselConf {
            A: 0.,
            B: 0.,
            k1m: 0.,
            k1p: 0.,
            k2m: 0.,
            k2p: 0.,
            k3m: 0.,
            k3p: 1.,
        };
        let q = [0.4, 2.1];
        let C = brüssel_C_benedikt(&bc, q);
        dbg!(C);

        const DIM: usize = 2;
        let mut x = [0.; DIM];
        for i in 0..DIM {
            for j in 0..DIM {
                x[i] += C[i][j];
            }
        }
        // These assertions will trigger if C was replaced by its transpose
        assert!(x[0] != 0.);
        assert!(x[1] != 0.);
    }
}

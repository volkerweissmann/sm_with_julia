use crate::brüssel_C_benedikt;
use crate::brüssel_force;
use crate::misc::between_pm_pi;
use crate::save_ensemble_average;
use crate::BrüsselConf;
use crate::StandardEulerMaruyama;
use crate::StandardMulticore;

pub fn rust_theta_travo(stat: [f64; 2], q: [f64; 2]) -> f64 {
    (q[1] - stat[1]).atan2(q[0] - stat[0])
}
pub fn brüssel_diffusion() {
    let q0 = [1.0, 0.2];
    let deltamu = 7.0;
    let time = 4000.;
    let bc = BrüsselConf::like_basile(deltamu);

    // See function print_stationary_points()
    let stat = if deltamu == 7.0 {
        [0.3937395935811696, 2.2884564213759298]
    } else {
        panic!();
    };

    let theta0 = rust_theta_travo(stat, q0);
    let mut q = q0;

    let dt = 1e-3;
    let repeat = 10;

    // let mut count = 0;
    // let mut cur = None;

    let mut thetas = Vec::new();
    for i in 0..(time / dt) as usize {
        let theta = rust_theta_travo(stat, q);
        thetas.push(theta);

        for i in 0..repeat {
            let F = brüssel_force(&bc, q);
            q[0] += F[0] * dt / repeat as f64;
            q[1] += F[1] * dt / repeat as f64;
        }

        // let now = theta > theta0;
        // if let Some(last) = cur {
        //     if now != last {
        //         count += 1;
        //         if count == 2 {
        //             break;
        //         }
        //     }
        // }
        // cur = Some(now);
    }
    dbg!(thetas.len());

    for Omega in [1e2, 1e3, 1e4] {
        let bc = BrüsselConf::like_basile(deltamu);

        let Force = expr_string!(move |q: [f64; 2]| brüssel_force(&bc, q));

        let multi = 2_usize.pow(20);

        save_ensemble_average(
            StandardMulticore::<StandardEulerMaruyama>::default(),
            &format!("brüssel_var_Omega_{:.0}_deltamu_{:.1}.json", Omega, deltamu),
            Omega,
            false,
            q0,
            time,
            multi,
            Force,
            |q| brüssel_C_benedikt(&bc, q),
            |q, t| between_pm_pi(rust_theta_travo(stat, q) - thetas[(t / dt) as usize]).powi(2),
            vec![("deltamu".to_string(), deltamu)],
        );
    }
}

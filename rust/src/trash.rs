extern crate packed_simd;

use packed_simd::f32x4;

fn simd_dot_prod(a: &[f32], b: &[f32]) -> f32 {
    assert_eq!(a.len(), b.len());
    assert!(a.len() % 4 == 0);

    a.chunks_exact(4)
        .map(f32x4::from_slice_unaligned)
        .zip(b.chunks_exact(4).map(f32x4::from_slice_unaligned))
        .map(|(a, b)| a.cos() * b.sin())
        .sum::<f32x4>()
        .sum()
}

fn serial_dot_prod(a: &[f32], b: &[f32]) -> f32 {
    assert_eq!(a.len(), b.len());
    a.iter().zip(b.iter()).map(|v| v.0.cos() * v.1.sin()).sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;
    #[bench]
    fn b_serial(b: &mut Bencher) {
        let ar_a = [1., 2., 3., 4., 5., 6., 7., 8.];
        let ar_b = [1., 2., 3., 4., 5., 6., 7., 8.];
        b.iter(|| serial_dot_prod(&ar_a, &ar_b));
    }
    #[bench]
    fn b_simd(b: &mut Bencher) {
        let ar_a = [1., 2., 3., 4., 5., 6., 7., 8.];
        let ar_b = [1., 2., 3., 4., 5., 6., 7., 8.];
        b.iter(|| simd_dot_prod(&ar_a, &ar_b));
    }
}

#![feature(test)]
extern crate rand;
extern crate test;
use crate::rand::distributions::Distribution;
use rand::{Rng, SeedableRng};
use statrs::distribution::{Continuous, Normal};
use std::f64::consts::PI;

fn simulate_single_trajectory(N: usize, rescale: f64) -> Vec<f64> {
    let mut rng = rand::rngs::SmallRng::from_entropy();
    let n = Normal::new(0.0, 1.0).unwrap();

    let omega = 1000. as f64;
    let space_period = 2. * PI;

    let mut ret = Vec::with_capacity(N);

    //let mut t = 0.;
    let mut x = 0. as f64;
    for _ in 0..N {
        ret.push(x);
        // t += dt;

        // let U_1: f32 = rng.gen();
        // let U_2: f32 = rng.gen();
        // // https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
        // let Z_0 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).cos();
        // //let Z_1 = (-2. * U_1.ln()).sqrt() * (2. * PI * U_2).sin();
        // let W = Z_0;
        let f = 1.2 * x.sin() / rescale;
        let g = (2. * 1. / omega / rescale).sqrt();
        let W = n.sample(&mut rng);
        x += f + g * W;
        if x >= space_period {
            x -= space_period;
        } else if x < 0. {
            x += space_period;
        }
    }
    ret
}

fn main() {
    let multi = 256;

    let rescale = 1000. as f64;
    let time = 2. * PI * 100. * rescale;
    let dt = 1.;
    let N = (time / dt).ceil() as usize;

    let mut sum = vec![0.; N];
    for i in 0..multi {
        let single = simulate_single_trajectory(N, rescale);
        sum.iter_mut()
            .zip(single.iter())
            .for_each(|(sum, single)| *sum += single);
    }
    sum.iter_mut().for_each(|sum| *sum /= multi as f64);
    dbg!(sum);
}

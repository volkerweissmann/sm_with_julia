#!/bin/python
"""
Created on Wed Apr  8 20:25:54 2020

@author: Benedikt
"""

import matplotlib.pyplot as plt
import numpy as np
import datetime
import json


########################################################
########################################################
########### SET OPERATIONAL NUMBER #####################
########################################################

# already used:
# 123456789 10 11 12 13 14 15 16 17 18 19 20 21
op = 21


outputName = "Brusselator_" + str(op) + ".npy"

########################################################
########################################################


## solvers for the fundamental-matrix: RK4 combined with midpoint-rule


def solveM4(dt, x0, y0, x1, y1, m0, Df):
    k1 = np.dot(Df(x0, y0), m0)
    k2 = np.dot(1 / 2 * (Df(x1, y1) + Df(x0, y0)), m0 + dt / 2 * k1)
    k3 = np.dot(1 / 2 * (Df(x1, y1) + Df(x0, y0)), m0 + dt / 2 * k2)
    k4 = np.dot(Df(x1, y1), m0 + dt * k3)
    result = m0 + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
    return result


def solverM4(ni, nf, dt, data0, m0, Df):
    m = m0
    for i in range(ni, nf):
        # # begin temp
        # print("--------")
        # x, y = data0[0][i], data0[1][i]
        # # print(m + dt * np.dot(Df(x, y), m))
        # # print(i * dt, "\n", m)
        # np.dot(
        #     np.array(
        #         [
        #             [-km1 + 2 * kp2 * x * y - 3 * km2 * x ** 2, kp2 * x ** 2],
        #             [-2 * kp2 * x * y + 3 * km2 * x ** 2, -km3 - kp2 * x ** 2],
        #         ]
        #     ),
        #     m,
        # )
        # print(np.dot(Dvec(x, y, kp1, kp2, kp3, km1, km2, km3, cA, cB), m))
        # # end temp
        m = solveM4(
            dt, data0[0][i], data0[1][i], data0[0][i + 1], data0[1][i + 1], m, Df
        )
        # begin temp
        # print(m)
        # end temp
    return m


def solverMt4(ni, nf, dt, data0, m0, Df):
    m = np.zeros([int(nf - ni + 1), 2, 2])
    m[0] = m0
    for i in range(int(nf - ni)):
        m[i + 1] = solveM4(
            dt,
            data0[0, i + ni],
            data0[1, i + ni],
            data0[0, i + ni + 1],
            data0[1, i + ni + 1],
            m[i],
            Df,
        )
    return m


# numerical integration-scheme for the N-matrix


def N(dt, Q, ni, nf, data0, m):
    n = np.zeros([2, 2])
    for i in range(len(m)):
        n = n + dt * np.dot(
            np.dot(np.linalg.inv(m[i]), Q(data0[0, ni + i], data0[1, ni + i])),
            np.transpose(np.linalg.inv(m[i])),
        )
    n = 2 * np.dot(m[-1], n)
    return n


# RK4 combined with Midpoint Rule
def solver4(A, x0, x1, dt, dx0, dp0):
    x = np.array([dx0[0], dx0[1], dp0[0], dp0[1]])
    k1 = np.dot(A(x0[0], x0[1]), x)
    k2 = np.dot(1 / 2 * (A(x0[0], x0[1]) + A(x1[0], x1[1])), x + dt / 2 * k1)
    k3 = np.dot(1 / 2 * (A(x0[0], x0[1]) + A(x1[0], x1[1])), x + dt / 2 * k2)
    k4 = np.dot(A(x1[0], x1[1]), x + dt * k3)
    x = x + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
    dx = np.array([x[0], x[1]])
    dp = np.array([x[2], x[3]])
    return np.array([dx, dp])


# RK4 for the eom for the perturbation


def traj4(A, data0, dt, ni, nf, dx0, dp0):
    n = int(nf - ni) + 1
    dx = dx0
    dp = dp0
    for i in range(n):
        [dx, dp] = solver4(
            A,
            [data0[0][ni + i], data0[1][ni + i]],
            [data0[0][ni + i + 1], data0[1][ni + i + 1]],
            dt,
            dx,
            dp,
        )
    return dx


########################################################
########################################################

## general vectorfield
def vec(x, y, kp1, kp2, kp3, km1, km2, km3, cA, cB):
    result = np.array(
        [
            cA * kp1 - km1 * x + kp2 * x ** 2 * y - km2 * x ** 3,
            cB * kp3 - km3 * y - kp2 * x ** 2 * y + km2 * x ** 3,
        ]
    )
    return result


def Dvec(x, y, kp1, kp2, kp3, km1, km2, km3, cA, cB):
    return np.array(
        [
            [-km1 + 2 * kp2 * x * y - 3 * km2 * x ** 2, kp2 * x ** 2],
            [-2 * kp2 * x * y + 3 * km2 * x ** 2, -km3 - kp2 * x ** 2],
        ]
    )


## estimating the period
def period(data, t):
    if len(data) == len(t):
        n = len(data)
        T = np.zeros([n, 2])
        count = 0
        for i in range(1, n - 1):
            if data[i] < data[i - 1] and data[i] < data[i + 1]:
                T[count] = np.array([i, t[i]])
                count += 1
        T = T[:count]
        return T
    else:
        print("No compatible arrays")
        return "NaN"


js = json.load(open("/data/weissmann/our_brüssel_shape_mu_5.0.json"))
# Benjamin nutzt eine andere Numerierung der Reaktionen als ich.
kp1 = js[0]["k1p"]
km1 = js[0]["k1m"]
kp3 = js[0]["k2p"]
km3 = js[0]["k2m"]
kp2 = js[0]["k3p"]
km2 = js[0]["k3m"]
cA = js[0]["A"]
cB = js[0]["B"]
t = js[1]
temp = js[2]
data0 = [[a[0] for a in js[2]], [a[1] for a in js[2]]]

### load the parameter-set
# t = np.load(outputName)[0]
# data0 = np.load(outputName)[[1, 2]]
m0 = np.eye(2)
# [kp1, kp2, kp3, km1, km2, km3, cA, cB] = np.load("Parameters_" + outputName)

dt = t[1] - t[0]


### functions used for the analysis


def f(x, y):
    return vec(x, y, kp1, kp2, kp3, km1, km2, km3, cA, cB)


def Df(x, y):
    return Dvec(x, y, kp1, kp2, kp3, km1, km2, km3, cA, cB)


def Q(x, y):
    result = np.array(
        [
            [
                1 / 2 * (cA * kp1 + km1 * x + kp2 * x ** 2 * y + km2 * x ** 3),
                -1 / 2 * (kp2 * x ** 2 * y + km2 * x ** 3),
            ],
            [
                -1 / 2 * (kp2 * x ** 2 * y + km2 * x ** 3),
                1 / 2 * (cB * kp3 + km3 * y + kp2 * x ** 2 * y + km2 * x ** 3),
            ],
        ]
    )
    return result


def A(x, y):
    result = np.array(
        [
            [Df(x, y)[0, 0], Df(x, y)[0, 1], 2 * Q(x, y)[0, 0], 2 * Q(x, y)[0, 1]],
            [Df(x, y)[1, 0], Df(x, y)[1, 1], 2 * Q(x, y)[1, 0], 2 * Q(x, y)[1, 1]],
            [0, 0, -Df(x, y)[0, 0], -Df(x, y)[1, 0]],
            [0, 0, -Df(x, y)[0, 1], -Df(x, y)[1, 1]],
        ]
    )
    return result


########################################################
########################################################

print(datetime.datetime.now())


# periods = period(data0[0], t)
# nPeriod = int(periods[-2, 0] - periods[-3, 0])
# lastT = int(periods[-3, 0])

nPeriod = len(t) - 2
lastT = 0

T = nPeriod * dt

m0 = np.eye(2)
m = solverM4(lastT, lastT + nPeriod, dt, data0, m0, Df)

m = np.array(
    [
        [0.2840043145263212, 0.05443890778147772],
        [3.1972228437731465, 0.7569072988531429],
    ]
)
print("MP:", m)

if np.abs(np.linalg.eig(m)[0][0] - 1) < 0.01:
    e2 = np.linalg.eig(m)[1][:, 0]
    f1 = np.linalg.eig(np.transpose(m))[1][:, 0]
    lyap = np.log(np.abs(np.linalg.eig(m)[0][1])) / T
else:
    e2 = np.linalg.eig(m)[1][:, 1]
    f1 = np.linalg.eig(np.transpose(m))[1][:, 1]
    lyap = np.log(np.abs(np.linalg.eig(m)[0][0])) / T

# print(data0[0][lastT + nPeriod], data0[1][lastT + nPeriod])
# e1 = f(data0[0][lastT + nPeriod], data0[1][lastT + nPeriod])
e1 = f(data0[0][lastT], data0[1][lastT])

if np.dot(e1, e2) < 0:
    e2 = -e2

if np.dot(e2, f1) < 0:
    f1 = -f1

e1 = [0.011365665259157566, 0.14948439460760948]
e2 = [-0.7486924853610072, 3.3432278788684435]
f1 = [-0.9758301558984182, -0.21853033391103607]
f2 = [-0.9971220051827705, 0.07581363188959403]

f1 = f1 / np.dot(f1, e1)

print("rescaled:", e1, e2, f1)

# testM = solverMt4(lastT,lastT+nPeriod,dt,data0,m0,Df)

# testN=N(dt,Q,lastT,lastT+nPeriod,data0,testM)

dx = traj4(A, data0, dt, lastT, lastT + nPeriod, np.zeros(2), f1)
print("f1 Nf1:", f1, dx)
# dX=traj4(A,data0,dt,lastT,lastT+nPeriod,np.zeros(2),F2)
# dX2=traj(Df,Q,data0,dt,lastT,lastT+nPeriod,np.zeros(2),F2)

dtdE = -np.dot(f1, dx) / (np.dot(f1, e1))
# dtdE2 = - np.dot(f1,np.dot(testN,f1))/(np.dot(f1,e1))

R = T ** 2 / np.pi / np.abs(dtdE)

# dtdE3 = - np.dot(F1,dX)/(np.dot(F1,e2))
# dtdE4 = - np.dot(F2,dX2)/(np.dot(F2,e2))

## write results to the file
text = open("Brusselator_Results.txt", "a+")
text.write(
    str(op)
    + "\t "
    + "{:.5f}".format(T)
    + "\t "
    + "{:.5f}".format(lyap)
    + "\t "
    + "{:.6f}".format(dtdE)
    + " \t "
    + "{:.6f}".format(R)
    + "\r\n "
)
text.close()

print(datetime.datetime.now())

print("Period:")
print(T)
print("Lyap-Exp:")
print(lyap)
print("dT/dE: eom")
print(dtdE)
print("R")
print(R)
# print("dT/dE: quadratur")
# print(dtdE2)

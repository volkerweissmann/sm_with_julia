
C = (sqrt[2], 0; 0, sqrt[2])

let drdt = mu * r - r.powi(3);
let dthetadt = omega + b * r.powi(2);


0 = mu * r0 - r0^3
r0 = sqrt[mu]

Falls der Radius r0 des limit cycles gleich 1 ist (also mu=1 ist), sollte das das selbe sein, wie in 1D:

C = sqrt[2]
dthetadt = omega + b

Konstante Kraft und Diffusion kennen wir schön:

omega = F_0 = (par_omega+b)
gamma = Q_0/Omega = 1/Omega

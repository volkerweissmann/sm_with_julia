function func()
    #f(x) = 123
    f = x -> 123
    println(f(17))
    f(x) = 321
    println(f(17))
end
func()
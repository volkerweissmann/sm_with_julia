include("../Brüsselator/utils.jl")
using Plots
function main()
    N = 1000
    ∆T = 1.0

    L = 17
    βFv = 0.2*N/L
    D = 0.2/N^2*L^2

    k_m = D/L^2*N^2 * exp(-βFv*L/N/2)
    k_p = D/L^2*N^2 * exp(+βFv*L/N/2)

    cyc(i) = cyclic(N, i)
    E = zeros(N,N)
    for i in 1:N
        a = k_m
        c = k_p
        b = -a-c
        E[cyc(i-1), i] = ∆T*a
        E[    i,    i] = ∆T*b+1
        E[cyc(i+1), i] = ∆T*c
    end

    println("matrix matrix...")
    flush(stdout);
    matrix_exp = 256
    E = E^matrix_exp
    println("matrix vector...")
    flush(stdout);

    P = zeros(N)
    P[Int(round(N/2))] = 1

    numsteps = 1000
    NC = zeros(numsteps)
    #AC = zeros(numsteps)

    for i in 1:numsteps
        NC[i] = P[Int(round(N/2))]

        t = (i-1)*∆T*matrix_exp
        # AC[i] = 1/N
        # other = 1/N
        # for k in 1:1000
        #     AC[i] += 2/N*cos(2*pi*k*D*βFv/L*t)*exp(-(2*pi*k)^2*D/L^2*t)
        #     other += 2/N*cos(2*pi*k*D*βFv/L*t)*exp(-(2*pi*k)^2*2*D/L^2*t)
        # end
        # println(NC[i]-1/N, " ", AC[i]-1/N, " ", other)

        P = E*P
    end

    # pplot = plot(NC, yaxis=:log)
    # plot!(pplot, AC)
    # display(pplot)

    nothing
end

main()
ENV["JULIA_DEBUG"] = Main
include("../gaspard/simu.jl")
include("../Brüsselator/attract.jl")
import Zygote
using QuadGK

function build_L_from_F_and_Q(dim, Length, ε, F, Q_arg)
    ∆x = Length/dim
    Q(x) = Q_arg(x) + x*0
    v = State(1,3)
    L = CyclicArray(zeros(dim, dim))
    for i in 1:dim
        L[i+1,i] = (+F((i+1)*∆x) - 2*ε*Q'((i+1)*∆x)) / (2*∆x) + ε*Q((i+1)∆x)/∆x^2
        L[i-1,i] = (-F((i+1)*∆x) + 2*ε*Q'((i+1)*∆x)) / (2*∆x) + ε*Q((i+1)∆x)/∆x^2
        L[i,i] = - L[i+1,i] - L[i-1,i]
    end
    L
end

function calc_my_R(Length, ε, F, Q)
    (quadgk(x -> 1/F(x), 0, Length)[1])^2/(2*pi*ε * quadgk(x-> Q(x)/F(x)^3, 0, Length)[1])
end

function calc_f_border(Length, ε, F, Q)
    quadgk(x -> F(x)/Q(x), 0, Length)[1]/(2*pi*ε)
end

function verify(F, Q)
    Length = 2
    ε = 1e-2

    dtmat = build_L_from_F_and_Q(1000, Length, ε, F, Q)

    R_mat = get_rating(dtmat.ar).R
    R_myint = calc_my_R(Length, ε, F, Q)

    display(R_mat/R_myint)
end

function plots_for_seifert()
    # Different ε or Q should yield the same R/f TODO is there a way to check statically check this
    ε = 1
    Q(x) = 1
    Length = 1
    # a + b cos
    # max = a+b
    # min = a-b
    # a = (max + min)/2
    # b = (max - min)/2
    # max/min = (a+b)/(a-b)
    # (a-b) max/min = (a+b)
    # a * (max/min - 1) = b * (max/min + 1 )

    ratio = 2

    F(ratio) = x -> 1 + (ratio - 1)/(ratio + 1) * cos(2 * pi * x)
    v(ratio) = calc_my_R(Length, ε, F(ratio), Q) / calc_f_border(Length, ε, F(ratio), Q)

    ratios = LinRange(1, 7, 1000)
    p = plot(ratios, v.(ratios), xaxis=("maximum/minimum"), yaxis=("R/f"))
    savefig(p, "MARKER.pdf")


    # mins = LinRange(0.01, 4, 20)
    # maxs = LinRange(0.01, 4, 10)
    # data = zeros(length(mins), length(maxs))
    # for i in 1:length(mins)
    #     for j in 1:length(maxs)
    #         min = mins[i]
    #         max = maxs[j]
    #         F = x -> (max + min)/2 + (max - min)/2*cos(2 * pi * x)
    #         data[i,j] = calc_my_R(Length, ε, F, Q) / calc_f_border(Length, ε, F, Q)
    #     end
    # end
    # display(data)

    # #data = rand(21,100)
    # heatmap(1:size(data,2),
    #     1:size(data,1), data,
    #     c=cgrad([:blue, :white,:red, :yellow]),
    #     xlabel="min", ylabel="max",
    #     title="R/f for F = (max + min)/2 + (max - min)/2*cos, Q=const")
end

plots_for_seifert()

# Temp = 2
# F(x) = 1
# Q(x) = Temp

# verify(F, Q)
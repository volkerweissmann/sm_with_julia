ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "../Brüsselator/paths.jl"
using PrettyPrinting
import Zygote

function Wp_macro(bc::our_brüssel_conf, react::Reaction, q)
    if react.num == 1
        if react.sgn == 1
            return bc.k1p * bc.A
        elseif react.sgn == -1
            return bc.k1m * q[1]
        end
    elseif react.num == 2
        if react.sgn == 1
            return bc.k2p * bc.B
        elseif react.sgn == -1
            return bc.k2m * q[2]
        end
    elseif react.num == 3
        if react.sgn == 1
            return bc.k3p * q[1]^2 * q[2]
        elseif react.sgn == -1
            return bc.k3m * q[1]^3
        end
    end
    error("unreachable")
end

# function funcS()
#     ret = SVector(0,0)
#     ret = ret + SVector(123, 321)
#     return ret
# end
# function funcM()
#     ret = MVector(0,0)
#     ret += MVector(123, 321)
#     return ret
# end
# @benchmark funcS()
# @benchmark funcM()

function calc_brüssel_F(bc, q)
    ret = SVector(0.0, 0.0)
    ret = ret + brüssel_nu(r1p) * (Wp_macro(bc, r1p, q) - Wp_macro(bc, r1m, q))
    ret = ret + brüssel_nu(r2p) * (Wp_macro(bc, r2p, q) - Wp_macro(bc, r2m, q))
    ret = ret + brüssel_nu(r3p) * (Wp_macro(bc, r3p, q) - Wp_macro(bc, r3m, q))
    # this works too, but the loop does not get unrolled, which kills the performance. Consider using https://github.com/cstjean/Unrolled.jl
    # for react in all_reactions
    #     ret = ret + brüssel_nu(react) * Wp_macro(bc, react, q)
    # end
    return ret
end

function calc_brüssel_Q(bc, q)
    ret = SMatrix{2,2}(0.0, 0.0, 0.0, 0.0)
    ret = ret + (brüssel_nu(r1p) * brüssel_nu(r1p)') * ((Wp_macro(bc, r1p, q) + Wp_macro(bc, r1m, q)) / 2)
    ret = ret + (brüssel_nu(r2p) * brüssel_nu(r2p)') * ((Wp_macro(bc, r2p, q) + Wp_macro(bc, r2m, q)) / 2)
    ret = ret + (brüssel_nu(r3p) * brüssel_nu(r3p)') * ((Wp_macro(bc, r3p, q) + Wp_macro(bc, r3m, q)) / 2)
    # this works too, but the loop does not get unrolled, which kills the performance
    # for react in all_reactions
    #     ret = ret + brüssel_nu(react) * Wp_macro(bc, react, q)
    # end
    return ret
end

function other_calc_brüssel_Q(bc, q)
    ret = [0 0; 0 0]
    ret += [1 0; 0 0] * ((Wp_macro(bc, r1p, q) + Wp_macro(bc, r1m, q)) / 2)
    ret += [0 0; 0 1] * ((Wp_macro(bc, r2p, q) + Wp_macro(bc, r2m, q)) / 2)
    ret += [1 -1; -1 1] * ((Wp_macro(bc, r3p, q) + Wp_macro(bc, r3m, q)) / 2)
end

# @code_llvm opttest()
# ;  @ /home/volker/Sync/git/sm_with_julia/gaspard/simu.jl:58 within `opttest'
# define void @julia_opttest_2318([1 x [2 x double]]* noalias nocapture sret %0) {
# top:
# ;  @ /home/volker/Sync/git/sm_with_julia/gaspard/simu.jl:60 within `opttest'
#   %.sroa.0.sroa.0.0..sroa.0.0..sroa_cast1.sroa_idx = getelementptr inbounds [1 x [2 x double]], [1 x [2 x double]]* %0, i64 0, i64 0, i64 0
#   %1 = bitcast double* %.sroa.0.sroa.0.0..sroa.0.0..sroa_cast1.sroa_idx to <2 x double>*
#   store <2 x double> <double 0x408F3BFFFFFFEED2, double 0x40A76FF85D01FA5A>, <2 x double>* %1, align 8
#   ret void
# }

# @benchmark opttest()
# BenchmarkTools.Trial:
#   memory estimate:  0 bytes
#   allocs estimate:  0
#   --------------
#   minimum time:     3.255 ns (0.00% GC)
#   median time:      3.268 ns (0.00% GC)
#   mean time:        3.495 ns (0.00% GC)
#   maximum time:     200.014 ns (0.00% GC)
#   --------------
#   samples:          10000
#   evals/sample:     1000
function opttest()
    bc = basile_coeffs(10000, 4.1)
    calc_brüssel_Q(bc, (0.5, 0.3))
end
# display(@code_llvm opttest())
# display(@benchmark opttest())

function our_brüssel_F(p, q)
    [p.k1p * p.A - p.k1m * q[1] + p.k3p * q[1]^2 * q[2] - p.k3m * q[1]^3,
        p.k2p * p.B - p.k2m * q[2] - p.k3p * q[1]^2 * q[2] + p.k3m * q[1]^3]
end

function tang_our_brüssel_Q(p, q)
    x = q[1]
    y = q[2]

    Fx = p.k1p * p.A - p.k1m * x + p.k3p * x^2 * y - p.k3m * x^3
    Fy = p.k2p * p.B - p.k2m * y - p.k3p * x^2 * y + p.k3m * x^3

    N = sqrt(Fx^2 + Fy^2)

    fx = Fx / N
    fy = Fy / N

    a = fx * (p.k1p * p.A + p.k1m * x + p.k3p * x^2 * y + p.k3m * x^3) + fy * (p.k2p * p.B + p.k2m * y + p.k3p * x^2 * y + p.k3m * x^3)

    Q = [fx^2 fx*fy; fx*fy fy^2] * abs(a) / 2
    return Q
end

function verify_proj_Q()
    for deltamu in [3.3, 3.9, 4.1, 6.0]
        for x in [0.2, 0.4, 0.8, 1.4, 2.1]
            for y in [0.2, 0.4, 0.8, 1.4, 2.1]
                bc = basile_coeffs(0, deltamu)
                bc = our_brüssel_conf(OMEGA = 0, A = 1.0, B = 1.0, k1p = 0.0, k1m = 1.0, k2p = 0.0, k2m = 0.0, k3p = 0.0, k3m = 0.0)::gen_brüssel_conf

                q = [x, y]

                F = our_brüssel_F(bc, q)
                C2d = our_brüssel_C(bc, q)
                e_F = F / norm(F)
                c0 = C2d[1, 1] * e_F[1] + C2d[2, 1] * e_F[2]
                c1 = C2d[1, 2] * e_F[1] + C2d[2, 2] * e_F[2]
                C_diff = sqrt(c0^2 + c1^2)
                C = [e_F[1] 0; e_F[2] 0] * C_diff
                Q_a = 1 / 2 * C * transpose(C)

                Q_b = tang_our_brüssel_Q(bc, q)
                @assert isapprox(Q_a, Q_b)
            end
        end
    end
end

function our_brüssel_Q(p, q)
    Qxx = (p.k1p * p.A + p.k1m * q[1] + p.k3p * q[1]^2 * q[2] + p.k3m * q[1]^3) / 2
    Qdia = (-p.k3p * q[1]^2 * q[2] - p.k3m * q[1]^3) / 2
    Qyy = (p.k2p * p.B + p.k2m * q[2] + p.k3p * q[1]^2 * q[2] + p.k3m * q[1]^3) / 2
    [Qxx Qdia; Qdia Qyy]
end

function our_brüssel_C(p, q)
    x = q[1]
    y = q[2]

    Q = our_brüssel_Q(p, q)

    m = sqrt(2.0 / Q[1, 1])
    C_x1 = m * Q[1, 1]
    C_x2 = 0.0
    C_y1 = m * Q[1, 2]
    C_y2 = m * sqrt(Q[1, 1] * Q[2, 2] - Q[1, 2]^2)
    return [C_x1 C_x2; C_y1 C_y2]
end

function our_brüssel_C_T(p, q)
    return transpose(our_brüssel_C(p, q))
end


function verify_our_brüssel_Q()
    for deltamu in [3.3, 3.9, 4.1, 6.0]
        for x in [0.2, 0.4, 0.8, 1.4, 2.1]
            for y in [0.2, 0.4, 0.8, 1.4, 2.1]
                p = basile_coeffs(0, deltamu)
                error = our_brüssel_Q(p, [x, y]) - calc_brüssel_Q(p, [x, y])
                @assert abs(maximum(error)) < 1e-14
                @assert abs(minimum(error)) < 1e-14

                error = our_brüssel_C(p, [x, y]) * transpose(our_brüssel_C(p, [x, y])) / 2 - calc_brüssel_Q(p, [x, y])
                @assert abs(maximum(error)) < 1e-14
                @assert abs(minimum(error)) < 1e-14
            end
        end
    end
end

function gaspard_brüssel_F(p, q)
    x = q[1]
    y = q[2]
    # Quelle: (74) und (75) im correlation time paper
    [p.k1 - p.k2 * x + p.k3 * x^2 * y - p.k4 * x,
        p.k2 * x - p.k3 * x^2 * y]
end

# See cubic_hopf_bifurcation_force_a
function steward_landau_F(p, q)
    x, y = q
    r = sqrt(x^2 + y^2)
    theta = atan(y, x)

    # let drdt = mu * r - a * r.powi(3);
    # let dthetadt = omega + b * r.powi(2);

    drdt = p.a * r - p.b * r^3
    dthetadt = p.c + p.d * r^2

    dxdr = cos(theta)
    dxdtheta = -sin(theta) * r
    dydr = sin(theta)
    dydtheta = cos(theta) * r

    dxdt = drdt * dxdr + dthetadt * dxdtheta
    dydt = drdt * dydr + dthetadt * dydtheta

    [dxdt, dydt]
end

function steward_landau_Q(p, q)
    [1 0; 0 1]
end

function gaspard_brüssel_Q(p, q)
    x = q[1]
    y = q[2]
    # Quelle: (76), (77), (78) und TABLE I im correlation time paper
    Qxx = (p.k1 + p.k2 * x + p.k3 * x^2 * y + p.k4 * x) / 2
    Qdia = (-p.k2 * x - p.k3 * x^2 * y) / 2
    Qyy = (p.k2 * x + p.k3 * x^2 * y) / 2
    [Qxx Qdia; Qdia Qyy]
end

function our_brüssel_hamilton(bc, q, p)
    H = dot(p, our_brüssel_Q(bc, q) * p) + dot(p, our_brüssel_F(bc, q))
    return H
end

function tang_our_brüssel_hamilton(bc, q, p)
    H = dot(p, tang_our_brüssel_Q(bc, q) * p) + dot(p, our_brüssel_F(bc, q))
    return H
end

function gaspard_brüssel_hamilton(bc, q, p)
    H = dot(p, gaspard_brüssel_Q(bc, q) * p) + dot(p, gaspard_brüssel_F(bc, q))
    return H
end

function get_limit_dgl(bc::gen_brüssel_conf)
    if typeof(bc) == our_brüssel_conf || typeof(bc) == tang_our_brüssel_conf
        return (u, p, t) -> our_brüssel_F(bc, u)
    elseif typeof(bc) == gaspard_brüssel_conf
        return (u, p, t) -> gaspard_brüssel_F(bc, u)
    elseif typeof(bc) == steward_landau_conf
        return (u, p, t) -> steward_landau_F(bc, u)
    end
    error("bad type")
end

function our_brüssel_hamilton_dqpdt(bc, q, p)
    H(q, p) = our_brüssel_hamilton(bc, q, p)
    grad = Zygote.gradient(H, q, p)
    dqdt = +grad[2]
    dpdt = -grad[1]
    return vcat(dqdt, dpdt)
end

function tang_our_brüssel_hamilton_dqpdt(bc, q, p)
    H(q, p) = tang_our_brüssel_hamilton(bc, q, p)
    grad = Zygote.gradient(H, q, p)
    dqdt = +grad[2]
    dpdt = -grad[1]
    return vcat(dqdt, dpdt)
end

function gaspard_brüssel_hamilton_dqpdt(bc, q, p)
    H(q, p) = gaspard_brüssel_hamilton(bc, q, p)
    grad = Zygote.gradient(H, q, p)
    dqdt = +grad[2]
    dpdt = -grad[1]
    return vcat(dqdt, dpdt)
end

function get_hamilton_dgl(bc::gen_brüssel_conf)
    if typeof(bc) == our_brüssel_conf
        return (u, p, t) -> our_brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])
    elseif typeof(bc) == tang_our_brüssel_conf
        return (u, p, t) -> tang_our_brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])
    elseif typeof(bc) == gaspard_brüssel_conf
        return (u, p, t) -> gaspard_brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])
    end
    throw(ValueError)
end


function brüssel_sigma_without_omega(bc, q)
    x, y = q
    [
        [sqrt(bc.k1p * bc.A + bc.k1m * x), 0],
        [0, sqrt(bc.k2p * bc.B + bc.k2m * y)],
        [sqrt(bc.k3p * x^2 * y + bc.k3m * x^3), -sqrt(bc.k3p * x^2 * y + bc.k3m * x^3)]
    ]
end

function verify_sigmas()
    for deltamu in [3.3, 3.9, 4.1, 6.0]
        for x in [0.2, 0.4, 0.8, 1.4, 2.1]
            for y in [0.2, 0.4, 0.8, 1.4, 2.1]
                bc = basile_coeffs(0, deltamu)
                q = [x, y]
                sigmas = brüssel_sigma_without_omega(bc, q)
                @assert sigmas[1] == brüssel_nu(r1p) * sqrt(Wp_macro(bc, r1p, q) + Wp_macro(bc, r1m, q))
                @assert sigmas[2] == brüssel_nu(r2p) * sqrt(Wp_macro(bc, r2p, q) + Wp_macro(bc, r2m, q))
                @assert sigmas[3] == brüssel_nu(r3p) * sqrt(Wp_macro(bc, r3p, q) + Wp_macro(bc, r3m, q))
            end
        end
    end
end

# macro rpos(ar, x, y)
#     :($(esc(ar))[[$(esc(x)),$(esc(y))]-ranges[:,1]+[1,1]])
# end

function timestep(bc, ranges, P, deltaP)
    dt = 0.02 / bc.OMEGA

    xmax = ranges[1, 2] - ranges[1, 1] + 1
    ymax = ranges[2, 2] - ranges[2, 1] + 1

    for ix = 1:xmax
        P[ix, 1] = 0
        P[ix, end] = 0
        deltaP[ix, 1] = 0
        deltaP[ix, end] = 0
    end
    for iy = 1:ymax
        P[1, iy] = 0
        P[end, iy] = 0
        deltaP[1, iy] = 0
        deltaP[end, iy] = 0
    end

    for iy = 2:ymax-1
        for ix = 2:xmax-1
            qx = ix + ranges[1, 1]
            qy = iy + ranges[2, 1]
            deltaP[ix, iy] = dt * (
                +P[ix-1, iy] * bc.OMEGA * bc.k1p * bc.A
                +
                P[ix+1, iy] * bc.k1m * (qx + 1)
                -
                P[ix, iy] * bc.OMEGA * bc.k1p * bc.A
                -
                P[ix, iy] * bc.k1m * qx + P[ix, iy-1] * bc.OMEGA * bc.k2p * bc.B
                + P[ix, iy+1] * bc.k2m * (qy + 1)
                -
                P[ix, iy] * bc.OMEGA * bc.k2p * bc.B
                -
                P[ix, iy] * bc.k2m * qy + P[ix-1, iy+1] * bc.k3p / bc.OMEGA^2 * (qx - 1) * (qx - 2) * (qy + 1)
                + P[ix+1, iy-1] * bc.k3m / bc.OMEGA^2 * (qx + 1) * qx * (qx - 1)
                -
                P[ix, iy] * bc.k3p / bc.OMEGA^2 * qx * (qx - 1) * qy
                -
                P[ix, iy] * bc.k3m / bc.OMEGA^2 * qx * (qx - 1) * (qx - 2)
            )
        end
    end
    return deltaP
end

function center_of_mass(P)
    ranges = P.ranges
    center = [0, 0]
    for y = ranges[2, 1]:ranges[2, 2]
        for x = ranges[1, 1]:ranges[1, 2]
            @assert rpos(P, x, y) >= 0
            center += [x, y] * rpos(P, x, y)
        end
    end
    center /= sum(P.data)
    return center
end

function outer_ring(ranges, P)
    xmax = ranges[1, 2] - ranges[1, 1] + 1
    ymax = ranges[2, 2] - ranges[2, 1] + 1

    lowX = 0.0
    highX = 0.0
    lowY = 0.0
    highY = 0.0

    for ix = 1:xmax
        lowY += P[ix, 1]
        highY += P[ix, end]
    end
    for iy = 1:ymax
        lowX += P[1, iy]
        highX += P[end, iy]
    end
    # println("outer_ring ", tot*length(P)/ ( ranges[2,2]-ranges[2,1] + ranges[1,2]-ranges[1,1] ) /2 )
    print("lowX: ", lowX * length(P) / (ranges[1, 2] - ranges[1, 1]), " ")
    print("highX: ", highX * length(P) / (ranges[1, 2] - ranges[1, 1]), " ")
    print("lowY: ", lowY * length(P) / (ranges[2, 2] - ranges[2, 1]), " ")
    print("highY: ", highY * length(P) / (ranges[2, 2] - ranges[2, 1]), " ")
    println()
end

function recenter(P, newcenter)
    # display(newcenter)
    diff = (P.ranges[:, 1] + P.ranges[:, 2]) / 2 - newcenter
    diff = round.(diff)
    # display(diff)
    # display(P.ranges)
    newP = zeroWindow(SArray{Tuple{2,2},Int64}(P.ranges[1, 1] - diff[1], P.ranges[2, 1] - diff[2], P.ranges[1, 2] - diff[1], P.ranges[2, 2] - diff[2]))

    for y = maximum((P.ranges[2, 1], newP.ranges[2, 1]))+1:minimum((P.ranges[2, 2], newP.ranges[2, 2]))-1
        for x = maximum((P.ranges[1, 1], newP.ranges[1, 1]))+1:minimum((P.ranges[1, 2], newP.ranges[1, 2]))-1
            wpos(newP, x, y, rpos(P, x, y))
        end
    end

    return newP
end

mutable struct Window
    ranges::SArray{Tuple{2,2},Int64}
    data::Array{Float64,2}
end

function rpos(win::Window, x::Int64, y::Int64)
    return win.data[x-win.ranges[1, 1]+1, y-win.ranges[2, 1]+1]
end

function wpos(win::Window, x::Int64, y::Int64, value)
    win.data[x-win.ranges[1, 1]+1, y-win.ranges[2, 1]+1] = value
end

function zeroWindow(ranges::SArray{Tuple{2,2},Int64})
    Window(ranges, zeros(1 + ranges[1, 2] - ranges[1, 1], 1 + ranges[2, 2] - ranges[2, 1]))
end

function Base.:+(lhs::Window, rhs::Window)
    @assert lhs.ranges == rhs.ranges
    Window(lhs.ranges, lhs.data + rhs.data)
end

# 1.0000000000000002 [199.34712404542057, 3000.1722391913313]
# ...
# 0.9999141951606907 [227.94075916857219, 3038.823423160876]
# ...
# 0.980876402569118 [251.32738025018548, 3027.1017953223272]

function simuMain()
    bc = basile_coeffs(200, 4.1)
    # start = [200, 400]
    start = [29, 448]
    winsize = 512
    P = zeroWindow(SArray{Tuple{2,2},Int64}(start[1] - winsize / 2, start[2] - winsize / 2, start[1] + winsize / 2 - 1, start[2] + winsize / 2 - 1))
    # P = zeroWindow(SA[2 258; 2162 2418])
    P.data[Int(round((P.ranges[1, 2] - P.ranges[1, 1]) / 2)), Int(round((P.ranges[2, 2] - P.ranges[2, 1]) / 2))] = 1.0
    deltaP = zeros(1 + P.ranges[1, 2] - P.ranges[1, 1], 1 + P.ranges[2, 2] - P.ranges[2, 1])

    centerX = []
    centerY = []
    for _ = 1:1000
        for _ = 1:1000
            P.data += timestep(bc, P.ranges, P.data, deltaP)
        end
        center = center_of_mass(P)
        # println(sum(P.data), " ", center)
        P = recenter(P, center)
        println(sum(P.data), " ", center)
        flush(stdout)
        push!(centerX, center[1])
        push!(centerY, center[2])
    end
    println(center_of_mass(P))

    plot(centerX, centerY)

    # for x in ranges[1,1]:ranges[1,2]
    #     for y in ranges[2,1]:ranges[2,2]
    #         if P[x,y] > 1e-3
    #             println(P[x,y])
    #         end
    #     end
    # end
    # pprint(open("testout.txt", "w"), P)
end

# [254.11488586074094, 503.9664557482025]
# 16.074516 seconds (1.87 M allocations: 4.069 GiB, 0.84% gc time)
# @time simuMain()

verify_our_brüssel_Q()
verify_sigmas()
verify_proj_Q()

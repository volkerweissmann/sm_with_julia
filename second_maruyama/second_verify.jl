ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "main.jl"
using LinearAlgebra

function Q_to_C(Q)
    @assert Q[1, 2] == Q[2, 1]
    m = sqrt(2.0 / Q[1, 1])
    C_x1 = m * Q[1, 1]
    C_x2 = 0.0
    C_y1 = m * Q[1, 2]
    C_y2 = m * sqrt(Q[1, 1] * Q[2, 2] - Q[1, 2]^2)
    C = [C_x1 C_x2; C_y1 C_y2]

    @assert isapprox(maximum(Q - 1 / 2 * C * transpose(C)), 0, atol = 1e-9)
    @assert isapprox(minimum(Q - 1 / 2 * C * transpose(C)), 0, atol = 1e-9)
    return C
end

function Q_project(Q, e)
    e = e / norm(e)
end

function second_verify_C_Q()
    OMEGA = 1000
    dt = 1e-3
    F = [2.0, 1.0]
    pos = dat.Pos(OMEGA * 1.3, OMEGA * 2.1)

    sum_a = 0
    for i = 1:1000000
        r1p = Poisson(dt * OMEGA * k1 * A)
        r1m = Poisson(dt * km1 * pos.nx)

        r2p = Poisson(dt * OMEGA * k2 * B)
        r2m = Poisson(dt * km2 * pos.ny)

        r3p = Poisson(dt * k3 * pos.nx * (pos.nx - 1) * pos.ny / OMEGA^2)
        r3m = Poisson(dt * k3 * pos.nx * (pos.nx - 1) * (pos.nx - 2) / OMEGA^2)

        r1p = rand(r1p)
        r1m = rand(r1m)
        r2p = rand(r2p)
        r2m = rand(r2m)
        r3p = rand(r3p)
        r3m = rand(r3m)

        temp = dat.Pos(0, 0)

        temp.nx += r1p - r1m + r3p - r3m
        temp.ny += r2p - r2m - r3p + r3m

        avg = [dt * OMEGA * k1 * A - dt * km1 * pos.nx + dt * k3 * pos.nx * (pos.nx - 1) * pos.ny / OMEGA^2 - dt * k3 * pos.nx * (pos.nx - 1) * (pos.nx - 2) / OMEGA^2,
            dt * OMEGA * k2 * B - dt * km2 * pos.ny - dt * k3 * pos.nx * (pos.nx - 1) * pos.ny / OMEGA^2 + dt * k3 * pos.nx * (pos.nx - 1) * (pos.nx - 2) / OMEGA^2]

        diff = ([temp.nx, temp.ny] - avg) / OMEGA

        proj = dot(diff, F) / norm(F)
        sum_a += proj^2
    end
    @debug sum_a

    sum_b = 0
    rn = Normal(0, 1)
    for i = 1:1000000
        C = our_brüssel_C(basile_coeffs(0, 5.3), [1.3, 2.1])
        diff = [0, 0]
        zetas = rand(rn, 2)

        diff += sqrt(dt / OMEGA) * C * zetas

        proj = dot(diff, F) / norm(F)
        sum_b += proj^2
    end
    @debug sum_b

    Q_to_C(our_brüssel_Q(basile_coeffs(0, 5.3), [1.3, 2.1]))
end

#second_verify_C_Q()


function original_problem()
    a = 7
    b = 3
    c = 5

    F = [2, 1]

    N = 3
    nus = [[1, 0], [0, 1], [1, -1]]
    wps = [a, b, c]

    @assert length(nus) == N
    @assert length(wps) == N
    Q = [0 0; 0 0]
    for i = 1:N
        nu = nus[i]
        wp = wps[i]
        Q += (nu * nu') * wp / 2
    end

    C = Q_to_C(Q)
    @debug "" C Q

    sum_a = 0
    sum_b = 0
    rn = Normal(0, 1)
    for u = 1:1000000

        zetas = rand(rn, N)
        diff = [0, 0]
        for i = 1:N
            diff += sqrt(wps[i]) * nus[i] * zetas[i]
        end
        proj = dot(diff, F) / norm(F)
        sum_a += proj^2


        zetas = rand(rn, 2)
        diff = C * zetas
        proj = dot(diff, F) / norm(F)
        sum_b += proj^2
    end

    @debug sum_a sum_b
    return Q
end

function simpler_problem()
    c = 1

    F = [2, 1]

    N = 1
    nus = [[1, -1]]
    wps = [c]

    @assert length(nus) == N
    @assert length(wps) == N
    Q = [0 0; 0 0]
    for i = 1:N
        nu = nus[i]
        wp = wps[i]
        Q += (nu * nu') * wp / 2
    end

    C = Q_to_C(Q)
    @debug "" C Q


    sum_a = 0
    sum_b = 0
    rn = Normal(0, 1)
    for u = 1:1000000

        zetas = rand(rn, N)
        diff = [0, 0]
        for i = 1:N
            diff += sqrt(wps[i]) * nus[i] * zetas[i]
        end
        proj = dot(diff, F) / norm(F)
        sum_a += proj^2


        zetas = rand(rn, 2)
        diff = C * zetas
        proj = dot(diff, F) / norm(F)
        sum_b += proj^2
    end

    @debug sum_a sum_b
    return Q
end


#original_problem()
#second_verify_C_Q()

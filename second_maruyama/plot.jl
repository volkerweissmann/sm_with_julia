ENV["JULIA_DEBUG"] = "all"
import Plots
import JSON


js = JSON.parsefile(ENV["HOME"] * "/Sync/data/from_itp2/second_maruyama_0.json")

@debug typeof(js)

Plots.plot(js)
Plots.savefig("temp.pdf")

ENV["JULIA_DEBUG"] = "all"
using Random, Distributions
using StatProfilerHTML
import JSON
#Random.seed!()

module dat
using Parameters
mutable struct Pos
    nx::Int
    ny::Int
end

@with_kw mutable struct Config
    OMEGA::Int
end
end

deltamu = 5.3
dt = 0.01

k1 = 0.1
k2 = 0.1
km1 = 1
k3 = 1
A = 1
B = 3

km2 = exp(-deltamu) * km1 * k2 * B / k1
@assert isapprox(deltamu, log(km1 * k2 * B / (km2 * k1 * A)))

function evil_force(pos::dat.Pos)
    q = [pos.nx / OMEGA, pos.ny / OMEGA]
    [k1 * A - km1 * q[1] + k3 * q[1]^2 * q[2] - k3 * q[1]^3,
        k2 * B - km2 * q[2] - k3 * q[1]^2 * q[2] + k3 * q[1]^3]
end

function force_timestep!(pos::dat.Pos)
    F = evil_force(pos)
    pos.nx += round(dt * OMEGA * F[1])
    pos.ny += round(dt * OMEGA * F[2])
end

function timestep!(conf, pos::dat.Pos)
    OMEGA = conf.OMEGA

    r1p = Poisson(dt * OMEGA * k1 * A)
    r1m = Poisson(dt * km1 * pos.nx)

    r2p = Poisson(dt * OMEGA * k2 * B)
    r2m = Poisson(dt * km2 * pos.ny)

    r3p = Poisson(dt * k3 * pos.nx * (pos.nx - 1) * pos.ny / OMEGA^2)
    r3m = Poisson(dt * k3 * pos.nx * (pos.nx - 1) * (pos.nx - 2) / OMEGA^2)

    r1p = rand(r1p)
    r1m = rand(r1m)
    r2p = rand(r2p)
    r2m = rand(r2m)
    r3p = rand(r3p)
    r3m = rand(r3m)

    pos.nx += r1p - r1m + r3p - r3m
    pos.ny += r2p - r2m - r3p + r3m

    if pos.nx < 3
        pos.nx = 3
    end
    if pos.ny < 3
        pos.ny = 3
    end
end

evil_stationary = [0.3647382399092852, 2.354742843556741]
evil_limit = [
    0.9234344275609,
    2.3541330787795767,
]

function find_limit_cycle()
    macroconf = dat.Config(OMEGA = 1000000)
    OMEGA = macroconf.OMEGA
    p = dat.Pos(3, 3)
    for u = 1:10000000
        timestep!(macroconf, p)
    end

    while atan(p.ny - evil_stationary[2] * OMEGA, p.nx - evil_stationary[1] * OMEGA) < 0
        timestep!(macroconf, p)
    end
    while atan(p.ny - evil_stationary[2] * OMEGA, p.nx - evil_stationary[1] * OMEGA) > 0
        timestep!(macroconf, p)
    end

    return [p.nx / OMEGA, p.ny / OMEGA]
end

function main()
    #plimit = find_limit_cycle()
    plimit = [0.930471, 2.345987]
    conf = dat.Config(OMEGA = 10000)

    TN = 400000

    @assert length(ARGS) == 1
    @assert ARGS[1] != ""
    @assert !isdir(ARGS[1])
    @assert !isfile(ARGS[1])

    sum = zeros(TN)
    for i = 1:2^13

        println(i)
        p = dat.Pos(round(conf.OMEGA * plimit[1]), round(conf.OMEGA * plimit[2]))
        for u = 1:TN
            timestep!(conf, p)
            sum[u] += p.nx
        end
    end

    jsout = open(ARGS[1], "w")
    JSON.print(jsout, sum)
    close(jsout)
end
#main()

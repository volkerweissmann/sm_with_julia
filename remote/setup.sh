#!/bin/bash

if [ "$(cat /etc/hostname)" != "hydra" ]; then
  echo "not running on the hydra"
  exit 1
fi

echo "running on the hydra"

curl https://julialang-s3.julialang.org/bin/linux/x64/1.6/julia-1.6.2-linux-x86_64.tar.gz -o julia.tar.gz

tar -zxvf  julia.tar.gz

julia-1.6.2/bin/julia setup.jl

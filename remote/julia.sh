#!/bin/bash

if [ "$(cat /etc/hostname)" != "hydra" ]; then
  echo "not running on the hydra"
  exit 1
fi

echo "running on the hydra"
JULIA_EXCLUSIVE=1 julia-1.6.2/bin/julia langevin/langevin.jl

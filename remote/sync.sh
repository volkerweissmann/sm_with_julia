#!/bin/bash

while true ; do
    rsync -r setup.jl ../Brüsselator ../monocycle ../gaspard ../langevin weissmann@endor.theo2.physik.uni-stuttgart.de:/home/weissmann
    sleep 3s
done

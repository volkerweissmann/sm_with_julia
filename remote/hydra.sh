#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usuage: ./hydra.sh payload.sh"
    exit 1
fi

rsync $1 weissmann@endor.theo2.physik.uni-stuttgart.de:payload.sh
rsync -r setup.jl ../Brüsselator ../monocycle ../gaspard ../langevin weissmann@endor.theo2.physik.uni-stuttgart.de:/home/weissmann
echo "finished rsync"

if [ "$2" == "async" ]; then
  ssh -i ~/.ssh/id_itp2 weissmann@endor.theo2.physik.uni-stuttgart.de "
    rm payout.txt
    chmod +x payload.sh
    ssh -i ~/.ssh/id_itp2 weissmann@hydra.theo2.physik.uni-stuttgart.de ./payload.sh &> payout.txt &
    "
else
  ssh -i ~/.ssh/id_itp2 weissmann@endor.theo2.physik.uni-stuttgart.de "
    rm payout.txt
    chmod +x payload.sh
    echo executing payload
    ssh -i ~/.ssh/id_itp2 weissmann@hydra.theo2.physik.uni-stuttgart.de ./payload.sh &> payout.txt
    "
  rsync weissmann@endor.theo2.physik.uni-stuttgart.de:payout.txt .
fi

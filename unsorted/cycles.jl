using LinearAlgebra

 
dim = 4
L = zeros(dim, dim)


@enum Reacts a1 a2 a3
@enum Directs ltr rtl
struct Step
    react::Reacts
    direct::Directs
end
mutable struct State
    X::Int8
    Y::Int8
end

function f!(x,y)
    println(x,y)
end
f!(2,3)


#step_choices = collect(Base.Iterators.product(instances(Reacts), instances(Direct)))

#steps_away = rand(step_choices, 10)



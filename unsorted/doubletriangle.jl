include("eigenvals.jl")
include("equations.jl")
import Plots
using Polynomials
using Roots
#gr()

function heatmap() 
    orig = triangle(2)
    xlen = 200
    ylen = 200
    data = zeros(ylen, xlen)
    
    xi = 0
    xs = range(30000,stop=60000,length=xlen)
    ys = range(100000,stop=300000,length=ylen)
    for x in xs
        xi += 1
        yi = 0
        for y in ys
            yi += 1
            rating  = calc_rating(orig, round(x), round(y))
            data[yi, xi] = rating.R
            #@debug x y xi yi rating
        end
    end
    #display(data)
    #data = rand(21,100)
    p = Plots.heatmap(xs,
    ys, data,
    c=Plots.cgrad([:blue, :white,:red, :yellow]),
    xlabel="x values", ylabel="y values",
    title="My title");

    display(p)
end

function best_x_y(orig)
    x = 0.41850337518585834 * OMEGA * 1.2
    y = 1.7757863437021588 * OMEGA * deltamu *0.05
    curbest = calc_rating(orig, round(x), round(y))
    fact = 1.01
    while true
        ratbegin = curbest

        xnew = x * fact
        rat = calc_rating(orig, round(xnew), round(y))
        if rat > curbest
            curbest = rat
            x = xnew
        end

        xnew = x / fact
        rat = calc_rating(orig, round(xnew), round(y))
        if rat > curbest
            curbest = rat
            x = xnew
        end

        ynew = y * fact
        rat = calc_rating(orig, round(x), round(ynew))
        if rat > curbest
            curbest = rat
            y = ynew
        end

        ynew = y / fact
        rat = calc_rating(orig, round(x), round(ynew))
        if rat > curbest
            curbest = rat
            y = ynew
        end

        if curbest == ratbegin
            break
        end
    end
    (x, y, curbest)
end

function plt_omega()
    global OMEGA
    xp = []
    yp = []
    op = []
    for om in 400:30:20000
        OMEGA = om
        x,y,rat = best_x_y(2)
        push!(xp, x/OMEGA)
        push!(yp, y/OMEGA)
        push!(op, OMEGA)
    end
    p = Plots.plot(op,xp, label="optimal x / OMEGA")
    Plots.plot!(p, op,yp, label="optimal y /OMEGA")

    Plots.xlabel!(p, "OMEGA")
    @debug xp[end] yp[end] op[end]
    @debug sum(xp)/length(xp)
    @debug sum(yp)/length(yp)
    display(p)
end

function inner()
    return 123
end
@cached function func()
    inner()
end

@cached function plt_mu(p, orig, note)
    println("plot mu", length(orig))
    global k2m, deltamu
    
    
    xp = []
    yp = []
    rp = []
    dmp = []
    fp = []
    ratios = []
    xfix = []
    yfix = []
    for dm in 1:0.1:10
        deltamu = dm
        k2m = exp(-deltamu) * k1m * k2p * B / (k1p * A)
        x,y,rat = best_x_y(orig)
        push!(xp, x/OMEGA)
        push!(yp, y/OMEGA)
        push!(rp, rat.R)

        affi = deltamu*(count(react->(react == r3p), orig) - count(react->(react == r3m), orig))
        border = cot(pi/length(orig))*tanh(affi/(2*length(orig)))
        push!(fp, border)

        #affi = deltamu*1
        #border = cot(pi/length(orig))*tanh(affi/(2*length(orig)))

        push!(ratios, rat.R/border)

        push!(dmp, dm)


        x = variable()
        x0 = find_zero(k1p*A - k1m * x + x^2 * k3/k2m * (k1p*A+k2p*B) + x^3 * (-k3 - k3*k1m/k2m) , 1.0)
        y0 = (k1p*A + k2p*B - k1m*x0)/k2m
        @assert isapprox([0,0], limit_dgl([x0, y0], nothing, nothing), atol=1e-10)
        push!(xfix, x0)
        push!(yfix, y0)
    end
    #Plots.plot!(p, dmp,xp, label="x_0 " * note, legend=:outerleft) #legend=:topleft
    #Plots.plot!(p, dmp,yp, label="y_0 " * note)
    #Plots.plot!(p, dmp,rp, label="R")
    #Plots.plot!(p, dmp,fp, label="f")
    #Plots.plot!(p, dmp, ratios, label="R/f " * note)
    # Plots.xlabel!(p, "Delta mu")
    # Plots.title!(p, "OMEGA = " * string(OMEGA))
    # Plots.plot!(p, ylims = (0,3.2))


    
    k2m = nothing
    deltamu = nothing

    [plt(dmp,xp, label="x_0 " * note), 
    plt(dmp,xfix, label="x Fixpunkt " * note),
    plt(dmp,yp, label="y_0 " * note),
    plt(dmp,yfix, label="y Fixpunkt " * note),
    plt(dmp,rp, label="R " * note),
    plt(dmp,fp, label="f " * note),
    plt(dmp,ratios, label="R/f " * note),]
end

function functest(args... ; kw...)
    @debug args kw
end
function plt(args... ; kw...)
    args, kw
end


function gen_plots()
    global k2m, deltamu
    
    
    k2m = nothing
    deltamu = nothing
    #temp = plt(321, abc=123)
    #functest(temp[1]...; temp[2]...)
    #functest(321, abc=123)

    #return

    #plt_mu()
    

    # p = Plots.plot()
    # plt_mu(p, [r2p, r2p, r2p, r3p, r3p, r3p, r1m, r1m, r1m], "Dreieck")
    # plt_mu(p, [r2p, r2p, r2p, r3p, r3p, r1m, r3p, r1m, r1m], "Tannenbaum")
    # plt_mu(p, [r2p, r2p, r2p, r3p, r2m, r3p, r2m, r3m, r1m], "längliches Dingends")
    
    # display(p)
    # Plots.savefig(p, "VerschiedeneFormen.pdf")
    ar = []
    # for i in 2:10
    #     println(i)
    #     push!(ar, plt_mu(nothing, triangle(i), string(i)))
    # end

    # push!(ar, plt_mu(nothing, [r2p, r2p, r2p, r3p, r3p, r3p, r1m, r1m, r1m], "Dreieck"))
    # push!(ar, plt_mu(nothing, [r2p, r2p, r2p, r3p, r3p, r1m, r3p, r1m, r1m], "Tannenbaum"))
    # push!(ar, plt_mu(nothing, [r2p, r2p, r2p, r3p, r2m, r3p, r2m, r3m, r1m], "längliches Dingends"))

    push!(ar, plt_mu(nothing, triangle(2), ""))

    p = Plots.plot()
    Plots.plot!(p, legend=:outerleft)
    Plots.xlabel!(p, "Delta mu")
    for el in ar
        Plots.plot!(p, el[1][1]...; el[1][2]...)
        Plots.plot!(p, el[2][1]...; el[2][2]...)
        Plots.plot!(p, el[3][1]...; el[3][2]...)
        Plots.plot!(p, el[4][1]...; el[4][2]...)
    end

    #Plots.plot!(p, ar[1][4][1]...; ar[1][4][2]...)
    display(p)
    #Plots.savefig(p, "Fixpunkt.pdf")

end
gen_plots()
# val = plt_mu(nothing, triangle(3), "test3")
# val = plt_mu(nothing, triangle(3), "test3")


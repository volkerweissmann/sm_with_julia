using PackageCompiler
#create_sysimage([:Plots, :DifferentialEquations, :Zygote], sysimage_path="sys_std.so", precompile_execution_file="precompile.jl")

create_sysimage([:Plots, :DifferentialEquations, :Zygote, :DifferentialEquations, :Roots, :Polynomials, :StaticArrays, :PrettyPrinting], sysimage_path="sys_std.so")
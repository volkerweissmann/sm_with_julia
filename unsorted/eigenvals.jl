using LinearAlgebra
using Combinatorics

kps = [[k1p,k1m],[k2p,k2m],[k3p,k3m]]

function kp(react::Reaction)
    kps[react.num][react.sgn]
end

function Wp(react::Reaction, X_old::Int, Y_old::Int)
    if react.num == 1
        if react.sgn == 1
            return OMEGA * k1p * A
        elseif react.sgn == -1
            return k1m * X_old
        end
    elseif react.num == 2
        if react.sgn == 1
            return OMEGA * k2p * B
        elseif react.sgn == -1
            return k2m * Y_old
        end
    elseif react.num == 3
        if react.sgn == 1
            return k3p/OMEGA^2 * X_old * (X_old - 1) * Y_old
        elseif react.sgn == -1
            return k3m/OMEGA^2 * X_old * (X_old - 1) * (X_old - 2)
        end
    end
    error("unreachable")
end

# OMEGA * k1p * A = k1m * X_old
# -> X = OMEGA * A * k1p / k1m
X_start = Int(round(OMEGA * A * k1p / k1m))
Y_start = Int(round(OMEGA * B * k2p / k2m))


function is_monocycle(Xs, Ys)
    #check if its a monocycle
    @assert length(Xs) == length(Ys)
    for i = 1:length(Xs)
        for j = 1:length(Xs)
            if i != j && Xs[i] == Xs[j] && Ys[i] == Ys[j]
                return false
            end
        end
    end
    return true
end
function is_monocycle(states::Array{State,1})
    for i in 1:length(states)
        for j in 1:length(states)
            if i != j && states[i] == states[j]
                return false
            end
        end
    end
    return true
end
struct InvalidCycle <: Exception end
function calc_path_xy(steps, X_start, Y_start, closed::Bool=true)
    len = length(steps)
    if !closed
        len += 1
    end
    Xs::Array{Int} = fill(-7, len)
    Ys::Array{Int} = fill(-7, len)
    X_cur = X_start
    Y_cur = Y_start
    for i in 1:length(steps)
        if X_cur < 0 || Y_cur < 0
            @debug "invalid cycle"
            throw(InvalidCycle())
        end
        Xs[i] = X_cur
        Ys[i] = Y_cur
        delt = stepDelta(steps[i])
        X_cur += delt.X
        Y_cur += delt.Y
    end
    if closed
        @assert X_cur == X_start
        @assert Y_cur == Y_start
    else
        Xs[end] = X_cur
        Ys[end] = Y_cur
    end
    @assert Xs[end] >= 0
    @assert Ys[end] >= 0
    (Xs, Ys)
end
function calc_path_states(steps, State_start::State, closed::Bool)
    (Xs, Ys) = calc_path_xy(steps, State_start.X, State_start.Y, closed)
    states::Array{State,1} = []
    for i in 1:length(Xs)
        push!(states, State(Xs[i], Ys[i]))
    end
    return states
end

function build_L(steps, Xs, Ys)
    @assert length(steps) == length(Xs)
    @assert length(steps) == length(Ys)

    #(xc, yc) = calc_path_xy(steps, Xs[1], Ys[1], true)
    #@assert Xs == xc && Ys == yc

    if !is_monocycle(Xs, Ys)
        throw(InvalidCycle())
    end
    dim = length(steps)
    L = zeros(dim, dim)
    cyc(i) = cyclic(dim, i)
    for i = 1:dim
        # i -> i+1
        L[cyc(i+1), i] += Wp(steps[i], Xs[i], Ys[i])
        # i+1 -> i
        L[i, cyc(i+1)] += Wp(Reaction(steps[i].num, -steps[i].sgn), Xs[cyc(i+1)], Ys[cyc(i+1)])
    end

    for i = 1:dim
        L[i, i] = - L[cyc(i+1), i] - L[cyc(i-1), i]
    end
    return L
end


function triangle(length)
    steps::Array{Reaction} = []
    for i in 1:length
        push!(steps, r1m)
    end
    for i in 1:length
        push!(steps, r2p)
    end
    for i in 1:length
        push!(steps, r3p)
    end
    return steps
end

module dat
    struct Rating
        R::Float64
        next::Float64
    end
    Base.:(isless)(a::Rating, b::Rating) = a.R < b.R || (a.R == b.R  && a.next < b.next)
    mutable struct Path
        steps::Array{Main.Reaction}
        x0::Int
        y0::Int
        rating::Rating
    end
end

function get_rating(ar)
    argsort = sortperm(real(ar))
    first_nontrivial = ar[argsort[end-1]]


    biggest_real_with_nonzero_imag = 0
    for arg in reverse(argsort)
        if imag(ar[arg]) != 0
            biggest_real_with_nonzero_imag = real(ar[arg])
            break
        end
    end

    return dat.Rating(abs(imag(first_nontrivial)/real(first_nontrivial)), real(first_nontrivial)/biggest_real_with_nonzero_imag)
    # vals = []
    # for i in 1:length(ar)
    #     @assert real(ar[i]) <= 0
    #     append!(vals, imag(ar[i])/real(ar[i]))
    # end
    # @dbg(ar[findmax(vals)[2]])
    # return findmax(vals)
end


function workstuff()
    orig_steps = [r1m, r1m, r2p, r2p, r3p, r3p]

    orig_steps = triangle(20)
    steps = copy(orig_steps)

    (Xs, Ys) = calc_path_xy(steps, X_start, Y_start)
    L = build_L(steps, Xs, Ys);
    oldvals = eigvals(L)


    i = 20
    temp = steps[i]
    steps[i] = steps[i+1]
    steps[i+1] = temp
    println(steps)

    (Xs, Ys) = calc_path_xy(steps, X_start, Y_start)
    L = build_L(steps, Xs, Ys);
    display(get_best(oldvals))

    display(get_best(eigvals(L)))
end

#display(eigvecs(L))

#orig_steps = [r1m, r2p, r3p]
function try_all_permutations(orig_steps)
    for steps in unique(collect(permutations(orig_steps[1:end-1])))
        push!(steps, orig_steps[end])
        (Xs, Ys) = calc_path_xy(steps, X_start, Y_start)
        if !is_monocycle(Xs,Ys)
        continue
        end
        L = build_L(steps, Xs, Ys)
        display(eigvals(L))
        #display(eigvecs(L)[:,1]) #Eine Spalte ist ein Eigenvector
        #display(L*eigvecs(L)[:,1]/eigvals(L)[1])
    end
end

function calc_rating(steps::Array{Reaction}, X_start, Y_start)
    (Xs, Ys) = (nothing, nothing)
    try
        (Xs, Ys) = calc_path_xy(steps, X_start, Y_start, true)
    catch ex
        if typeof(ex) != InvalidCycle
            throw(ex)
        end
        return dat.Rating(-1,0)
    end
    @assert length(steps) == length(Xs)
    @assert length(steps) == length(Ys)
    L = nothing
    try
        L = build_L(steps, Xs, Ys);
    catch ex
        if typeof(ex) != InvalidCycle
            throw(ex)
        end
        return dat.Rating(-1,0)
    end

    # dim = size(L)[1]
    # forward = 1
    # for i in 1:dim
    #     forward *= L[i,cyclic(dim, i+1)]
    # end
    # backward = 1
    # for i in 1:dim
    #     backward *= L[i,cyclic(dim, i-1)]
    # end

    # for i in 1:dim
    #     for j in 1:dim
    #         @assert j == i || j == cyclic(dim, i+1) || j == cyclic(dim, i-1) || L[i,j] == 0
    #     end
    # end

    # vec = rand(dim)
    # @assert abs(sum(L*vec))  < 1e-10

    # @dbg(forward, backward)
    # f = cot(pi/dim)*tanh(log(forward/backward)/(2*dim))
    # f2 = cot(pi/dim)*tanh(log(backward/forward)/(2*dim))
    # @dbg(f, f2)

    #@dbg(L)
    return get_rating(eigvals(L))
end

#module Data
#    mutable struct MyStruct
#        x::Int
#    end
#end

function discret_to_cont(steps, X_start, Y_start, label)
    (Xs, Ys) = calc_path_xy(steps, X_start, Y_start)
    push!(Xs, Xs[1])
    push!(Ys, Ys[1])
    Plots.plot!(Xs/OMEGA, Ys/OMEGA, label=label)
end
function discret_to_cont(path::dat.Path, label)
    discret_to_cont(path.steps, path.x0, path.y0, label)
end
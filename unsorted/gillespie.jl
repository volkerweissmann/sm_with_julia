using Gillespie
using Random
using Gadfly
#using Plots
#using VegaLite
import Cairo, Fontconfig
include("coeffs.jl")

plot_theme = Theme(
    panel_fill=colorant"white",
    default_color=colorant"black"
)





#::LinearAlgebra.Adjoint{Int64,Array{Int64,1}}
#::Array{Float64,1}
function F(x, parms)
    #deltamu = parms[1]
    (X,Y) = x

    onePlus = k1p * A * OMEGA
    onePlus = k1p * A * OMEGA
    oneMinus = k1m * X

    twoPlus = k2p * B * OMEGA
    twoMinus = k2m * Y

    threePlus = k3p * X^2 * Y / OMEGA^2
    threeMinus = k3m * X^3 / OMEGA^2
    [onePlus,oneMinus,twoPlus,twoMinus,threePlus,threeMinus]
end
#nx0 = 100
#x0 = [nx0,Int(floor(OMEGA/2))-nx0]
#x0 = [20000, 20000]
x0 = [52669,  213546]
nu = [  [1 0];[-1 0];
        [0 1];[0 -1];
        [1 -1];[-1 1]]
parms = [4.1]
tf = 200.0 # 250.0

#Random.seed!(12345)

result = ssa(x0,F,nu,parms,tf)

data = ssa_data(result)

#plotdata = data[1:Int(ceil(size(data)[1]/100)):end, 1:end]

# p = plot(data,
#     layer(x=:time,y=:x1,Geom.step,Theme(default_color=colorant"red")),
#     #layer(x=:time,y=:x2,Geom.step,Theme(default_color=colorant"orange")),
#     #layer(x=:time,y=:x3,Geom.step,Theme(default_color=colorant"blue")),
#     Guide.xlabel("Time"),
#     Guide.ylabel("Number"),
#     Guide.title("Gillespie simulation"),
#     Guide.manual_color_key("Number",["X"],["red"]),
#     plot_theme)

p = plot(data, layer(x=:x1, y=:x2, Geom.step))

# p = plot(data,
#     layer(x=:time,y=:x1,Geom.step,Theme(default_color=colorant"red")),
#     layer(x=:time,y=:x2,Geom.step,Theme(default_color=colorant"orange")),
#     #layer(x=:time,y=:x3,Geom.step,Theme(default_color=colorant"blue")),
#     Guide.xlabel("Time"),
#     Guide.ylabel("Number"),
#     Guide.title("SSA simulation"),
#     Guide.manual_color_key("Subpopulation",["X","Y"],["red","orange"]),
#     plot_theme)

#pout = PDF("stochastic.pdf", 14cm, 8cm);
#draw(pout, p)




# @time F([600,700], [4.1])
# 0.006049 seconds (18 allocations: 848 bytes)
# 6-element Array{Float64,1}:
#  0.1
# 600.0
#  0.30000000000000004
# 34.80261834369864
#  2.52e8
#  2.16e8

# @time F([@time F([600,700], [4.1])
#   0.000031 seconds (18 allocations: 848 bytes)
# 6-element Array{Float64,1}:
#    0.1
#  600.0
#    0.30000000000000004
#   34.80261834369864
#    2.52e8
#    2.16e8
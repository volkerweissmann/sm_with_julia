function step_swap(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] == steps[next]
        return nothing
    end
    cursteps = copy(steps)
    temp = cursteps[i]
    cursteps[i] = cursteps[next]
    cursteps[next] = temp
    return cursteps
end
function far_step_swap(steps, i, j)
    if steps[i] == steps[j]
        return nothing
    end
    cursteps = copy(steps)
    temp = cursteps[i]
    cursteps[i] = cursteps[j]
    cursteps[j] = temp
    return cursteps
end
function far_step_swap_n(steps, i, j, n)
    cyc(x) = cyclic(length(steps), x)
    if !(cyc(i+n) < j || cyc(j+n) < i)
        return nothing
    end
    cursteps = copy(steps)
    for k in 1:n
        temp = cursteps[cyc(i+k)]
        cursteps[cyc(i+k)] = cursteps[cyc(j+k)]
        cursteps[cyc(j+k)] = temp
    end
    return cursteps
end
function r1p_r2m_to_r3p(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r1p || steps[next] != r2m
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3p
    deleteat!(cursteps, next)
    return cursteps
end
function r2m_r1p_to_r3p(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r2m || steps[next] != r1p
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3p
    deleteat!(cursteps, next)
    return cursteps
end

function r1m_r2p_to_r3m(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r1m || steps[next] != r2p
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3m
    deleteat!(cursteps, next)
    return cursteps
end
function r2p_r1m_to_r3m(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r2p || steps[next] != r1m
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3m
    deleteat!(cursteps, next)
    return cursteps
end





function r1p_to_r3p_r2p(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r1p
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3p
    insert!(cursteps, next, r2p)
    return cursteps
end
function r1m_to_r3m_r2m(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r1m
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3m
    insert!(cursteps, next, r2m)
    return cursteps
end


function r2m_to_r3p_r1m(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r2m
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3p
    insert!(cursteps, next, r1m)
    return cursteps
end
function r2p_to_r3m_r1p(steps, i)
    next = cyclic(length(steps), i+1)
    if steps[i] != r2p
        return nothing
    end
    cursteps = copy(steps)
    cursteps[i] = r3m
    insert!(cursteps, next, r1p)
    return cursteps
end




function search_and_replace(steps, i, search, replace)
    if steps == nothing
        return nothing
    end

    for j in 1:length(search)
        if steps[cyclic(length(steps), i+j-1)] != search[j]
            return nothing
        end
    end

    cursteps = copy(steps)
    if length(search) == length(replace)
        for j in 1:length(search)
            cursteps[cyclic(length(steps), i+j-1)] = replace[j]
        end
        return cursteps
    end

    #todo use a cyclical array and remove the if above
    num_deleted_in_front = 0
    for j in 1:length(search)
        if i <= length(cursteps)
            deleteat!(cursteps, i)
        else
            deleteat!(cursteps, 1)
            num_deleted_in_front += 1
        end
    end
    for j in 1:length(replace)
        insert!(cursteps, i-num_deleted_in_front+j-1, replace[j])
    end
    return cursteps
end

function unit_test_search_and_replace()
    steps = [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    search =  [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    replace = [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]
    cursteps = search_and_replace(steps, 1, search, replace)
    @assert cursteps == [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]

    steps = [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1), Reaction(4, 1)]
    search =  [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    replace = [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]
    cursteps = search_and_replace(steps, 1, search, replace)
    @assert cursteps == [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1), Reaction(4, 1)]


    steps = [Reaction(4, 1), Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    search =  [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    replace = [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]
    cursteps = search_and_replace(steps, 2, search, replace)
    @assert cursteps == [Reaction(4, 1), Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]

    steps = [Reaction(3, 1), Reaction(1, 1), Reaction(2, 1)]
    search =  [Reaction(1, 1), Reaction(2, 1), Reaction(3, 1)]
    replace = [Reaction(5, 1), Reaction(6, 1), Reaction(7, 1)]
    cursteps = search_and_replace(steps, 2, search, replace)
    @assert cursteps == [Reaction(7, 1), Reaction(5, 1), Reaction(6, 1)]
end
unit_test_search_and_replace()
ENV["JULIA_DEBUG"] = Main

include("eigenvals.jl")
include("../Brüsselator/equations.jl")
include("../Brüsselator/paths.jl")
using DifferentialEquations
import Plots
using ForwardDiff


function F(x,y)
    [1,0] * (k1p * A - k1m * x) + [0,1] * (k2p * B - k2m * y) + [1,-1] * (k2p * B - k2m * y)
end
function G(x,y)
    [1,-1] * (-k3p * x * y + k3m * 3 * x^2)
end
function Q(x,y)
    Qxx = (k1p * A + k1m * x + k3p * x^2 * y + k3m * x^3)/2
    Qyy = (k2p * B + k2m * y + k3p * x^2 * y + k3m * x^3)/2
    Qxy = (-k3p * x^2 * y - k3m * x^3)/2
    [[Qxx, Qxy], [Qyx, Qyy]]
end
function dPdt(P,x,y)
    F(x,y) * P[x,y]
end


function deterministic()
    #u0 = [0, 0]
    u0 = [0.4253675153853952, 1.7997828731427208] # Dieser Punkt liegt auf dem limit cycle
    #tspan = (0.0, 24.803765101175795)
    tspan = (0.0, 25.0)
    #tspan = (0.0, 100.0)
    #tspan = (0.0, 25.095874793746454)

    coeffs = basile_coeffs(0, 4.1)
    dglfunc(u,t) = limit_dgl(u,coeffs,t)

    prob = ODEProblem(dglfunc,u0,tspan)
    println("starting to solve dgl")
    sol = solve(prob, Tsit5(), dtmax=1e-3, reltol=1e-8, abstol=1e-8)
    println("solved dgl")



    Plots.plot()
    p = Plots.plot!(sol,title="Deterministic",  xaxis="n_x",yaxis="n_y", vars=(1,2)) # legend=false
    display(p)
    #(steps_orig, X_orig, Y_orig) = cont_to_discret(sol)
    #@dbg(calc_R(steps_orig, X_orig, Y_orig))
    #discret_to_cont(steps_orig, X_orig, Y_orig)

    #try_to_improve(steps_orig, X_orig, Y_orig)
end
deterministic()




function find_loop_end()
    mindist = Inf
    minind = nothing
    for i in Int(round(length(sol)/2)):length(sol)
        dist = (u0[1]-sol[i][1])^2 + (u0[2]-sol[i][2])^2
        if dist < mindist
            mindist = dist
            minind = i
        end
    end
    return minind
end
# minind = find_loop_end()
# display(sol[minind])
# display(sol.t[minind])


function get_me_from_cur_to_new!(steps, curX, curY, newX, newY)
    if newX > curX
        temp = repeat([r1p], newX-curX)
        append!(steps, temp)
    end
    if newX < curX
        temp = repeat([r1m], curX-newX)
        append!(steps, temp)
    end
    if newY > curY
        temp = repeat([r2p], newY-curY)
        append!(steps, temp)
    end
    if newY < curY
        temp = repeat([r2m], curY-newY)
        append!(steps, temp)
    end
end
function cont_to_discret(sol)
    steps = []
    X_start = Int(round(sol[1][1]*OMEGA))
    Y_start = Int(round(sol[1][2]*OMEGA))
    curX = X_start
    curY = Y_start
    @assert curX >= 0
    @assert curY >= 0
    for i in 2:length(sol)
        newX = Int(round(sol[i][1]*OMEGA))
        newY = Int(round(sol[i][2]*OMEGA))
        get_me_from_cur_to_new!(steps, curX, curY, newX, newY)
        curX = newX
        curY = newY
        @assert curX >= 0
        @assert curY >= 0
    end
    get_me_from_cur_to_new!(steps, curX, curY, X_start, Y_start)
    return (steps, X_start, Y_start)
end



# while true
#     var = calc_R(steps, X_start+1, Y_start)
#     if var > oldbest
#         oldbest = var
#         X_start += 1
#         continue
#     end
#
#     var = calc_R(steps, X_start-1, Y_start)
#     if var > oldbest
#         oldbest = var
#         X_start -= 1
#         continue
#     end
#
#     var = calc_R(steps, X_start, Y_start+1)
#     if var > oldbest
#         oldbest = var
#         Y_start += 1
#         continue
#     end
#
#     var = calc_R(steps, X_start, Y_start-1)
#     if var > oldbest
#         oldbest = var
#         Y_start -= 1
#         continue
#     end
#
#     break
# end
# dbg(X_orig, Y_orig)
# dbg(X_start, Y_start)

# for X_start in 0:100
#     @dbg(X_start, calc_R(steps, X_start, Y_start))
# end
# for Y_start in 300:400
#     @dbg(Y_start, calc_R(steps, 85, Y_start))
# end







# #Plots.savefig("deterministic.pdf")
# Plots.plot!()


# #Plots.plot!(sol.t, t->0.5*exp(1.01t),lw=3,ls=:dash,label="True Solution!")

# # using LinearAlgebra
# # using BenchmarkTools
# # dim = 1000
# # M = rand(Float64, (dim,dim));
# # @benchmark eigvecs(M)

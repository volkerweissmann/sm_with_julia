set_cache_path("/home/volker/julia_simple_cache")
A = 1.0
B = 3.0

k1p = 0.1
k1m = 1.0
k2p = 0.1
k3 = 1.0
k3p = k3
k3m = k3

deltamu = 4.1
k2m = exp(-deltamu) * k1m * k2p * B / (k1p * A)

OMEGA = 100000



struct Reaction
    num::Int
    sgn::Int
end

r1p = Reaction(1, +1)
r1m = Reaction(1, -1)
r2p = Reaction(2, +1)
r2m = Reaction(2, -1)
r3p = Reaction(3, +1)
r3m = Reaction(3, -1)

all_reactions = (r1p, r1m, r2p, r2m, r3p, r3m)

mutable struct State
    X::Int
    Y::Int
end
Base.:+(a::State, b::State) = State(a.X + b.X, a.Y + b.Y)
Base.:-(a::State) = State(-a.X, -a.Y)
Base.:(==)(a::State, b::State) = a.X == b.X && a.Y == b.Y

function stepDelta(step::Reaction)
    if step.sgn == -1
        return -stepDelta(Reaction(step.num, +1))
    end
    if step.num == 1
        return State(+1, 0)
    elseif step.num == 2
        return State(0, +1)
    elseif step.num == 3
        return State(+1, -1)
    end
    throw("Impossible")
end


# returns +1 if a and b are "more or less in the same direction", returns -1 if a and b are "more or less in the oppposite direction"
function scalar_prod_sgn(a::Int, b::Int)
    if a > b
        return scalar_prod_sgn(b, a)
    end

    if a == b
        return +1
    end
    if a == 1 && b == 2
        return +1
    end
    if a == 1 && b == 3
        return +1
    end
    if a == 2 && b == 3
        return -1
    end
    @error "unreachable"
end
function scalar_prod_sgn(a::Reaction, b::Reaction)
    return scalar_prod_sgn(a.num, b.num) * a.sgn * b.sgn
end
function scalar_prod_sgn(a::Reaction, b::Int)
    return scalar_prod_sgn(a.num, b) * a.sgn
end
function scalar_prod_sgn(a::Int, b::Reaction)
    return scalar_prod_sgn(a, b.num) * b.sgn
end





function func()
    println("func")
    return 123
end


# macro dbg(vars...)
#     printargs = []
#     if length(vars) == 0
#         push!(printargs, "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] ")
#         push!(printargs, "\n")
#     end
#     for var in vars
#         push!(printargs, "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] ")
#         push!(printargs, :($(string(var))))
#         push!(printargs, " = ")
#         #push!(printargs, :( $(esc(var))  ))
#         push!(printargs, :( Base.repr($(esc(var)))  ))
#         push!(printargs, "\n")
#     end
#     Expr(:call, println, printargs...)
# end

import Plots
using Combinatorics
using Serialization
using StatProfilerHTML
using Profile
using Random

include("eigenvals.jl")
include("modifications.jl")
function square(length)
    steps::Array{Reaction} = []
    for i in 1:length
        push!(steps, r1p)
    end
    for i in 1:length
        push!(steps, r2p)
    end
    for i in 1:length
        push!(steps, r1m)
    end
    for i in 1:length
        push!(steps, r2m)
    end
    return steps
end

function try_modification!(path::dat.Path, modification_func)
    newsteps = modification_func(path)
    if newsteps == nothing # if modification_func finds that its modification is not applicable on orig.steps, it will return nothing
        return false
    end
    newbest = calc_rating(newsteps, path.x0, path.y0)
    equalflag = false
    if newbest > path.rating
        path.rating = newbest
        path.steps = newsteps
    elseif newbest == path.rating
        equalflag = true
    end
    return equalflag
end
function move_descent!(path::dat.Path)
    #flag = true
    #while flag
        #flag = false

    newbest = calc_rating(path.steps, path.x0+1, path.y0)
    if newbest > path.rating
        path.rating = newbest
        path.x0 += 1
        #flag = true
    end

    newbest = calc_rating(path.steps, path.x0-1, path.y0)
    if newbest > path.rating
        path.rating = newbest
        path.x0 -= 1
        #flag = true
    end

    newbest = calc_rating(path.steps, path.x0, path.y0+1)
    if newbest > path.rating
        path.rating = newbest
        path.y0 += 1
        #flag = true
    end

    newbest = calc_rating(path.steps, path.x0, path.y0-1)
    if newbest > path.rating
        path.rating = newbest
        path.y0 -= 1
        #flag = true
    end

    #end
end
function reorder_descent!(path::dat.Path)
    eflag = false
    for i in randperm(length(path.steps))
        equalflag = try_modification!(path, path -> step_swap(path.steps, i))
        if equalflag
            eflag = true
        end
    end
    return eflag
end
function far_reorder_descent!(path::dat.Path)
    eflag = false
    for len in 1:3
        for i in randperm(length(path.steps))
            for j in randperm(length(path.steps))
                equalflag = try_modification!(path, path -> far_step_swap_n(path.steps, i, j, len))
                if equalflag
                    eflag = true
                end
            end
        end
    end
    return eflag
end
function replace_descent!(path::dat.Path, reps)
    eflag = false
    for i in randperm(length(path.steps))
        for k in randperm(length(reps))
            pair = reps[k]
            orig = path.rating
            equalflag = try_modification!(path, path -> search_and_replace(path.steps, i, pair[1], pair[2]))
            if equalflag
                eflag = true
            end
        end
    end
    return eflag
end
function try_to_improve(steps_orig, X_orig, Y_orig)
    Plots.plot()
    ya_steps = copy(steps_orig)
    path = dat.Path(ya_steps, X_orig, Y_orig, calc_rating(ya_steps, X_orig, Y_orig))
    discret_to_cont(steps_orig, X_orig, Y_orig, "start " * string(path.rating))

    reps = []
    append!(reps, find_all_replacements(3, 3))
    append!(reps, find_all_replacements(4, 4))
    append!(reps, find_all_replacements(5, 5))

    #seperated_reps = []
    #append!(seperated_reps, find_all_replacements(2, 1))
    #append!(seperated_reps, find_all_replacements(3, 1))
    #append!(seperated_reps, find_all_replacements(3, 2))

    #descender = [move_descent!, reorder_descent!, far_reorder_descent!, path -> replace_descent!(path, reps)]
    descender = [move_descent!, reorder_descent!, far_reorder_descent!, path -> replace_descent!(path, reps)]
    while true
        old = path.rating
        for desc! in descender
            desc!(path)
            if path.rating != old
                @assert path.rating > old
                break
            end
        end
        if old == path.rating
            break
        end
    end

    @debug "best" path.x0 path.y0
    discret_to_cont(path, "best: " * string(path.rating))
end

function is_good_replacement(search, replace)
    s = calc_path_states(search, State(100,100), false)
    r = calc_path_states(replace, State(100,100), false)
    for i in 2:min(length(s), length(r))-1
        if s[i] == r[i]
            return false
        end
    end
    if s[end] != r[end]
        return false
    end
    return true
end
function all_reaction_combinations(N::Integer)
    args = repeat([all_reactions], N)
    return Iterators.product(args...)
end
@cached function find_all_replacements(Ns::Int, Nr::Int)
    @debug "begin reps" Ns Nr
    reps::Array{Tuple{Array{Reaction,1},Array{Reaction,1}},1} = []#
    for search in all_reaction_combinations(Ns)
        for replace in all_reaction_combinations(Nr)
            # if r3p in search || r3m in search
            #     continue
            # end
            # if !(r3p in replace) && !(r3m in replace)
            #     continue
            # end

            if !is_monocycle(calc_path_states(search, State(100, 100), false))
                continue
            end
            if !is_monocycle(calc_path_states(replace, State(100, 100), false))
                continue
            end
            if !is_good_replacement(search, replace)
                continue
            end
            search = collect(search)
            replace = collect(replace)
            pair::Tuple{Array{Reaction,1},Array{Reaction,1}} = (search, replace)
            temp = [pair, pair]
            push!(reps, pair)
        end
    end
    @debug "end reps" 
    return reps
end


struct MyStruct
    x::Int
end

function proffunc(orig, reps, seperated_reps)
    try_to_improve(orig, 100, 100, reps, seperated_reps)
end
function incremental()


    orig = triangle(20)
    #orig = square(15)
    # temp = orig[i]
    # orig[i] = orig[i+1]
    # orig[i+1] = temp

    #@debug is_monocycle(calc_path_states(orig, State(40, 40), true))
    #@debug is_monocycle(calc_path_xy(orig, 40, 40, true)...)
    
    #@debug calc_rating(orig, 40, 40)

    #
    #@profilehtml try_to_improve(orig, 100, 100, reps, seperated_reps)


    println("starting...")
    @time @profilehtml try_to_improve(orig, 44+200, 66+200)
    #@profile try_to_improve(orig, 100, 100, reps, seperated_reps)
    #try_to_improve(orig, 100, 100, reps, seperated_reps)

    #profout = open("profout.txt", "w")
    #Profile.print(profout)
    #close(profout)

    #try_to_improve(square(7), 40, 40, reps, seperated_reps)
    #try_to_improve([r1m,r2p,r2p,r3p,r2m], 40, 40)

    #0.7896951677829548
end

Random.seed!(321+2);
incremental()

# function weird(N, orig, best)
#     for i in 1:10000
#         for i in 1:N
#             args = (orig, i+40, 40, best, st -> step_swap(st, i))
#             wrap(args...)
#         end
#     end
# end
# @profilehtml weird(60, triangle(60), dat.Rating(-1.0,0))

function find_all_replacements(N::Int)
    cache = Base.open(string(N), "r")
    reps = deserialize(cache)
    Base.close(cache)
    return reps

    reps::Array{Tuple{Array{Reaction,1},Array{Reaction,1}},1} = []#
    for search in all_reaction_combinations(N)
        for replace in all_reaction_combinations(N)
            # if r3p in search || r3m in search
            #     continue
            # end
            # if !(r3p in replace) && !(r3m in replace)
            #     continue
            # end

            if !is_monocycle(calc_path_states(search, State(100, 100), false))
                continue
            end
            if !is_monocycle(calc_path_states(replace, State(100, 100), false))
                continue
            end
            if !is_good_replacement(search, replace)
                continue
            end
            search = collect(search)
            replace = collect(replace)
            pair::Tuple{Array{Reaction,1},Array{Reaction,1}} = (search, replace)
            temp = [pair, pair]
            push!(reps, pair)
        end
    end
    cache = Base.open(string(N), "w")
    serialize(cache, reps)
    Base.close(cache)

    return reps
end

function old_try_modification(steps::Array{Reaction}, X_start, Y_start, best, modification_func)
    cursteps = modification_func(steps)
    if cursteps == nothing
        return (steps, best, false)
    end
    newbest = calc_rating(cursteps, X_start, Y_start)
    equalflag = false
    if newbest > best
        best = newbest
        steps = cursteps
    elseif newbest == best
        equalflag = true
    end
    return (steps, best, equalflag)
end



# oldbest = calc_R(steps_orig, X_orig, Y_orig)
function grad_descent(steps::Array{Reaction}, X_start, Y_start, origbest, replacements, seperated_reps)
    
    oldbest = origbest
    @dbg(origbest)

    for i in 1:length(steps)
        (steps, oldbest) = try_modification(steps, X_start, Y_start, origbest, st -> step_swap(st, i))
    end

    # for i in 1:length(steps)
    #     for j in 1:length(steps)
    #         for pair_1 in seperated_reps
    #             for pair_2 in seperated_reps
    #                 if length(pair_1[1]) + length(pair_2[2]) != length(pair_1[2]) + length(pair_2[1])
    #                     continue
    #                 end
    #                 (steps, oldbest) = try_modification(steps, X_start, Y_start, oldbest,
    #                 st -> search_and_replace(
    #                         search_and_replace(st, j, pair_2[2], pair_2[1]), 
    #                         i, pair_1[1], pair_1[2])  )
    #             end
    #         end
    #     end
    # end

    #origlength = length(steps)
    #i = 1
    #while i <= length(steps) && i < origlength*2
    for i in 1:length(steps)
        for pair in replacements
            (steps, oldbest) = try_modification(steps, X_start, Y_start, oldbest,
            path -> dat.Path(search_and_replace(path.st, i, pair[1], pair[2]), path.x0, path.y0, dat.Rating(-2,-2)))
        end

        #i += 1
    end

    return steps, origbest != oldbest
end
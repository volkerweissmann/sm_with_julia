#!/bin/python
import os
import re

# PDIR = "/data/weissmann/precious/"
PDIR = os.environ["HOME"] + "/Sync/data/from_itp2/"

p = re.compile("mail_10_12_x_x0_b_(.+)_mu_(.+).json")
for el in os.listdir(PDIR):
    m = p.match(el)
    if m is None:
        continue
    newn = "mail_19_01_x_x0_b_" + m.group(1) + "_mu_" + m.group(2) + ".json"
    os.rename(PDIR + el, PDIR + newn)
    print(PDIR + el, "->", PDIR + newn)

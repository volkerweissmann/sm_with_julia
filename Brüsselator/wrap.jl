ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "attract.jl"

mus = [4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.4, 6.0, 6.5, 7.0, 7.5, 8.0]
gaspard_robustness_omega(basile_coeffs(0, 5.3))

# for mu in mus
#     println("mu: ", mu)
#     gaspard_robustness_omega(basile_coeffs(0, mu))
# end

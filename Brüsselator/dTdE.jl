ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
import Zygote
using QuadGK
using Memoize
@includeonce "attract.jl"

function calc_dTdE_via_N(bc)
    @time xl_sol = limit_cycle_shape(bc)
    @assert xl_sol.t[1] == 0
    T = xl_sol.t[end]

    q0 = xl_sol.u[1]

    stat = stationary_point(bc)
    F0 = brüssel_F(bc, q0)
    f1 = F0 / norm(F0)

    @time M_sol = calc_M(bc, xl_sol)
    @time NT = calc_NT(bc, xl_sol, M_sol)

    f1Nf1 = dot(f1, NT * f1)
    return (-f1Nf1, T)
end

function gaspard_robustness_via_N(ε, µ)
    bc = basile_coeffs(0, µ)

    dTdE, T = calc_dTdE_via_N(bc)
    R = T^2 / (pi * abs(dTdE)) / ε # (63) in the correlation time paper
    return R
end

function calc_M(bc, xl_sol)
    dgl = get_limit_dgl(bc)
    T = xl_sol.t[end]
    function M_dgl(M, bc, t)
        jac = Zygote.jacobian(u -> dgl(u, bc, nothing), xl_sol(t))[1]
        #jac = Zygote.jacobian(u -> our_brüssel_F(bc, u), xl_sol(t))[1]
        jac * M
    end

    # function M_dgl(M, bc, t)
    #     x, y = xl_sol(t)
    #     # dfxdx = - bc.k2 + 2 * bc.k3 * x * y - bc.k4
    #     # dfydx = bc.k2 - 2 * bc.k3 * x * y
    #     # dfxdy = bc.k3 * x^2
    #     # dfydy = - bc.k3 * x^2
    #     dfxdx = - bc.k1m + 2 * bc.k3p * x * y - 3 * bc.k3m * x^2
    #     dfydx = - 2 * bc.k3p * x * y + 3 * bc.k3m * x^2
    #     dfxdy = bc.k3p * x^2
    #     dfydy = - bc.k2m - bc.k3p * x^2

    #     jac = [dfxdx dfxdy; dfydx dfydy]
    #     jac * M
    # end

    prob = DifferentialEquations.ODEProblem(M_dgl, [1 0; 0 1], (0.0, T), bc)
    M_sol = DifferentialEquations.solve(prob, dense = false, dtmax = 1e-3, reltol = 1e-12, abstol = 1e-12, maxiter = 1e6)
    #M_sol = DifferentialEquations.solve(prob, dense = false, dtmax = 1e-3, reltol = 1e-9, abstol = 1e-9, maxiter = 1e6)
    return M_sol
end

# decomp_matrix([0.31478773456070475 8.715250299776887e-8; -1.841925892665094e-6 0.3147874581397971])
function decomp_matrix(A)
    # @debug A

    temp = eigvals(A)
    @assert length(temp) == 2
    evs = [temp[2], temp[1]]
    e_vecs = eigvecs(A)
    f_vecs = eigvecs(transpose(A))

    ea = e_vecs[:, 1]
    eb = e_vecs[:, 2]
    fa = f_vecs[:, 1]
    fb = f_vecs[:, 2]

    # begin # find out which eigenvector corresponds to which eigenvalue
    if norm(A * ea - evs[1] * ea) < norm(A * ea - evs[2] * ea)
        e1 = ea
        e2 = eb
    else
        e1 = eb
        e2 = ea
    end
    if norm(transpose(A) * fa - evs[1] * fa) < norm(transpose(A) * fa - evs[2] * fa)
        f1 = fa
        f2 = fb
    else
        f1 = fb
        f2 = fa
    end
    # end

    e1 /= dot(f1, e1) # todo: are we supposed to do this? 22.12.21 does this solve my gaspard problems
    e2 /= dot(f2, e2) # todo: are we supposed to do this?

    if !approx(1, dot(f1, e1), 1e-3)
        @debug f1 e1 dot(f1, e1) dot(f2, e2) dot(f1, e2) dot(f2, e1)
    end
    @assert approx(1, dot(f1, e1), 1e-3)
    @assert approx(1, dot(f2, e2), 1e-3)
    @assert approx(0, dot(f1, e2), 1e-3)
    @assert approx(0, dot(f2, e1), 1e-3)

    @assert approx(A * e1, e1 * evs[1], 1e-3)
    @assert approx(A * e2, e2 * evs[2], 1e-3)

    cmp_prod(x) = e1 * evs[1] * dot(f1, x) + e2 * evs[2] * dot(f2, x)

    @assert approx(cmp_prod([1, 0]), A * [1, 0], 0.2)
    @assert approx(cmp_prod([0, 1]), A * [0, 1], 0.2)

    lambda_1 = evs[1]
    lambda_2 = evs[2]
    return lambda_1, lambda_2, e1, e2, f1, f2
end

@memoize function time_evolve(bc, T, u0)
    dgl = get_hamilton_dgl
    tspan = (0.0, T)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan)
    sol = DifferentialEquations.solve(prob, reltol = 1e-10, abstol = 1e-10, maxiter = 1e6)
    # DifferentialEquations.Tsit5(),
    return sol
end

@memoize function delta_evolve(bc, xl_sol, t_max, u0)
    tspan = (0.0, t_max)
    if typeof(bc) == our_brüssel_conf
        dgl = (u, p, t) -> vcat(
            Zygote.jacobian(q -> our_brüssel_F(bc, q), xl_sol(t))[1] * u[1:2] + 2 * our_brüssel_Q(bc, xl_sol(t)) * u[3:4],
            -transpose(Zygote.jacobian(q -> our_brüssel_F(bc, q), xl_sol(t))[1]) * u[3:4])
    elseif typeof(bc) == tang_our_brüssel_conf
        dgl = (u, p, t) -> vcat(
            Zygote.jacobian(q -> our_brüssel_F(bc, q), xl_sol(t))[1] * u[1:2] + 2 * tang_our_brüssel_Q(bc, xl_sol(t)) * u[3:4],
            -transpose(Zygote.jacobian(q -> our_brüssel_F(bc, q), xl_sol(t))[1]) * u[3:4])
    elseif typeof(bc) == gaspard_brüssel_conf
        dgl = (u, p, t) -> vcat(
            Zygote.jacobian(q -> gaspard_brüssel_F(bc, q), xl_sol(t))[1] * u[1:2] + 2 * gaspard_brüssel_Q(bc, xl_sol(t)) * u[3:4],
            -transpose(Zygote.jacobian(q -> gaspard_brüssel_F(bc, q), xl_sol(t))[1]) * u[3:4])
    elseif typeof(bc) == steward_landau_conf
        #theosol(t) = [cos(0.11 * t - 0.7598038686257643), sin(0.11 * t - 0.7598038686257643)]
        dgl = (u, p, t) -> vcat(
            Zygote.jacobian(q -> steward_landau_F(bc, q), xl_sol(t))[1] * u[1:2] + 2 * steward_landau_Q(bc, xl_sol(t)) * u[3:4],
            -transpose(Zygote.jacobian(q -> steward_landau_F(bc, q), xl_sol(t))[1]) * u[3:4])
    else
        throw(ValueError)
    end

    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan)

    # dgl(u, p, t) = vcat(
    #     Zygote.jacobian(q -> gaspard_brüssel_F(bc, q), xl_sol(t) + u[1:2])[1] * u[1:2] + 2 * gaspard_brüssel_Q(bc, xl_sol(t) + u[1:2]) * u[3:4], -transpose(Zygote.jacobian(q -> gaspard_brüssel_F(bc, q), xl_sol(t) + u[1:2])[1]) * u[3:4])

    # println(59)
    # stepper = DifferentialEquations.init(prob, DifferentialEquations.Tsit5())
    # @debug T
    # for i in 0:1000
    #     @debug stepper
    #     DifferentialEquations.step!(stepper)
    # end

    # throw(123)

    sol = DifferentialEquations.solve(prob, reltol = 1e-10, abstol = 1e-10) # , maxiter = 1e25
    return sol
end


@memoize function calc_NT(bc, xl_sol, M_sol)
    T = xl_sol.t[end]
    if typeof(bc) == our_brüssel_conf
        NT = M_sol(T) * 2 * quadgk(t -> inv(M_sol(t)) * our_brüssel_Q(bc, xl_sol(t)) * transpose(inv(M_sol(t))), 0, T, rtol = 1e-8, atol = 1e-8)[1]
    elseif typeof(bc) == gaspard_brüssel_conf
        NT = M_sol(T) * 2 * quadgk(t -> inv(M_sol(t)) * gaspard_brüssel_Q(bc, xl_sol(t)) * transpose(inv(M_sol(t))), 0, T, rtol = 1e-8, atol = 1e-8)[1]
    else
        throw(ValueError)
    end
    return NT
end

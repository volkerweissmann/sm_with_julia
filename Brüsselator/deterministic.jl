import DifferentialEquations
import Plots
include("paths.jl")
include("equations.jl")
function deterministic()
    #u0 = [0, 0]
    u0 = [0.4253675153853952, 1.7997828731427208] # Dieser Punkt liegt auf dem limit cycle
    #tspan = (0.0, 24.803765101175795)
    tspan = (0.0, 25.0)
    #tspan = (0.0, 100.0)
    #tspan = (0.0, 25.095874793746454)

    coeffs = basile_coeffs(0, 4.1)
    dglfunc(u,t) = limit_dgl(u,coeffs,t)

    prob = DifferentialEquations.ODEProblem(limit_dgl,u0,tspan, coeffs)
    println("starting to solve dgl")
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), dtmax=1e-3, reltol=1e-8, abstol=1e-8)
    println("solved dgl")

    Plots.plot()
    p = Plots.plot!(sol,title="Deterministic",  xaxis="n_x",yaxis="n_y", vars=(1,2)) # legend=false
    display(p)
end
deterministic()
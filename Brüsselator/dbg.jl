#include("coeffs.jl")


# function print_array(ar)
#     for el in ar
#         print(el)
#     end
# end

macro dbg_a(vars...)
    ret = "print("
    for var in vars
        ret *= "\"[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] \","
        ret *= "\"" * string(var) * " = \"," * string(var) * ",\"\\n\","
    end
    ret *= ")"
    display(ret)
    Meta.parse(ret)
end


macro dbg_b(vars...)
    printargs = []
    for var in vars
        push!(printargs, "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] ")
        push!(printargs, :($(string(var))))
        push!(printargs, " = ")
        push!(printargs, :( $(esc(var))  ))
        push!(printargs, "\n")
    end
    Expr(:call, println, printargs...)
end


macro dbg_c(vars...)
    cmds = []
    for var in vars
        pref = "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] "
        push!(cmds, :(print($pref)))
        push!(cmds, :(print($(string(var)))))
        push!(cmds, :(print(" = ")))
        #push!(cmds, :(print($(esc(var)))))
        push!(cmds, :(dump($(esc(var)))))
        #push!(cmds, :(print("\n")))
    end
    Expr(:block, cmds...)
end

macro dbg_d(vars...)
    cmds = []
    pref = "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] "
    if length(vars) == 0
        push!(cmds, :(println($pref)))
    end
    for var in vars
        push!(cmds, :(print($pref)))
        push!(cmds, :(print($(string(var)))))
        push!(cmds, :(print(" = ")))
        #push!(cmds, :(print($(esc(var)))))
        push!(cmds, :(println($(esc(var)))))
        #push!(cmds, :(print("\n")))
    end
    Expr(:block, cmds...)
end


# macro my_assert(ex)
#     return :( $ex ? nothing : throw(AssertionError($(string(ex)))) )
# end



#Meta.parse("a=2","b=3")



# struct MyStruct
#     i
# end

# function Base.show(io::IO, x::MyStruct)
#     println("-----")
#     display(io)
#     println(io)
# end

# val = MyStruct(123)
# display(val)

#@dbg_b(func(123))



"""
    @dbg

Displays all arguments and the current linenumber and filename.

Useful for debugging.

Example:
```julia
x = 123
y = 321
@dbg(x, y)
@dbg()
```
would output:
```
[main.jl:3] x = 123
[main.jl:3] y = 321
[main.jl:4] 
```
"""
macro dbg_e(vars...)
    cmds = []
    pref = "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] "
    if length(vars) == 0
        push!(cmds, :(println($pref)))
    end
    push!(cmds, :(temp = []))
    for var in vars
        push!(cmds, :(push!(temp, $(esc(var)))))
    end
    for i in 1:length(vars)
        var = vars[i]
        push!(cmds, :(print($pref)))
        push!(cmds, :(print($(string(var)))))
        push!(cmds, :(print(" = ")))
        push!(cmds, :(display(temp[$i])))
    end
    Expr(:block, cmds...)
end


function to_debug_string_recursion(var, indent=0)
    if indent > 3
        return "to much recursion"
    end
    ret = string(typeof(var))
    if fieldnames(typeof(var)) == ()
        return ret * " " * string(var)
    end
    ret *= " {"
    for key in fieldnames(typeof(var))
        field = "unable to get field"
        if isdefined(var, key)
            field = getfield(var, key)
        end
        # try
        #     field = getfield(var, key)
        # catch ex
        #     #error(ex)
        # end
        ret *= "\n" * "\t"^(indent+1) * string(key) * ": " * to_debug_string_recursion(field, indent+1)
    end
    ret *= "\n" * "\t"^(indent) * "}"
    ret
end

function to_debug_string(var, indent=0)
    return "\t"^indent * to_debug_string_recursion(var, indent)
end

macro dbg(vars...)
    cmds = []
    pref = "[" * Base.Filesystem.relpath(string(__source__.file)) * ":" * string(__source__.line) * "] "
    if length(vars) == 0
        push!(cmds, :(println($pref)))
    end
    push!(cmds, :(temp = []))
    for var in vars
        push!(cmds, :(push!(temp, $(esc(var)))))
    end
    for i in 1:length(vars)
        var = vars[i]
        push!(cmds, :(print($pref)))
        push!(cmds, :(print($(string(var)))))
        push!(cmds, :(println(" = ")))
        push!(cmds, :(println(to_debug_string(temp[$i], 1))))
    end
    Expr(:block, cmds...)
end
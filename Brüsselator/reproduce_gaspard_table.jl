ENV["JULIA_DEBUG"] = "all"
using IncludeGuards

@includeonce "dTdE.jl"
using Printf
import JSON
function reproduce_gaspard_table()
    for k2 in append!([1.2625, 1.275], 1.3:0.05:1.8)
        # for k2 in [1.4]
        bc = gaspard_brüssel_conf(OMEGA = 1000, k1 = 0.5, k2 = k2, k3 = 1, k4 = 1)::gen_brüssel_conf
        dgl = get_limit_dgl(bc)
        xl_sol = limit_cycle_shape(bc)
        @assert xl_sol.t[1] == 0
        T = xl_sol.t[end]

        M_sol = calc_M(bc, xl_sol)
        MP = M_sol(T)

        lambda_1, lambda_2, false_e1, e2, f1, f2 = decomp_matrix(MP)
        @assert approx(1, lambda_1, 0.02)
        e1 = dgl(xl_sol(0), nothing, nothing)
        @assert approx(e1 / norm(e1), false_e1 / norm(false_e1), 1e-2) || approx(-e1 / norm(e1), false_e1 / norm(false_e1), 1e-2)

        f1 = f1 / dot(f1, e1)

        TS = Printf.@sprintf "%.3f" T
        lesS = Printf.@sprintf "%.4f" log(lambda_2) / T

        f1M = [f1[1] * MP[1, 1] + f1[2] * MP[2, 1], f1[1] * MP[1, 2] + f1[2] * MP[2, 2]]
        if !(norm(f1M - f1) < 1e-4)
            println("warning, assertion failed")
        end

        sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
        f1Nf1 = dot(f1, sol.u[end][1:2])

        dTdE = -f1Nf1 / dot(f1, e1)
        dTdES = Printf.@sprintf "%.1f" dTdE

        R = T^2 / (pi * abs(dTdE)) * bc.OMEGA

        gamma = 2 * pi / R / T
        tau = 1 / gamma
        tauS = Printf.@sprintf "%.2f" tau

        # println(rpad(k2, 5, " "), "\t", TS, "\t", lesS, "\t", lpad(dTdES, 8, " "), "\t", tauS)
        println(k2, " ", TS, " ", lesS, " ", dTdES, " ", tauS)
    end
end

function reproduce_basile_data()
    for mu in [5.0]
        bc = basile_coeffs(1000, mu)
        dgl = get_limit_dgl(bc)
        xl_sol = limit_cycle_shape(bc)
        @assert xl_sol.t[1] == 0
        T = xl_sol.t[end]

        # Nres = 30000
        # out_t = collect(map(i -> i / Nres * T, 0:(Nres - 1)))
        # out_s = collect(map(i -> xl_sol(i / Nres * T), 0:(Nres - 1)))
        # JSON.print(open("/data/weissmann/our_brüssel_shape_mu_" * string(mu) * ".json", "w"), (bc, out_t, out_s))

        M_sol = calc_M(bc, xl_sol)
        MP = M_sol(T)

        # function M_dgl(M, bc, t)
        #     jac = Zygote.jacobian(u -> dgl(u, bc, nothing), xl_sol(t))[1]
        #     jac * M
        # end
        # Nres = 1000
        # MP = [1 0; 0 1]
        # for i in 1:Nres
        #     MP += T / Nres * M_dgl(MP, bc, i * T / Nres)
        # end

        # for t in out_t
        #     println(t, "\n", M_sol(t))
        # end

        @debug MP

        # MP = [0.2840043145263212  0.05443890778147772; 3.1972228437731465 0.7569072988531429] # -> dTdE = -5.960491220936046e21
        # MP = [0.28378222          0.05425531;          3.20517099         0.75709112]         # -> dTdE = -1.654676849188272e10

        # MP = [0.99186768 -0.00601184; -1.29251446  0.04450502] # -> dTdE = -1.3629769532489101e21
        # MP = [0.97914119 -0.00602321; -1.26841439  0.04445027] # -> dTdE = -5.485136674953381e8

        lambda_1, lambda_2, false_e1, e2, f1, f2 = decomp_matrix(MP)
        @assert approx(1, lambda_1, 0.02)
        e1 = dgl(xl_sol(0), nothing, nothing)
        @assert approx(e1 / norm(e1), false_e1 / norm(false_e1), 1e-2) || approx(-e1 / norm(e1), false_e1 / norm(false_e1), 1e-2)

        println("eigenvecs: ", e1, " ", e2, " ", f1, " ", f2)
        f1 = f1 / dot(f1, e1)

        println("rescaled: ", f1)

        TS = Printf.@sprintf "%.3f" T
        lesS = Printf.@sprintf "%.4f" log(lambda_2) / T

        # @assert norm(e1 * evs[2] - (MP * e1)) < norm(e1) * 1e-4

        f1M = [f1[1] * MP[1, 1] + f1[2] * MP[2, 1], f1[1] * MP[1, 2] + f1[2] * MP[2, 2]]
        if !(norm(f1M - f1) < 1e-4)
            println("warning, assertion failed")
        end

        # Me1 = MP * e1
        # f1Me1 = f1[1] * Me1[1] + f1[2] * Me1[2]

        # WARNING: using dat.brüssel_conf in module Main conflicts with an existing identifier.
        # ┌ Debug: [0.05623072085014467 0.04256781133580534; 0.201221746713733 0.9909240835958579]
        # │   f1 =
        # │    2-element Vector{Float64}:
        # │     -0.9989843619707599
        # │      0.0450582349618127
        # └ @ Main ~/Sync/DatenVolker/git/sm_with_julia/Brüsselator/reproduce_gaspard_table.jl:37


        # @debug "" MP * [1,0] delta_evolve(bc, xl_sol, T, [1,0,0,0]).u[end]
        # @debug "" MP * [0,1] delta_evolve(bc, xl_sol, T, [0,1,0,0]).u[end]
        # @debug "" transpose(inv(MP)) * [1,0] delta_evolve(bc, xl_sol, T, [0,0,1,0]).u[end]
        # @debug "" transpose(inv(MP)) * [0,1] delta_evolve(bc, xl_sol, T, [0,0,0,1]).u[end]

        # f1 = [12953.58382066,  2894.23935993]
        # e1 = [ 0.01133733 -0.05039632]

        # f1 = [-775.85396008, -576.68123299]
        # e1 = [ 0.23625886, -0.31959141]

        # @debug "" delta_evolve(bc, xl_sol, T, vcat([0,0], [1,0])).u[end]  delta_evolve(bc, xl_sol, T, vcat([0,0], [0,1])).u[end]

        # @debug calc_NT(bc, xl_sol, M_sol)

        sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
        f1Nf1 = dot(f1, sol.u[end][1:2])
        print("f1 Nf1: ", f1, " ", sol.u[end])

        dTdE = -f1Nf1 / dot(f1, e1)

        R = T^2 / (pi * abs(dTdE)) * bc.OMEGA

        @debug "" e1 e2 f1 f2

        println(rpad(mu, 5, " "), "\t", TS, "\t", lesS, "\t", dTdE, "\t", dot(f1, e1), "\t", R)
    end
end

# This function is the same as reproduce_gaspard_table, but with our_brüssel instead of gaspard_brüssel and some @debug's
function complex_check()
    for deltamu in [9.0]
        bc = basile_coeffs(1000, deltamu)
        dgl = get_limit_dgl(bc)
        xl_sol = limit_cycle_shape(bc)
        @assert xl_sol.t[1] == 0
        T = xl_sol.t[end]

        M_sol = calc_M(bc, xl_sol)
        MP = M_sol(T)

        lambda_1, lambda_2, false_e1, e2, f1, f2 = decomp_matrix(MP)
        @assert approx(1, lambda_1, 0.02)
        e1 = dgl(xl_sol(0), nothing, nothing)
        @assert approx(e1 / norm(e1), false_e1 / norm(false_e1), 1e-2) || approx(-e1 / norm(e1), false_e1 / norm(false_e1), 1e-2)

        @debug "" e1 f1 f2
        f1 = f1 / dot(f1, e1)
        @debug "" e1 f1 f2

        TS = Printf.@sprintf "%.3f" T
        lesS = Printf.@sprintf "%.4f" log(lambda_2) / T

        f1M = [f1[1] * MP[1, 1] + f1[2] * MP[2, 1], f1[1] * MP[1, 2] + f1[2] * MP[2, 2]]
        if !(norm(f1M - f1) < 1e-4)
            println("warning, assertion failed")
        end

        sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
        f1Nf1 = dot(f1, sol.u[end][1:2])

        dTdE = -f1Nf1 / dot(f1, e1)
        dTdES = Printf.@sprintf "%.1f" dTdE

        R = T^2 / (pi * abs(dTdE)) * bc.OMEGA

        gamma = 2 * pi / R / T
        tau = 1 / gamma
        tauS = Printf.@sprintf "%.2f" tau

        @debug f1Nf1
        println(rpad(deltamu, 5, " "), "\t", TS, "\t", lesS, "\t", lpad(dTdES, 8, " "), "\t", tauS)
    end
end


# decomp_matrix([0.28400431452654135 0.05443890778153201; 3.1972228437755215 0.7569072988538538])
reproduce_gaspard_table()
# reproduce_basile_data()
# decomp_matrix([4 3; 2 6])
# complex_check()

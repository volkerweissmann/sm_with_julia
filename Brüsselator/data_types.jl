# module dat
using Parameters
#    export our_brüssel_conf, gaspard_brüssel_conf, tang_our_brüssel_conf, gen_brüssel_conf, brüssel_conf, Reaction, State, Rating, Path, calc_input, calc_output, GenPath, CyclicArray, MyType
@with_kw struct brüssel_conf # only exists for legacy reasons
    OMEGA::Int
    A::Float64
    B::Float64
    k1p::Float64
    k1m::Float64
    k2p::Float64
    k2m::Float64
    k3p::Float64
    k3m::Float64
end

@with_kw struct our_brüssel_conf
    OMEGA::Int
    A::Float64
    B::Float64
    k1p::Float64
    k1m::Float64
    k2p::Float64
    k2m::Float64
    k3p::Float64
    k3m::Float64
end
@with_kw struct tang_our_brüssel_conf
    OMEGA::Int
    A::Float64
    B::Float64
    k1p::Float64
    k1m::Float64
    k2p::Float64
    k2m::Float64
    k3p::Float64
    k3m::Float64
end
@with_kw struct gaspard_brüssel_conf
    OMEGA::Int
    k1::Float64
    k2::Float64
    k3::Float64
    k4::Float64
end

@with_kw struct steward_landau_conf
    OMEGA::Int
    a::Float64
    b::Float64
    c::Float64
    d::Float64
end

gen_brüssel_conf = Union{tang_our_brüssel_conf,our_brüssel_conf,gaspard_brüssel_conf,steward_landau_conf}

struct Reaction
    num::Int
    sgn::Int
end
mutable struct State
    X::Int
    Y::Int
end
Base.:+(a::State, b::State) = State(a.X + b.X, a.Y + b.Y)
Base.:-(a::State) = State(-a.X, -a.Y)
Base.:(==)(a::State, b::State) = a.X == b.X && a.Y == b.Y
struct Rating
    R::Float64
    next::Float64
end
# Base.:(isless)(a::dat.Rating, b::dat.Rating) = a.R < b.R || (a.R == b.R  && a.next < b.next)
Base.:(isless)(a::Rating, b::Rating) = a.R < b.R || (a.R == b.R && a.next < b.next)
mutable struct Path
    steps::Array{Reaction}
    x0::Int
    y0::Int
    # rating::dat.Rating
    rating::Rating
end

# --- optimize_position.jl ---
GenPath = Union{Int,Array{Reaction,1}}
@with_kw struct calc_input
    path::GenPath
    OMEGA::Int
    deltamu::Float64
    # fact::Float64
    # delt::Int
end

# --- start_pars.jl ---

struct StartParCache
    parDim::Int
    startDim::Int
    defaultInp::Array{Float64,1}
    known_vals::Array{Tuple{Array{Float64,1},Array{Float64,1}},1}
end

mutable struct CyclicArray <: AbstractArray{Float64,2}
    ar::Array{Float64,2}
end

Base.size(A::CyclicArray) = size(A.ar)

function Base.:getindex(cycar::CyclicArray, cycinds...)
    sizes = size(cycar.ar)
    @assert length(sizes) == length(cycinds)
    inds = map(x -> mod(x[2], 1:x[1]), zip(sizes, cycinds))
    return getindex(cycar.ar, inds...)
end

function Base.:setindex!(cycar::CyclicArray, value, cycinds...)
    sizes = size(cycar.ar)
    @assert length(sizes) == length(cycinds)
    inds = map(x -> mod(x[2], 1:x[1]), zip(sizes, cycinds))
    return setindex!(cycar.ar, value, inds...)
end

# @forward CyclicArray.ar funcname

# abstract type MyArray{S <: Tuple, T, N} <: AbstractArray{T, N} end
# end

# --- framework.jl ---

@with_kw mutable struct FrameState
    coeffs
    size
    theo_omega
    theo_gamma
    fit_omega
    fit_gamma
    cor_t
    cor
    peaks_t
    peaks_u
end

@with_kw mutable struct PlotData
    plot
    data
end

# --- sonstiges

@with_kw mutable struct PointA
    omega
    otau
    omega_error
    otau_error
end

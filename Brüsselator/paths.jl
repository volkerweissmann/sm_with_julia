using IncludeGuards
# @includeonce "data_types.jl"
# using .dat
@includeonce "data_types.jl"
@includeonce "utils.jl"
using LinearAlgebra
using StaticArrays


# Mitigation for https://github.com/JuliaLang/julia/issues/42109
const mitigation_1 = our_brüssel_conf(1, 1, 1, 1, 1, 1, 1, 1, 1)
const mitigation_2 = gaspard_brüssel_conf(1, 1, 1, 1, 1)
const mitigation_3 = gaspard_brüssel_conf(1, 1, 1, 1, 1)::gen_brüssel_conf

const r1p = Reaction(1, +1)
const r1m = Reaction(1, -1)
const r2p = Reaction(2, +1)
const r2m = Reaction(2, -1)
const r3p = Reaction(3, +1)
const r3m = Reaction(3, -1)

const all_reactions = (r1p, r1m, r2p, r2m, r3p, r3m)

function brüssel_nu(step::Reaction)
    if step.sgn == -1
        return -brüssel_nu(Reaction(step.num, +1))
    end
    if step.num == 1
        return SVector(+1, 0)
    elseif step.num == 2
        return SVector(0, +1)
    elseif step.num == 3
        return SVector(+1, -1)
    end
    error("Impossible")
end

function stepDelta(step::Reaction)
    temp = brüssel_nu(step)
    return State(temp[1], temp[2])
end

function Wp(bc::brüssel_conf, react::Reaction, old::State)
    if react.num == 1
        if react.sgn == 1
            return bc.OMEGA * bc.k1p * bc.A
        elseif react.sgn == -1
            return bc.k1m * old.X
        end
    elseif react.num == 2
        if react.sgn == 1
            return bc.OMEGA * bc.k2p * bc.B
        elseif react.sgn == -1
            return bc.k2m * old.Y
        end
    elseif react.num == 3
        if react.sgn == 1
            return bc.k3p / bc.OMEGA^2 * old.X * (old.X - 1) * old.Y
        elseif react.sgn == -1
            return bc.k3m / bc.OMEGA^2 * old.X * (old.X - 1) * (old.X - 2)
        end
    end
    error("unreachable")
end

function is_monocycle(states::Array{State,1})
    for i = 1:length(states)
        for j = 1:length(states)
            if i != j && states[i] == states[j]
                return false
            end
        end
    end
    return true
end

struct InvalidCycle <: Exception end

function calc_path_states(steps::Array{Reaction,1}, State_start::State, closed::Bool)
    len = length(steps)
    if !closed
        len += 1
    end
    states::Array{State} = fill(State(-7, -7), len)
    cur = State_start
    for i = 1:length(steps)
        if cur.X < 0 || cur.Y < 0
            @debug "invalid cycle"
            throw(InvalidCycle())
        end
        states[i] = cur
        cur += stepDelta(steps[i])
    end
    if closed
        @assert cur == State_start
    else
        states[end] = cur
    end
    @assert states[end].X >= 0
    @assert states[end].Y >= 0
    states
end

function build_L(bc::brüssel_conf, steps::Array{Reaction,1}, states::Array{State,1})
    @assert length(steps) == length(states)
    if !is_monocycle(states)
        throw(InvalidCycle())
    end
    dim = length(steps)
    L = zeros(dim, dim)
    cyc(i) = cyclic(dim, i)
    for i = 1:dim
        # i -> i+1
        L[cyc(i + 1), i] += Wp(bc, steps[i], states[i])
        # i+1 -> i
        L[i, cyc(i + 1)] += Wp(bc, Reaction(steps[i].num, -steps[i].sgn), states[cyc(i + 1)])
    end

    for i = 1:dim
        L[i, i] = -L[cyc(i + 1), i] - L[cyc(i - 1), i]
    end
    return L
end

function get_rating(L)
    ar = eigvals(L)
    argsort = sortperm(real(ar))
    first_nontrivial = ar[argsort[end-1]]
    biggest_real_with_nonzero_imag = 0
    for arg in reverse(argsort)
        if imag(ar[arg]) != 0
            biggest_real_with_nonzero_imag = real(ar[arg])
            break
        end
    end
    return Rating(abs(imag(first_nontrivial) / real(first_nontrivial)), real(first_nontrivial) / biggest_real_with_nonzero_imag)
end

function calc_rating(bc::brüssel_conf, steps::Array{Reaction}, State_start)
    states = (nothing, nothing)
    try
        states = calc_path_states(steps, State_start, true)
    catch ex
        if typeof(ex) != InvalidCycle
            print("--------------------------")
            print(ex)
            display(ex)
            rethrow(ex)
        end
        return Rating(-1, 0)
    end
    @assert length(steps) == length(states)
    L = nothing
    try
        L = build_L(bc, steps, states)
    catch ex
        if typeof(ex) != InvalidCycle
            rethrow(ex)
        end
        return Rating(-1, 0)
    end
    return get_rating(L)
end

function calc_border(steps::Array{Reaction}, deltamu)
    affi = deltamu * (count(react -> (react == r3p), steps) - count(react -> (react == r3m), steps))
    border = cot(pi / length(steps)) * tanh(affi / (2 * length(steps)))
    return border
end

function basile_coeffs(OMEGA::Int, deltamu::Float64)
    A = 1.0
    B = 3.0
    k1p = 0.1
    k1m = 1.0
    k2p = 0.1
    k2m = exp(-deltamu) * k1m * k2p * B / (k1p * A)
    return our_brüssel_conf(OMEGA = OMEGA, A = A, B = B, k1p = k1p, k1m = k1m, k2p = k2p, k2m = k2m, k3p = 1.0, k3m = 1.0)::gen_brüssel_conf
end

function tang_basile_coeffs(OMEGA::Int, deltamu::Float64)
    A = 1.0
    B = 3.0
    k1p = 0.1
    k1m = 1.0
    k2p = 0.1
    k2m = exp(-deltamu) * k1m * k2p * B / (k1p * A)
    return tang_our_brüssel_conf(OMEGA = OMEGA, A = A, B = B, k1p = k1p, k1m = k1m, k2p = k2p, k2m = k2m, k3p = 1.0, k3m = 1.0)::gen_brüssel_conf
end

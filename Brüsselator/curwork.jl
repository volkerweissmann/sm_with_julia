include("dTdE.jl")

function curwork_1()
    bc = basile_coeffs(0, 4.1)
    @time xl_sol = limit_cycle_shape(bc)
    @assert xl_sol.t[1] == 0
    T = xl_sol.t[end]
    @time M_sol = calc_M(bc, xl_sol)
    @time NT = calc_NT(bc, xl_sol, M_sol)
    xs = xl_sol.u[1]

    deltax0 = [0, 0 ]
    deltap0 = [0, 1e-6]
    @time x_sol = time_evolve(bc, T, vcat(xs + deltax0, deltap0))
    xf = x_sol(T)[1:2]
    deltaxT = xf - xl_sol.u[end]

    for t in 0:0.1:T
        # @debug (M_sol(t) * deltax0) (x_sol(t)[1:2] - xl_sol(t))
        @debug eigvals(M_sol(t))
    end

    @debug (M_sol(T) * deltax0 + NT * deltap0) (x_sol(T)[1:2] - xl_sol(T))

    nothing
end

function curwork_2()
    bc = basile_coeffs(0, 4.1)
    T = 24.895298969976093
    u0 = [0.1710640684442499, 2.3097705352646107, 0.0, 1e-5]

    dgl(u, p, t) = brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])
    tspan = (0.0, T)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan)
    stepper = DifferentialEquations.init(prob, DifferentialEquations.Tsit5())
    for i in 0:30
        @debug stepper
        DifferentialEquations.step!(stepper)
    end

    # sol = DifferentialEquations.solve(prob, maxiter=1e6)
    # DifferentialEquations.Tsit5(),
end

function curwork()
    µ = 4.1
    bc = basile_coeffs(0, µ)
    gaspard_robustness_via_N(1e-3, µ)
end

curwork_1()
# exit(1)


# ┌ Debug: [0.0027213197346439067, -0.012809568192181866]
# │   (x_sol(t))[1:2] - xl_sol(t) =
# │    2-element Vector{Float64}:
# │      0.0026970858083547322
# │     -0.013561043273206952


# ┌ Debug: [0.0002721319734643907, -0.0012809568192181865]
# │   (x_sol(t))[1:2] - xl_sol(t) =
# │    2-element Vector{Float64}:
# │     -3.7588413435679646e-5
# │     -0.002454863065889601

# ┌ Debug: [0.0013606598673219534, -0.006404784096090933]
# │   (x_sol(t))[1:2] - xl_sol(t) =
# │    2-element Vector{Float64}:
# │      0.0011162201678737715
# │     -0.005728418499335142

# ┌ Debug: [0.008163959203931719, -0.0384287045765456]
# │   (x_sol(t))[1:2] - xl_sol(t) =
# │    2-element Vector{Float64}:
# │      0.010796466048865028
# │     -0.04376250966865669
# └ @ Main ~/Sync/DatenVolker/git/sm_with_julia/Brüsselator/curwork.jl:17

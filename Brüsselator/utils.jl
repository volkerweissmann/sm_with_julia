using Serialization
import LinearAlgebra
include("dbg.jl")
# replace this with mod(x[2], 1:x[1])
function cyclic(dim, i)
    c = i
    while c <= 0
        c += dim
    end
    while c > dim
        c -= dim
    end
    return c
end

function rerange(val::Float64, min::Float64, max::Float64)
    @assert max > min
    period = max - min
    i::Int = 0
    if val > max
        i = -ceil((val - max) / period)
    end
    if val < min
        i = ceil((min - val) / period)
    end
    return val + i * period
end

function save_hydra_precious(var)
    for i in 1:10000
        path = "/data/weissmann/precious/file" * string(i) * ".dat"
        if !isfile(path)
            serialize(path, var)
            return
        end
    end
    throw("unable to safe")
end

function approx(v1, v2, tolerance)
    if LinearAlgebra.norm(v1 - v2) >= tolerance
        # @debug "" v1 v2
    end
    return LinearAlgebra.norm(v1 - v2) < tolerance
end

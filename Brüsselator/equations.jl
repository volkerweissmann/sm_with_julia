
# style: this is the same as brüssel_F
# (6) from https://doi.org/10.1063/1.5032104
function limit_dgl(u, p, t)
    x = u[1]
    y = u[2]
    #@assert p.k3p == p.k3m
    #k3 = p.k3p
    #[p.k1p*p.A - p.k1m*x + k3*(x^2*y-x^3), p.k2p*p.B - p.k2m*y - k3*(x^2*y-x^3)]
    [
        p.k1p * p.A - p.k1m * x + p.k3p * x^2 * y - p.k3m * x^3,
        p.k2p * p.B - p.k2m * y - p.k3p * x^2 * y + p.k3m * x^3,
    ]
end


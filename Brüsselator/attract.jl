# set_cache_path("/home/volker/julia_simple_cache")
ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "paths.jl"
@includeonce "equations.jl"
@includeonce "../gaspard/simu.jl"
@includeonce "dTdE.jl"
import Roots
using Memoize
using Polynomials
# import PolynomialRoots
using Plots
import DifferentialEquations
using JSON

function stationary_point(p::gen_brüssel_conf)
    # 0 = p.k1p * p.A - p.k1m * x + p.k3p * x^2 * y - p.k3m * x^3
    # 0 = p.k2p * p.B - p.k2m * y - p.k3p * x^2 * y + p.k3m * x^3

    if typeof(p) == our_brüssel_conf || typeof(p) == tang_our_brüssel_conf
        # TODO CODESTYLE what is the best way to write this
        xvar = variable()
        yvar = (p.k1p * p.A + p.k2p * p.B - p.k1m * xvar) / p.k2m
        xstatic::Float64 = Roots.find_zero(p.k1p * p.A - p.k1m * xvar + p.k3p * xvar^2 * yvar - p.k3m * xvar^3, 1.0)
        ystatic::Float64 = (p.k1p * p.A + p.k2p * p.B - p.k1m * xstatic) / p.k2m

        @assert isapprox([0, 0], limit_dgl([xstatic, ystatic], p, nothing), atol = 1e-10)
        return [xstatic, ystatic]
    elseif typeof(p) == gaspard_brüssel_conf
        return [p.k1 / p.k4, p.k2 * p.k4 / (p.k1 * p.k3)]
    elseif typeof(p) == steward_landau_conf
        return [0.0, 0.0]
    end
    error("bad type")
end

function print_stationary_points()
    deltamu = 7.0
    bc = basile_coeffs(0, deltamu)
    println(deltamu, "\t", stationary_point(bc))
end

function old_stationary()
    for dm = 1:0.1:10
        bc = basile_coeffs(10000, dm)

        @assert bc.k3p == bc.k3m
        k3 = bc.k3p

        xvar = variable()
        xstatic = Roots.find_zero(bc.k1p * bc.A - bc.k1m * xvar + xvar^2 * k3 / bc.k2m * (bc.k1p * bc.A + bc.k2p * bc.B) + xvar^3 * (-k3 - k3 * bc.k1m / bc.k2m), 1.0)
        ystatic = (bc.k1p * bc.A + bc.k2p * bc.B - bc.k1m * xstatic) / bc.k2m
        @assert isapprox([0, 0], limit_dgl([xstatic, ystatic], bc, nothing), atol = 1e-10)

    end
end

function experiments(bc)
    # [0.42325109640927516, 2.600081409011347] liegt auf dem limit cycle
    stat = stationary_point(bc)
    N = 100
    step = 0.01
    dir = [1, 0]
    flag = true
    for i = 1:N
        pos = stat + dir * i * step
        if flag && pos[1] > 0.42325109640927516
            println("-------------------")
            display(pos)
            println("-------------------")
            flag = false
        end
        display(limit_dgl(pos, bc, nothing))
    end
end

function plot_limit_cycle(bc)
    sol = limit_cycle_shape(bc)

    # println(sol[length(sol) // 2])
    # println(sol[30000])
    # println(length(sol))

    Plots.plot()
    p = Plots.plot!(sol, title = "Deterministic", xaxis = "n_x", yaxis = "n_y", vars = (1, 2)) # legend=false
    display(p)
end


function find_limit_cycle(bc::gen_brüssel_conf)
    #u0 = stationary_point(bc) * 0.8
    u0 = [1, 1]
    tspan = (0.0, 500.0)

    dgl = get_limit_dgl(bc)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan)
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), reltol = 1e-8, abstol = 1e-8)
    return sol.u[end]

    # Plots.plot()
    # p = Plots.plot!(sol,title="Deterministic",  xaxis="n_x",yaxis="n_y", vars=(1,2)) # legend=false
    # display(p)
end

# @memoize
function limit_cycle_shape(bc::gen_brüssel_conf)
    stat = stationary_point(bc)
    u0 = find_limit_cycle(bc)
    alpha0 = atan(stat[1] - u0[1], stat[2] - u0[2])
    # todo if phase falls as t grows, this works just fine, but if phase rises as t grows this does not work correctly
    direction = nothing
    function condition(u, t, integrator)
        angle = atan(stat[1] - u[1], stat[2] - u[2])
        phase = rerange(angle - alpha0, -pi, Float64(pi))
        if direction == nothing
            if phase > 2.0 # 2.0 is an arbitrary real number > 0
                direction = +1
            end
            if phase < -2.0 # -2.0 is an arbitrary real number < 0
                direction = -1
            end
            return -1
        end
        if phase * direction > 1.7 # abritrary real number between 0 and our arbitrary 2.0
            return -1
        end
        return phase * direction
    end
    affect!(integrator) = DifferentialEquations.terminate!(integrator)
    cb = DifferentialEquations.ContinuousCallback(condition, affect!)

    dgl = get_limit_dgl(bc)

    tspan = (0.0, Inf)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan, callback = cb)
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), dense = true, dtmax = 1e-3, reltol = 1e-17, abstol = 1e-17, maxiter = 1e6)
    # sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), dense=true, dtmax=2e-5, reltol=1e-17, abstol=1e-17, maxiter=1e13)

    # @debug "" sol.u[1][1] sol.u[end][1]
    # @debug "" sol.u[1][2] sol.u[end][2]
    @assert isapprox(sol.u[1][1], sol.u[end][1], rtol = 1e-3)
    @assert isapprox(sol.u[1][2], sol.u[end][2], rtol = 1e-3)
    #@debug "" stat u0 alpha0 atan(-sol.u[1][1], -sol.u[1][2]) atan(-sol.u[end][1], -sol.u[end][2])

    return sol
end

function calc_brüsselator_period(bc)
    sol = limit_cycle_shape(bc)
    T = sol.t[end] - sol.t[1]
    return T
end


function calc_dTdE(bc, q0, delta_p)
    oldperiod = calc_brüsselator_period(bc)
    stat = stationary_point(bc)
    F0 = brüssel_F(bc, q0)
    f1 = F0 / norm(F0)
    u0 = vcat(q0, f1 * delta_p)
    dgl(u, p, t) = brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])
    tspan = (0.0, oldperiod)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan)
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), dtmax = 1e-3, reltol = 1e-20, abstol = 1e-20, maxiter = 1e7)
    f1Nf1 = dot(f1, (sol.u[end]-u0)[1:2]) / delta_p
    return (-f1Nf1, oldperiod)
end

function calc_T(bc, q0, E)
    stat = stationary_point(bc)
    F0 = brüssel_F(bc, q0)
    f1 = F0 / norm(F0)
    u0 = vcat(q0, f1 * E / norm(F0))

    dgl(u, p, t) = brüssel_hamilton_dqpdt(bc, u[1:2], u[3:4])

    alpha0 = atan(stat[1] - u0[1], stat[2] - u0[2])
    mintime = Inf
    function condition(u, t, integrator)
        <
        phase = rerange(atan(stat[1] - u[1], stat[2] - u[2]), alpha0 - 2 * pi + 0.4, alpha0 + 0.4) - alpha0
        if phase < -pi / 2 && phase > -pi && mintime == Inf
            mintime = t
        end
        if t >= mintime
            phase
        else
            -1
        end
    end
    affect!(integrator) = DifferentialEquations.terminate!(integrator)
    cb = DifferentialEquations.ContinuousCallback(condition, affect!)

    tspan = (0.0, Inf)
    prob = DifferentialEquations.ODEProblem(dgl, u0, tspan, bc, callback = cb)
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), dtmax = 1e-3, reltol = 1e-20, abstol = 1e-20, maxiter = 1e6)
    T = sol.t[end] - sol.t[1]
    return T
end

function check_liouville(bc, T, xl_sol, M_sol)
    dgl = get_limit_dgl(bc)
    #println("t \t det(M(t)) \t exp(...)")
    for t = 0:0.1:T
        lhs = det(M_sol(t))
        rhs = exp(quadgk(tau -> tr(Zygote.jacobian(u -> dgl(u, bc, nothing), xl_sol(tau))[1]), 0, t)[1])
        #println(t, "\t", lhs, "\t", rhs)
        if !isapprox(lhs, rhs, rtol = 1e9)
            println("warning in check_liouville", lhs / rhs, "\t", lhs, "\t", rhs)
        end
        @assert isapprox(lhs, rhs, rtol = 1e4)
    end
end

function inner_gaspard_robustness_omega(bc)
    dgl = get_limit_dgl(bc)
    xl_sol = limit_cycle_shape(bc)
    @assert xl_sol.t[1] == 0
    T = xl_sol.t[end]
    @debug xl_sol.u[1]

    M_sol = calc_M(bc, xl_sol)
    @assert M_sol(0) == [1 0; 0 1]
    MP = M_sol(T)
    @debug MP
    check_liouville(bc, T, xl_sol, M_sol)

    lambda_1, lambda_2, false_e1, e2, f1, f2 = decomp_matrix(MP)
    #println("should be 1 lambda_1 = ", lambda_1)
    @assert approx(1, lambda_1, 0.02)
    e1 = dgl(xl_sol(0), nothing, nothing)
    @assert approx(e1 / norm(e1), false_e1 / norm(false_e1), 1e-2) || approx(-e1 / norm(e1), false_e1 / norm(false_e1), 1e-2)

    #println("Force = ", e1, " (should be equal) MP * Force = ", MP * e1)
    #@debug f1 [xl_sol(0)[2], -xl_sol(0)[1]]
    #@debug dot(f1, e1) f1 e1

    f1 = f1 / dot(f1, e1) # tod: das können wir wegmachen, denn unten steht schon /dot(f1,e1)
    #@debug f1

    f1M = [f1[1] * MP[1, 1] + f1[2] * MP[2, 1], f1[1] * MP[1, 2] + f1[2] * MP[2, 2]]
    if !(norm(f1M - f1) < 1e-4)
        println("warning, assertion failed")
    end

    sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
    f1Nf1 = dot(f1, sol.u[end][1:2])
    #@debug f1Nf1 sol.u[end]

    dTdE = -f1Nf1 #/ dot(f1, e1)

    R = T^2 / (pi * abs(dTdE)) # (63) in the correlation time paper


    omega = 2 * pi / T
    gamma = 2 * pi / R / T

    return (R, omega, gamma)
end

function gaspard_robustness_omega(bc)
    path = ENV["HOME"] * "/Sync/itp2/cache/gaspard_" * string(typeof(bc)) * JSON.json(bc)
    #println(path)
    if isfile(path)
        return JSON.parsefile(path)
    else
        dat = inner_gaspard_robustness_omega(bc)
        ofile = open(path, "w")
        JSON.print(ofile, dat)
        close(ofile)
        return dat
    end
end

function gaspard_new_format(bc)
    R, omega, gamma = gaspard_robustness_omega(bc)
    PointA(omega, 1 / gamma, NaN, NaN)
end

function gaspard_robustness(bc)
    (R, omega, gamma) = gaspard_robustness_omega(bc)
    return (R * bc.OMEGA, omega, gamma / bc.OMEGA)
end

function compare_nguyen()
    for delta_mu in [3.0, 3.5, 3.8, 3.9, 4.0]
        println(delta_mu, "\t", gaspard_robustness(1 / 1e3, delta_mu))
    end
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # 3.0     36.16325095625133
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # 3.5     39.313866352559955
    # 3.8     697.5004777703122
    # 3.9     774.3942082253362
    # 4.0     3715.6077394949966

    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # 3.0     36.16325095625133
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # ┌ Warning: Interrupted. Larger maxiters is needed.
    # └ @ SciMLBase ~/.julia/packages/SciMLBase/dyqr8/src/integrator_interface.jl:331
    # 3.5     39.313866352559955
    # 3.8     697.5004777703122
    # 3.9     774.3942082253362
    # 4.0     3715.6077394949966
end

# compare_nguyen()

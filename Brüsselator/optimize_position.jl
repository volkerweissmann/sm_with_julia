# import PolynomialRoots
import Roots
using Polynomials
# set_cache_path("/home/volker/julia_simple_cache")
include("paths.jl")
include("equations.jl")
using Plots
import Serialization
import Dates

function triangle(length)
    steps::Array{Reaction,1} = []
    for i in 1:length
        push!(steps, r1m)
    end
    for i in 1:length
        push!(steps, r2p)
    end
    for i in 1:length
        push!(steps, r3p)
    end
    return steps
end

function best_x_y(bc::brüssel_conf, orig, xstart, ystart, fact::Float64, delt::Int)
    x = xstart
    y = ystart
    curbest = calc_rating(bc, orig, State(round(x), round(y)))
    while true
        ratbegin = curbest

        xnew = x * fact + delt
        rat = calc_rating(bc, orig, State(round(xnew), round(y)))
        if rat > curbest
            curbest = rat
            x = xnew
        end

        xnew = x / fact - delt
        rat = calc_rating(bc, orig, State(round(xnew), round(y)))
        if rat > curbest
            curbest = rat
            x = xnew
        end

        ynew = y * fact + delt
        rat = calc_rating(bc, orig, State(round(x), round(ynew)))
        if rat > curbest
            curbest = rat
            y = ynew
        end

        ynew = y / fact - delt
        rat = calc_rating(bc, orig, State(round(x), round(ynew)))
        if rat > curbest
            curbest = rat
            y = ynew
        end

        if curbest == ratbegin
            break
        end
    end
    (Int(round(x)), Int(round(y)), curbest)
end

function start_point_wrapper(inp)
    @assert typeof(inp.path) == Int
    bc = basile_coeffs(inp.OMEGA, inp.deltamu)
    steps = GenPath_to_steps(inp.path)

    xstart = 0.41850337518585834 * 1.2
    ystart = 1.7757863437021588 * inp.deltamu * 0.05
    pars = [inp.OMEGA, inp.path, inp.deltamu]

    starts = [xstart, ystart]

    x, y, rat = best_x_y(bc, steps, starts[1] * inp.OMEGA, starts[2] * inp.OMEGA, inp.fact, inp.delt)
    correct = [x / inp.OMEGA, y / inp.OMEGA]


    return x, y, rat
end

function GenPath_to_steps(GenPath::GenPath)
    if typeof(GenPath) == Int
        return triangle(GenPath)
    elseif typeof(GenPath) == Array{Reaction,1}
        return GenPath
    else
        throw()
    end
end

function calc(inp::calc_input, targets::Array{String})
    x = nothing
    y = nothing
    rat = nothing
    xstatic = nothing
    ystatic = nothing

    bc = basile_coeffs(inp.OMEGA, inp.deltamu)
    if "f" in targets || "x0" in targets || "y0" in targets || "R" in targets
        steps = GenPath_to_steps(inp.path)
    else
        steps = nothing
    end

    xstart = 0.41850337518585834 * inp.OMEGA * 1.2
    ystart = 1.7757863437021588 * inp.OMEGA * inp.deltamu * 0.05

    if "x0" in targets || "y0" in targets || "R" in targets
        if typeof(inp.path) == Int
            x, y, rat = start_point_wrapper(inp)
        else
            x, y, rat = best_x_y(bc, steps, xstart, ystart, 1.001, 0)
        end
    end

    # k1p*A - k1m * x + x^2 * k3/k2m * (k1p*A+k2p*B) + x^3 * (-k3 - k3*k1m/k2m)
    @assert bc.k3p == bc.k3m
    k3 = bc.k3p

    if "xstatic" in targets || "ystatic" in targets
        xvar = variable()
        xstatic = Roots.find_zero(bc.k1p * bc.A - bc.k1m * xvar + xvar^2 * k3 / bc.k2m * (bc.k1p * bc.A + bc.k2p * bc.B) + xvar^3 * (-k3 - k3 * bc.k1m / bc.k2m), 1.0)
        ystatic = (bc.k1p * bc.A + bc.k2p * bc.B - bc.k1m * xstatic) / bc.k2m
        @assert isapprox([0,0], limit_dgl([xstatic, ystatic], bc, nothing), atol=1e-10)
    end

    ret = Dict()
    for key in targets
        if key == "xstart"
            ret[key] = xstart / inp.OMEGA
        elseif key == "ystart"
            ret[key] = ystart / inp.OMEGA
        elseif key == "x0"
            ret[key] = x / inp.OMEGA
        elseif key == "y0"
            ret[key] = y / inp.OMEGA
        elseif key == "R"
            ret[key] = rat.R
        elseif key == "f"
            ret[key] = calc_border(steps, inp.deltamu)
        elseif key == "xstatic"
            ret[key] = xstatic
        elseif key == "ystatic"
            ret[key] = ystatic
        else
            throw(ValueError("bad key"))
        end
    end
    return ret
end

function tarwrap(el)
    if typeof(el) == String
        return el
    elseif typeof(el) == Array{String,1}
        return el[1]
    else
        throw("bad type " * string(typeof(el)))
    end
end

function dispatcher(calfunc, totdict, targets)
    totar = collect(totdict)
    multi_value = map(x -> [x[1], Bool(length(x[2]) != 1)], totar)
    @assert count(x -> x[2] == true, multi_value) == 1
    runvar = Dict(map(reverse, multi_value))[true]

    targetlist = map(tarwrap, targets)
    ret = []
    for val in totdict[runvar]
        # map(x->, totar)
        function nearly_clone(el)
            if el[1] == runvar
                return [el[1], val]
            else
                return [el[1], deepcopy(el[2][1])]
            end
        end
        inp = calc_input(;Dict(map(nearly_clone, totar))...)
        println("calculating for", val)
        push!(ret, calc(inp, targetlist))
    end
    return (totdict, targets, ret)
end
function plotter(p, dispout)
    totdict, targets, datapoints = dispout

    totar = collect(totdict)
    multi_value = map(x -> [x[1], Bool(length(x[2]) != 1)], totar)
    @assert count(x -> x[2] == true, multi_value) == 1
    runvar = Dict(map(reverse, multi_value))[true]

    Plots.xlabel!(p, string(runvar))
    for el in targets
        key = tarwrap(el)
        if typeof(el) == String
            name = el
        else
            name = el[1] * " (" * el[2] * ")"
        end
        dat = map(x -> x[key], datapoints)
        Plots.plot!(p, totdict[runvar], dat, label=name, legend=:outerleft)
    end
end
function dat_plotter(p, data, runvar, whattoplot)
    Plots.xlabel!(p, string(runvar))
    comp = data[1][1]
    plot_dict = Dict()
    for (key, value) in data[1][2]
        plot_dict[key] = []
    end
    runvar_ar = []

    for el in data
        for key in fieldnames(typeof(el[1]))
            if key != runvar
                @assert getfield(el[1], key) == getfield(comp, key)
            end
        end
        push!(runvar_ar, getfield(el[1], runvar))
        for (key, value) in el[2]
            push!(plot_dict[key], value)
        end
    end
    plot_dict[string(runvar)] = runvar_ar
    # display(plot_dict["deltamu"])
    for (key, name) in whattoplot
        if typeof(key) == String
            Plots.plot!(p, runvar_ar, plot_dict[key], label=name, legend=:outerleft)
        else
            Plots.plot!(p, runvar_ar, key(plot_dict), label=name, legend=:outerleft)
        end
    end
end

function remlein_plots()
    p = Plots.plot()
    disp = dispatcher(calc,
        Dict(:path => [triangle(2)], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        ["x0", "y0", "R", "f", "xstatic", "ystatic"])
    plotter(p, disp)
    display(p)
    Plots.savefig(p, "Basic.pdf")

    p = Plots.plot()
    disp = dispatcher(calc,
        Dict(:path => [triangle(3)], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        [["R", "Dreieck"], ["f", "Dreieck"]])
    plotter(p, disp)
    disp = dispatcher(calc,
        Dict(:path => [[r2p, r2p, r2p, r3p, r3p, r1m, r3p, r1m, r1m]], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        [["R", "Tannenbaum"]])
    plotter(p, disp)
    disp = dispatcher(calc,
        Dict(:path => [[r2p, r2p, r2p, r3p, r2m, r3p, r2m, r3m, r1m]], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        [["R", "längliches Dingends"]])
    plotter(p, disp)
    display(p)
    Plots.savefig(p, "Formen.pdf")

    p = Plots.plot()
    for N in [2,3,10,20]
        disp = dispatcher(calc,
            Dict(:path => [triangle(N)], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
            [["x0", string(N)], ["y0", string(N)]])
        plotter(p, disp)
    end
    display(p)
    Plots.savefig(p, "Größen.pdf")
end
# remlein_plots()

function calc_mu_down(K)
    delt = 0
    fact = 1.01
    OMEGA = 100000

    if K < 50
        startp = [Int(round(1 * OMEGA)), Int(round(1 * OMEGA))]
    else
        startp = [Int(round(0.520 * OMEGA)), Int(round(2.53 * OMEGA))]
    end

    results = []
    for deltamu in 10:-0.1:1
        startime = Dates.now()
        println("starting at: ", startime)
        flush(stdout)
        flush(stderr)

        bc = basile_coeffs(OMEGA, deltamu)
        x, y, rat = best_x_y(bc, triangle(K), startp[1], startp[2], fact, delt)
        push!(results, (
            calc_input(path=K, OMEGA=OMEGA, deltamu=deltamu),
            Dict("x0" => x / OMEGA, "y0" => y / OMEGA, "R" => rat.R)))
        startp = [x,y]
        println("finished in: ", Dates.now() - startime)
        println(deltamu, " ", x, " ", y)
        flush(stdout)
        flush(stderr)
    end
    return results
end

function plot_very_big()
    data = Dict(2 => calc_mu_down(2),
    10 => calc_mu_down(10),
    100 => Serialization.deserialize("../big_results/hundred.serjl"),
    1000 => Serialization.deserialize("../big_results/thousand.serjl"))
    # two = calc_mu_down(2)
    # ten = calc_mu_down(10)
    # hundred = Serialization.deserialize("../big_results/hundred.serjl")
    # thousand = Serialization.deserialize("../big_results/thousand.serjl")

    p = Plots.plot(title="x_0")
    for (K, val) in data
        dat_plotter(p, val, :deltamu, [["x0", "K=" * string(K)]])
    end
    disp = dispatcher(calc,
        Dict(:path => [1], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        ["xstatic"])
    plotter(p, disp)
    display(p)
    Plots.savefig(p, "x0.pdf")

    p = Plots.plot(title="y_0")
    for (K, val) in data
        dat_plotter(p, val, :deltamu, [["y0", "K=" * string(K)]])
    end
    disp = dispatcher(calc,
        Dict(:path => [1], :OMEGA => [100000], :deltamu => collect(1:0.1:10)),
        ["ystatic"])
    plotter(p, disp)
    display(p)
    Plots.savefig(p, "y0.pdf")

    p = Plots.plot(title="R/N")

    # for K in [2,10,100,1000]
    #     dat_plotter(p, two, :deltamu, [[d->cot(pi/3/K)/K*tanh.(d["deltamu"]/6), "f/K, K="*string(K)]])
    # end

    dat_plotter(p, two, :deltamu, [[d -> tanh.(d["deltamu"] / 6) / pi, "fmax/N = tanh(mu/6)/pi"]])

    for (K, val) in data
        dat_plotter(p, val, :deltamu, [[d -> d["R"] / 3 / K, "R/N, K=" * string(K)]])
    end
    display(p)

    # affi = deltamu*(count(react->(react == r3p), steps) - count(react->(react == r3m), steps))
    # border = cot(pi/N)*tanh(deltamu/6)

    Plots.savefig(p, "RdK.pdf")
end

function plot_omega()
    p = Plots.plot(xaxis=:log)
    for K in [2,10]
        for deltamu in [3,4,5]
            disp = dispatcher(calc,
                Dict(:path => [triangle(K)], :OMEGA => round.(exp10.(range(2, stop=8, length=100))), :deltamu => [deltamu]),
                [["R", "R für K=" * string(K) * " deltamu=" * string(deltamu)]])
            plotter(p, disp)
        end
    end
    display(p)
    Plots.savefig(p, "OMEGA.pdf")
end
plot_omega()

# plot_very_big()

# calc_mu_down(100)
# Serialization.serialize("hundred.serjl", calc_mu_down(100))
# Serialization.serialize("thousand.serjl", calc_mu_down(1000))
# Serialization.serialize("tenthousand.serjl", calc_mu_down(10000))

using IncludeGuards
@includeonce "from_json.jl"
@includeonce "framework.jl"

DATA = ENV["HOME"] * "/Sync/data"

# Produces the plots that benedikt requested in his 12.08.21 mail
function volker_plots()
    p1dat = Dict()

    p = Plots.plot(xlabel = L"t", ylabel = "cor")
    for Omega in [1e2, 1e3, 1e4]
        cor, dt = get_raw_avg(DATA * "/from_itp2/omega_x0_0_" * string(Int(Omega)) * "_a_0.50.json")
        wcor = cor[1:10000] .- pi
        println(Omega, wcor[1:10])
        Plots.plot!(p, 0:dt:(length(wcor)-1)*dt, wcor, label = L"\Omega = %$Omega")
        p1dat[Omega] = (0:dt:(length(wcor)-1)*dt, wcor)
    end
    # F(x) = 1 + a*sin(x) = 1+sin(x)/2
    # dot x = F(x)
    # Das hier ist falsch, da unseres von 0 bis 2 pi geht und dannach nach -pi bis pi verschoben wird
    # https://www.wolframalpha.com/input/?i=1%2Bsin%28x%29%2F2+%3D+x%27%28t%29
    # x(t) =  2 tan^(-1)(1/2 (    sqrt(3) tan(1/4 (sqrt(3) c_1 + sqrt(3) t)) - 1))
    # Das hier ist richtig:
    # https://www.wolframalpha.com/input/?i=1%2Bsin%28x%2Bpi%29%2F2+%3D+x%27%28t%29
    # x(t) = 2 tan^(-1)(1/2 ( 1 - sqrt(3) tan(1/4 (sqrt(3) c_1 - sqrt(3) t))))

    theo_deter(c_1, t) = 2 * atan(1 / 2 * (1 - sqrt(3) * tan(1 / 4 * (sqrt(3) * c_1 - sqrt(3) * t))))
    begin # Finding c_1, so that theo_deter(c_1, 0) = -pi
        bestval = nothing
        for c_1 = 3.6:0.0000001:3.7
            if theo_deter(c_1, 0) < 0
                bestval = c_1
                # println(c_1, "\t", theo_deter(c_1, 0))
            else
                # println("\t", c_1, "\t", theo_deter(c_1, 0))
            end
        end
    end
    @debug bestval
    @debug theo_deter(bestval, 0)

    xplot = 0:0.01:100
    yplot = theo_deter.(bestval, xplot)
    Plots.plot!(p, xplot, yplot, label = L"\Omega = \infty")
    p1dat[Inf] = (xplot, yplot)

    Plots.savefig(p, DATA * "/plots/volker_1.pdf")
    file1 = open(DATA * "/plots/volker_1.json", "w")
    JSON.print(file1, p1dat)
    close(file1)

    # for special_flip in [false, true]
    for special_flip in [false]
        p2dat = Dict()
        p3dat = Dict()
        p2 = Plots.plot(xlabel = L"a", ylabel = L"R/\Omega")
        p3 = Plots.plot(
            xlabel = L"R_{\mathrm{fit}}/\Omega",
            ylabel = L"R_{\mathrm{th}}/\Omega",
            legend = :bottomright,
        )
        for Omega in [1e2, 1e3, 1e4]
            a_ar = collect(-0.98:0.02:0.98)
            Rs_fit = []
            Rs_fit_err = []
            Rs_theo = []
            borders = []
            for a in a_ar
                if Omega == 1e4
                    path = DATA * Printf.@sprintf "/from_itp2/long_x0_PI_omega_%i_a_%.2f.json" Int(Omega) a
                else
                    path = DATA * Printf.@sprintf "/from_itp2/omega_x0_0_%i_a_%.2f.json" Int(Omega) a
                end
                cor_plot = DATA * Printf.@sprintf "/plots/1D_%i/%f_cor.pdf" Int(Omega) a
                fit_plot = DATA * Printf.@sprintf "/plots/1D_%i/%f_fit.pdf" Int(Omega) a
                cor_plot = false
                fit_plot = false
                (
                    Omega,
                    theo_omega,
                    omega,
                    theo_gamma,
                    gamma,
                    border,
                    cor_t,
                    cor,
                    js,
                    period_error,
                    gamma_error,
                    R_error,
                ) = from_json(path, cor_plot = cor_plot, fit_plot = fit_plot, cutoff = ("ratio", 0.4), special_flip = special_flip)
                R = omega / gamma
                if isnan(R)
                    @debug path
                end
                push!(Rs_fit, R)
                push!(Rs_fit_err, R_error)
                push!(Rs_theo, theo_omega / theo_gamma)
                push!(borders, border)
            end

            if Omega == 1e2
                p2dat["R_th/Omega"] = (a_ar, Rs_theo ./ Omega)
                p2dat["f/Omega"] = (a_ar, borders ./ Omega)
                Plots.plot!(p2, a_ar, Rs_theo ./ Omega, label = L"R_{\mathrm{th}}/\Omega")
                Plots.plot!(p2, a_ar, borders ./ Omega, label = L"f/\Omega")
            end

            p2dat[Omega] = (a_ar, Rs_fit ./ Omega, Rs_fit_err ./ Omega)
            Plots.plot!(
                p2,
                a_ar,
                Rs_fit ./ Omega,
                yerror = Rs_fit_err ./ Omega,
                fillalpha = 0.5,
                label = L"\Omega = %$Omega",
            )


            # begin # Mitigation for https://github.com/JuliaPlots/Plots.jl/issues/3059
            mid = 50
            for ar in [a_ar, Rs_fit, Rs_theo]
                temp = ar[1]
                ar[1] = ar[mid]
                ar[mid] = temp
            end
            # end


            p3dat[Omega] = (Rs_fit ./ Omega, Rs_theo ./ Omega)
            Plots.plot!(
                p3,
                Rs_fit ./ Omega,
                Rs_theo ./ Omega,
                label = L"\Omega = %$Omega",
                seriestype = :scatter,
            )
        end

        Plots.savefig(p2, DATA * "/plots/volker_" * string(special_flip) * "_2.pdf")
        Plots.plot!(p3, [0, 1], [0, 1], label = "equality")
        Plots.savefig(p3, DATA * "/plots/volker_" * string(special_flip) * "_3.pdf")


        file2 = open(DATA * "/plots/volker_" * string(special_flip) * "_2.json", "w")
        file3 = open(DATA * "/plots/volker_" * string(special_flip) * "_3.json", "w")
        JSON.print(file2, p2dat)
        JSON.print(file3, p3dat)
        close(file2)
        close(file3)
    end
end

function long_plots()
    p2 = Plots.plot(xlabel = L"a", ylabel = L"R/\Omega", legend = :bottom)
    p3 = Plots.plot(
        xlabel = L"R_{\mathrm{fit}}/\Omega",
        ylabel = L"R_{\mathrm{th}}/\Omega",
        legend = :bottomright,
    )
    for Omega in [1e4]
        a_ar = collect(-0.98:0.02:0.98)
        Rs_fit = []
        Rs_fit_err = []
        Rs_theo = []
        borders = []
        for a in a_ar
            path =
                Printf.@sprintf "/home/volker/Sync/data/from_itp2/long_x0_PI_omega_%i_a_%.2f.json" Int(
                    Omega,
                ) a
            (
                Omega,
                theo_omega,
                omega,
                theo_gamma,
                gamma,
                border,
                cor_t,
                cor,
                js,
                period_error,
                gamma_error,
                R_error,
            ) = from_json(path, cor_plot = false, fit_plot = false, cutoff = true)
            R = omega / gamma
            push!(Rs_fit, R)
            push!(Rs_fit_err, R_error)
            push!(Rs_theo, theo_omega / theo_gamma)
            push!(borders, border)
        end

        if Omega == 1e4
            Plots.plot!(p2, a_ar, Rs_theo ./ Omega, label = L"R_{\mathrm{th}}/\Omega")
            Plots.plot!(p2, a_ar, borders ./ Omega, label = L"f/\Omega")
        end

        Plots.plot!(
            p2,
            a_ar,
            Rs_fit ./ Omega,
            yerror = Rs_fit_err ./ Omega,
            fillalpha = 0.5,
            label = L"\Omega = %$Omega",
        )

        Plots.plot!(
            p3,
            Rs_fit ./ Omega,
            Rs_theo ./ Omega,
            label = L"\Omega = %$Omega",
            seriestype = :scatter,
        )
    end

    Plots.savefig(p2, "/home/volker/Sync/data/plots/long_2.pdf")
    Plots.plot!(p3, [0, 1], [0, 1], label = "equality")
    Plots.savefig(p3, "/home/volker/Sync/data/plots/long_3.pdf")
end

#from_json(DATA * "/from_itp2/omega_x0_0_100_a_0.96.json", cor_plot=DATA * "/plots/hard_Omega_100_a_0.96.pdf", fit_plot=true, cutoff=true)
#from_json(DATA * "/from_itp2/omega_x0_0_100_a_0.98.json", cor_plot=DATA * "/plots/hard_Omega_100_a_0.98.pdf", fit_plot=true, cutoff=true)
volker_plots()
# long_plots()

using IncludeGuards
@includeonce "../Brüsselator/attract.jl"

bc = steward_landau_conf(OMEGA = 0, a = 1.0, b = 1.0, c = 0.1, d = 0.01)::gen_brüssel_conf
@debug inner_gaspard_robustness_omega(bc)

#1e-10 (8.087737799146189e-33, 0.10999999999989857, 1.3600836566624492e31)
#1e-14 (8.228117427420038e-33, 0.10999999999989857, 1.3368793162008815e31)

# function main()
#     for c = 1:-0.02:0
#         bc = steward_landau_conf(OMEGA = 0, a = 1.0, b = 1.0, c = c, d = 0.0)::gen_brüssel_conf
#         eff_omega = bc.c + bc.d * bc.a / bc.b
#         T = 2 * pi / eff_omega
#         xl_sol(t) = [cos(eff_omega * t), sin(eff_omega * t)]
#         function func(f1)
#             sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
#             return dot(f1, sol.u[end][1:2]) #/ norm(f1)
#         end
#         println(c, "\t", log(10, abs(func([1, 0]))), "\t", log(10, abs(func([0, 1]))))
#     end
# end

#main()


# phiar = collect(0:0.1:2*pi)
# out = map(phi -> func([cos(phi), sin(phi)]), phiar)
# Plots.plot(phiar, out)


# f1 = [1, 0]

# sol = delta_evolve(bc, xl_sol, T, vcat([0, 0], f1))
# @debug sol.u[end][1:2]

# f1Nf1 = dot(f1, sol.u[end][1:2])
# dTdE = -f1Nf1 / 1.0

# R = T^2 / (pi * abs(dTdE))

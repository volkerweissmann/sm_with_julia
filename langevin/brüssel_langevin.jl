ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "../Brüsselator/attract.jl"
@includeonce "../Brüsselator/paths.jl"
@includeonce "from_json.jl"
@includeonce "framework.jl"
import JSON
using StatProfilerHTML

function coord_travo(stat, pos)
    rel_x, rel_y = pos - stat
    r = sqrt(rel_x^2 + rel_y^2)
    # Note that julia defines the atan different than wolfram alpha
    # Note that the rust langevin simulation keeps it in between [0,2*pi]
    theta = atan(rel_x, rel_y)
    if theta < 0
        theta += 2 * pi
    end
    (r, theta)
end

function benedikt_coord_travo(stat, pos)
    rel_x, rel_y = pos - stat
    r = sqrt(rel_x^2 + rel_y^2)
    # Note that julia defines the atan different than wolfram alpha
    # Note that the rust langevin simulation keeps it in between [0,2*pi]
    theta = atan(rel_y, rel_x)
    (r, theta)
end

function check_brüssel_proj()
    bc = basile_coeffs(0, 8.0)
    stat = stationary_point(bc)
    # js = JSON.parsefile("/data/weissmann/brüssel_shape_mu_8.0.json")
    js = JSON.parsefile("/data/weissmann/precious/brüssel_3_offsets.json")
end

function brüssel_tangential(mu)
    bc = basile_coeffs(0, mu)
    sol = limit_cycle_shape(bc)
    T = sol.t[end] - sol.t[1]

    function other(t)
        pos = sol(t)
        F = our_brüssel_F(bc, pos)
        speed = norm(F)

        C2d = our_brüssel_C(bc, pos)
        e_F = F / norm(F)
        c0 = C2d[1, 1] * e_F[1] + C2d[2, 1] * e_F[2]
        c1 = C2d[1, 2] * e_F[1] + C2d[2, 2] * e_F[2]
        C_tang = sqrt(c0^2 + c1^2)
        Q_tang = 1 / 2 * C_tang^2

        return speed * (Q_tang / norm(F)^3)
    end
    function border(t)
        pos = sol(t)
        F = our_brüssel_F(bc, pos)
        speed = norm(F)

        C2d = our_brüssel_C(bc, pos)
        e_F = F / norm(F)
        c0 = C2d[1, 1] * e_F[1] + C2d[2, 1] * e_F[2]
        c1 = C2d[1, 2] * e_F[1] + C2d[2, 2] * e_F[2]
        C_tang = sqrt(c0^2 + c1^2)
        Q_tang = 1 / 2 * C_tang^2

        return speed * (norm(F) / Q_tang)
    end
    quadint = quadgk(other, 0, T)[1]

    period = 0
    otherint = 0
    N = length(sol.u)
    for i = 1:N
        pos = (sol.u[cyclic(N, i + 1)] + sol.u[i]) / 2

        F = our_brüssel_F(bc, pos)
        C2d = our_brüssel_C(bc, pos)
        e_F = F / norm(F)
        c0 = C2d[1, 1] * e_F[1] + C2d[2, 1] * e_F[2]
        c1 = C2d[1, 2] * e_F[1] + C2d[2, 2] * e_F[2]
        C_tang = sqrt(c0^2 + c1^2)
        Q_tang = 1 / 2 * C_tang^2

        pos_next = sol.u[cyclic(N, i + 1)]
        pos_prev = sol.u[i]

        dist = norm(pos_next - pos_prev)

        period += dist / norm(F)
        otherint += dist * (Q_tang / norm(F)^3)
    end
    #println(T / period)
    #println(quadint / otherint)
    if mu < 4 || !isapprox(T, period, rtol = 1e-6)
        return (PointA(NaN, NaN, NaN, NaN), PointA(NaN, NaN, NaN, NaN))
    end
    @assert isapprox(T, period, rtol = 1e-6)
    @assert isapprox(quadint, otherint, rtol = 1e-6)

    omega = 2 * pi / T
    otau = T^3 / ((2 * pi)^2 * quadint)

    borderint = quadgk(border, 0, T)[1]

    return (PointA(omega, otau, NaN, NaN), PointA(omega, borderint / (2 * pi) / omega, NaN, NaN))
end

@enum QuadType quad_whatever = 1 quad_low = 2 quad_high = 3
function brüssel_to_1D(µ; quadtype = quad_whatever, verbose = false)
    bc = basile_coeffs(0, µ)
    sol = limit_cycle_shape(bc)

    # Plots.plot()
    # p = Plots.plot!(sol, title="Deterministic",  xaxis="n_x", yaxis="n_y", vars=(1, 2)) # legend=false
    # display(p)
    T = sol.t[end] - sol.t[1]

    stat = stationary_point(bc)

    F_theta_ar = []
    C_theta_ar = []
    theta_ar = []

    N = length(sol.u)
    if verbose
        println("deltamu = ", µ)
        println("theta x y F_theta c11 c12 c21 c22 C_theta")
    end
    for i = 1:N
        @assert norm(sol.u[cyclic(N, i + 1)] - sol.u[i]) < 4e-3
        q = (sol.u[cyclic(N, i + 1)] + sol.u[i]) / 2
        (r, theta) = benedikt_coord_travo(stat, q)
        x, y = q
        @assert isapprox(x, stat[1] + r * cos(theta))
        @assert isapprox(y, stat[2] + r * sin(theta))

        F = our_brüssel_F(bc, q)
        F_x, F_y = F

        # Used to be F_theta = cos(theta) / r * F_x - sin(theta) / r * F_y , that's wrong
        # robustness_v4.pdf (27)
        F_theta = cos(theta) / r * F_y - sin(theta) / r * F_x
        CT = our_brüssel_C_T(bc, q)
        c11 = CT[1, 1]
        c12 = CT[1, 2]
        c21 = CT[2, 1]
        c22 = CT[2, 2]
        @assert c21 == 0
        C_1theta = c21 * cos(theta) - c11 * sin(theta)
        C_2theta = c22 * cos(theta) - c12 * sin(theta)
        Q_theta = 1 / 2 * (C_1theta^2 + C_2theta^2) / r^2
        C_theta = sqrt(2 * Q_theta)

        if verbose
            println(theta, "\t", x, "\t", y, "\t", F_theta, "\t", c11, "\t", c12, "\t", c21, "\t", c22, "\t", C_theta)
        end

        # Just a change in the sign convention to ensure that period and tau are positive
        F_theta = -F_theta

        push!(F_theta_ar, F_theta)
        push!(C_theta_ar, C_theta)
        push!(theta_ar, theta)
    end

    # C^2/2 = Q

    if isdir("/data/weissmann")
        JSON.print(open("/data/weissmann/brüssel_shape_mu_" * string(µ) * ".json", "w"), Dict("period" => T, "mu" => µ, "theta" => theta_ar, "shape" => sol.u, "F_theta" => F_theta_ar, "C_theta" => C_theta_ar))
    end

    period = 0
    otherint = 0
    for i = 1:N
        pos_next = sol.u[cyclic(N, i + 1)]
        pos_prev = sol.u[i]
        (r, theta_prev) = coord_travo(stat, pos_prev)
        (r, theta_next) = coord_travo(stat, pos_next)


        delta_theta = theta_next - theta_prev

        if abs(delta_theta) > 1
            continue
        end

        @assert delta_theta >= 0


        left_dper = delta_theta / F_theta_ar[i]
        right_dper = delta_theta / F_theta_ar[cyclic(N, i + 1)]

        left_dother = delta_theta * (C_theta_ar[i]^2 / 2 / F_theta_ar[i]^3)
        right_dother = delta_theta * (C_theta_ar[cyclic(N, i + 1)]^2 / 2 / F_theta_ar[cyclic(N, i + 1)]^3)

        if quadtype == quad_whatever
            period += left_dper
            otherint += left_dother
        elseif quadtype == quad_low
            period += min(left_dper, right_dper)
            otherint += max(left_dother, right_dother)
        elseif quadtype == quad_high
            period += max(left_dper, right_dper)
            otherint += min(left_dother, right_dother)
        else
            error("match failed")
        end
    end

    @assert isapprox(period, T, rtol = 1e-3)

    omega = 2 * pi / period
    otau = period^3 / ((2 * pi)^2 * otherint)

    PointA(omega, otau, NaN, NaN)
end

function brüssel_1()
    mp = new_plot(xlabel = L"t", ylabel = "cor", legend = :bottomright)
    for µ in [3.8, 3.9, 4.0]
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error = from_json(
            "/home/volker/Sync/data/from_itp2/brüssel_3_Omega_1000_deltamu_" * string(µ) * ".json",
            cor_plot = false,
            fit_plot = false,
            cutoff = false,
        )
        max = 10000
        plot_line!(mp, cor_t[1:max], cor[1:max], label = Printf.@sprintf "%.1f" µ)
    end
    save_plot(mp, "/home/volker/Sync/data/plots/brüssel_1")
end

function series_1p5(Omega, mus)
    fits_1p5_omega = []
    fits_1p5_gamma = []
    for mu in mus
        el = "brüssel_1p5_Omega_" * string(Int(Omega)) * "_deltamu_" * string(mu) * ".json"
        @debug el
        flush(stdout)
        flush(stderr)
        (
            file_metadata_Omega,
            theo_omega,
            omega,
            theo_gamma,
            gamma,
            border,
            cor_t,
            cor,
            js,
            period_error,
            gamma_error,
            R_error,
        ) = from_json(
            dirname * "/" * el,
            cor_plot = plotdir * "/brüssel_1.5_" * string(Int(Omega)) * "/" * el * "_cor.pdf",
            fit_plot = plotdir * "/brüssel_1.5_" * string(Int(Omega)) * "/" * el * "_fit.pdf",
            cutoff = Int(round(Omega / 2)),
        )
        @assert file_metadata_Omega == Omega
        @assert mu == get_coeff(js, "deltamu")

        push!(fits_1p5_omega, omega)
        push!(fits_1p5_gamma, gamma * Omega)
    end
    return fits_1p5_omega, fits_1p5_gamma
end

function brüssel_plots()
    skip_volker = false
    skip_gaspard = false
    skip_1p5 = true
    skip_normal = true

    # brüssel_1()

    # Quote from basiles paper:
    # The number of coherent oscillations N is defined as the decay time divided by the period.

    dirname = ENV["HOME"] * "/Sync/data/from_itp2"
    plotdir = ENV["HOME"] * "/Sync/data/plots"
    # R = N * 2 pi /ln(2)

    mp_R = new_plot(xlabel = L"\Delta\mu", ylabel = L"R/\Omega", ylims = (0, 0.30), legend = :bottomright)
    mp_omega = new_plot(xlabel = L"\Delta\mu", ylabel = L"\omega", legend = :bottomright)
    mp_tau = new_plot(xlabel = L"\Delta\mu", ylabel = L"1/(\gamma \Omega)", legend = :topleft, ylim = (0, 1.5))
    for Omega in [100, 1000, 10000]
        mus = [3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.4, 6.0, 6.5, 7.0, 7.5, 8.0]
        Ns = []
        Rs = []
        gaspards_R = []
        gaspards_omega = []
        gaspards_gamma = []
        theos_R = []
        theos_omega = []
        theos_gamma = []
        fits_omega = []
        fits_gamma = []

        if !skip_1p5
            fits_1p5_omega, fits_1p5_gamma = series_1p5(Omega, mus)
        end

        for mu in mus
            el = "brüssel_6_Omega_" * string(Int(Omega)) * "_deltamu_" * string(mu) * ".json"
            @debug el
            flush(stdout)
            flush(stderr)

            (
                file_metadata_Omega,
                theo_omega,
                omega,
                theo_gamma,
                gamma,
                border,
                cor_t,
                cor,
                js,
                period_error,
                gamma_error,
                R_error,
            ) = from_json(
                dirname * "/" * el,
                cor_plot = plotdir * "/brüssel_" * string(Int(Omega)) * "/" * el * "_cor.pdf",
                fit_plot = plotdir * "/brüssel_" * string(Int(Omega)) * "/" * el * "_fit.pdf",
                cutoff = Int(round(Omega / 4)),
            )
            println(get_coeff(js, "deltamu"), "\t", omega, "\t", gamma, "\t", omega / gamma / Omega)
            # Calculating omega or gamma analytically for the brüssellator is an unsolved mathematical problem, so this should be nothing
            @assert theo_omega == nothing
            @assert theo_gamma == nothing

            push!(fits_omega, omega)
            push!(fits_gamma, gamma * Omega)

            @assert file_metadata_Omega == Omega
            @assert mu == get_coeff(js, "deltamu")
            period = 2 * pi / omega
            # halftime = log(2) / gamma
            # N = halftime / period
            decaytime = 1 / gamma
            N = decaytime / period

            if Omega == 100 && !skip_volker
                (theo_omega, theo_gamma_Omega) = brüssel_to_1D(get_coeff(js, "deltamu"))
                push!(theos_R, theo_omega / theo_gamma_Omega)
                push!(theos_omega, theo_omega)
                push!(theos_gamma, theo_gamma_Omega)
            end
            if Omega == 100 && !skip_gaspard
                (gaspard_R, gaspard_omega, gaspard_gamma) = (NaN, NaN, NaN)
                try
                    (gaspard_R, gaspard_omega, gaspard_gamma) = gaspard_robustness_omega(basile_coeffs(Omega, get_coeff(js, "deltamu")))
                catch
                    println("warning: gaspard errored out")
                end
                push!(gaspards_R, gaspard_R)
                push!(gaspards_omega, gaspard_omega)
                push!(gaspards_gamma, gaspard_gamma)
            end

            push!(Ns, N)
            push!(Rs, omega / gamma)
        end

        if Omega == 100 || Omega == 1000
            line_a = map(mu -> core_xtx0mxtx0(Omega, mu), mus)

            plot_line!(mp_omega, mus, map(c -> c.omega, line_a), yerror = map(c -> c.omega_error, line_a), label = L"\mathrm{xtx0mxtx0}\,\,\Omega=%$Omega")
            plot_line!(mp_tau, mus, map(c -> c.otau, line_a), yerror = map(c -> c.otau_error, line_a), label = L"\mathrm{xtx0mxtx0}\,\,\Omega=%$Omega")
            plot_line!(mp_R, mus, map(c -> c.omega * c.otau, line_a), yerror = map(c -> c.omega_error * c.otau + c.otau_error * c.omega, line_a), label = L"\mathrm{xtx0mxtx0}\,\,\Omega=%$Omega")
        end

        if Omega == 100 && !skip_volker
            plot_line!(mp_R, mus, theos_R, label = L"\mathrm{volker}")
            plot_line!(mp_omega, mus, theos_omega, label = L"\mathrm{volker}")
            plot_line!(mp_tau, mus, 1 ./ theos_gamma, label = L"\mathrm{volker}")
        end

        if Omega == 100 && !skip_gaspard
            plot_line!(mp_R, mus, gaspards_R, label = L"\mathrm{gaspard}")
            plot_line!(mp_omega, mus, gaspards_omega, label = L"\mathrm{gaspard}")
            plot_line!(mp_tau, mus, 1 ./ gaspards_gamma, label = L"\mathrm{gaspard}")
        end
        if !skip_normal
            plot_line!(mp_R, mus, Rs ./ Omega, label = L"\mathrm{fit}\,\,\Omega = %$Omega")
            plot_line!(mp_omega, mus, fits_omega, label = L"\mathrm{fit}\,\,\Omega = %$Omega")
            plot_line!(mp_tau, mus, 1 ./ fits_gamma, label = L"\mathrm{fit}\,\,\Omega = %$Omega")
        end

        if !skip_1p5
            plot_line!(mp_R, mus, fits_1p5_omega ./ fits_1p5_gamma, label = L"\mathrm{fit 1,5D }\,\,\Omega = %$Omega")
            plot_line!(mp_omega, mus, fits_1p5_omega, label = L"\mathrm{fit 1,5D }\,\,\Omega = %$Omega")
            plot_line!(mp_tau, mus, 1 ./ fits_1p5_gamma, label = L"\mathrm{fit 1,5D }\,\,\Omega = %$Omega")
        end
    end

    save_plot(mp_R, ENV["HOME"] * "/Sync/data/plots/brüssel_2")
    save_plot(mp_omega, ENV["HOME"] * "/Sync/data/plots/brüssel_2_omega")
    save_plot(mp_tau, ENV["HOME"] * "/Sync/data/plots/brüssel_2_tau")
    # "brüssel_2_deltamu_3.8.json"
end

# brüssel_1()
# brüssel_plots()


# js = JSON.parsefile(ENV["HOME"] * "/Sync/data/from_itp2/brüssel_xtx0mxtx0_mu_5.3.json")
# dt = js["trajectory_writeout_timestep"]
# cor = js["avg"]
# cor_t = 0:dt:(length(cor)-1)*dt
# cutoff = length(cor)
# p = Plots.plot()
# Plots.plot!(p, cor_t[1:cutoff], cor[1:cutoff])
# display(p)
# Plots.savefig(p, "temp.pdf")

function core_xtx0mxtx0(Omega, mu, gen_corfit)
    jsonname = "brüssel_xtx0mxtx0_ct_long_Omega_" * string(Int(Omega)) * "_mu_" * string(mu) * ".json"
    if gen_corfit
        cor_plot = ENV["HOME"] * "/Sync/data/plots/brüssel_xtx0mxtx0_" * string(Int(Omega)) * "/" * jsonname * "_cor.pdf"
        fit_plot = ENV["HOME"] * "/Sync/data/plots/brüssel_xtx0mxtx0_" * string(Int(Omega)) * "/" * jsonname * "_fit.pdf"
    else
        cor_plot = false
        fit_plot = false
    end
    (
        file_metadata_Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(
        ENV["HOME"] * "/Sync/data/from_itp2/" * jsonname,
        cor_plot = cor_plot,
        fit_plot = fit_plot,
        cutoff = ("ratio", 0.4),
        force_shift = 0.0,
    )
    @assert Omega == file_metadata_Omega
    @assert mu == get_coeff(js, "mu")

    if period_error == nothing
        period_error = NaN
    end
    if gamma_error == nothing
        gamma_error = NaN
    end
    omega_error = period_error * 2 * pi / (2 * pi / omega)^2

    tau = 1 / gamma
    tau_error = gamma_error / gamma^2

    #our_R_error = omega_error / gamma + gamma_error * omega / gamma^2
    our_R_error = omega * tau * sqrt((omega_error / omega)^2 + (tau_error / tau)^2)
    @assert approx(R_error, our_R_error, 1e-9) || (isnan(R_error) && isnan(our_R_error))

    return PointA(omega = omega, otau = tau / Omega, omega_error = omega_error, otau_error = tau_error / Omega)
end

function core_tang_xtx0mxtx0(Omega, mu, gen_corfit)
    jsonname = "tang_brüssel_xtx0mxtx0_ct_long_Omega_" * string(Int(Omega)) * "_mu_" * string(mu) * ".json"
    if gen_corfit
        cor_plot = ENV["HOME"] * "/Sync/data/plots/brüssel_tang_xtx0mxtx0_" * string(Int(Omega)) * "/" * jsonname * "_cor.pdf"
        fit_plot = ENV["HOME"] * "/Sync/data/plots/brüssel_tang_xtx0mxtx0_" * string(Int(Omega)) * "/" * jsonname * "_fit.pdf"
    else
        cor_plot = false
        fit_plot = false
    end
    (
        file_metadata_Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(
        ENV["HOME"] * "/Sync/data/from_itp2/" * jsonname,
        cor_plot = cor_plot,
        fit_plot = fit_plot,
        cutoff = ("ratio", 0.4),
        force_shift = 0.0,
    )
    @assert Omega == file_metadata_Omega
    @assert mu == get_coeff(js, "mu")

    if period_error == nothing
        period_error = NaN
    end
    if gamma_error == nothing
        gamma_error = NaN
    end
    omega_error = period_error * 2 * pi / (2 * pi / omega)^2

    tau = 1 / gamma
    tau_error = gamma_error / gamma^2

    our_R_error = omega * tau * sqrt((omega_error / omega)^2 + (tau_error / tau)^2)
    @assert approx(R_error, our_R_error, 1e-9) || (isnan(R_error) && isnan(our_R_error))

    return PointA(omega = omega, otau = tau / Omega, omega_error = omega_error, otau_error = tau_error / Omega)
end

function show_core()
    mus = [3.9, 4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.4, 6.0, 6.5, 7.0, 7.5, 8.0]
    # The gaspard code will trap if mu = 3.9
    gaspardmus = [4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0, 5.1, 5.2, 5.3, 5.4, 6.0, 6.5, 7.0, 7.5, 8.0]
    mp_omega = new_plot(xlabel = L"\Delta\mu", ylabel = L"\omega", legend = :bottomright)
    #mp_tau = new_plot(xlabel = L"\Delta\mu", ylabel = L"1/(\gamma \Omega)", legend = :topleft, ylim = (0, 1.0))
    mp_tau = new_plot(xlabel = L"\Delta\mu", ylabel = L"1/(\gamma \Omega)", legend = :topleft)
    mp_R = new_plot(xlabel = L"\Delta\mu", ylabel = L"R/\Omega", ylims = (0, 0.30), legend = :topleft)
    #mp_tau = new_plot(xlabel = L"\Delta\mu", ylabel = L"1/(\gamma \Omega)", legend = :topleft, ylim = (0, 0.3))
    #mp_R = new_plot(xlabel = L"\Delta\mu", ylabel = L"R/\Omega", ylims = (0, 0.10), legend = :topleft)

    function triline(xs, func, label)
        line = map(func, xs)
        println(label, "\t", line[1], "\t", xs[1])

        plot_line!(mp_omega, xs, map(c -> c.omega, line), yerror = map(c -> c.omega_error, line), label = label)
        plot_line!(mp_tau, xs, map(c -> c.otau, line), yerror = map(c -> c.otau_error, line), label = label)
        plot_line!(mp_R, xs, map(c -> c.omega * c.otau, line), yerror = map(c -> c.omega * c.otau * sqrt((c.omega_error / c.omega)^2 + (c.otau_error / c.otau)^2), line), label = label)
    end

    for Omega in [1e2, 1e3, 1e4]
        triline(mus, mu -> core_xtx0mxtx0(Omega, mu, false), L"\mathrm{xtx0mxtx0}\,\,\Omega=%$Omega")
        triline(mus, mu -> core_tang_xtx0mxtx0(Omega, mu, false), L"\mathrm{tang xtx0mxtx0}\,\,\Omega=%$Omega")
    end

    triline(mus, mu -> brüssel_tangential(mu)[1], "Tang Integral")
    triline(mus, mu -> brüssel_tangential(mu)[2], "Border")
    triline(mus, mu -> brüssel_to_1D(mu, quadtype = quad_low), "Theta Integral low")
    triline(mus, mu -> brüssel_to_1D(mu, quadtype = quad_high), "Theta Integral high")
    triline(gaspardmus, mu -> gaspard_new_format(basile_coeffs(0, mu)), "Gaspard 2D")
    triline(gaspardmus, mu -> gaspard_new_format(tang_basile_coeffs(0, mu)), "Gaspard Tang")


    save_plot(mp_tau, ENV["HOME"] * "/Sync/data/plots/brüssel_tau")
    save_plot(mp_omega, ENV["HOME"] * "/Sync/data/plots/brüssel_omega")
    save_plot(mp_R, ENV["HOME"] * "/Sync/data/plots/brüssel_R")
end

#brüssel_plots()
show_core()
#brüssel_to_1D(8.0, verbose = true)
#brüssel_tangential(8.0)

# dirname = "/home/volker/Sync/data/from_itp2"
# (
#     file_metadata_Omega,
#     theo_omega,
#     omega,
#     theo_gamma,
#     gamma,
#     border,
#     cor_t,
#     cor,
#     js,
#     period_error,
#     gamma_error,
#     R_error,
# ) = from_json(
#     dirname * "/" * "brüssel_5_Omega_10000_deltamu_3.9.json",
#     cor_plot=false,
#     fit_plot=false,
#     cutoff=false,
# )

# frame = 6000

# p = Plots.plot(cor_t[1:frame], cor[1:frame])
# Plots.vline!(p, [13.93, 28.06, 56.38])
# display(p)

# @debug guess_period(cor_t, cor)

# @debug omega

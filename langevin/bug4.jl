using Distributed
addprocs(64)
import DifferentialEquations
@everywhere f(du, u, p, t) = du .= 1.
@everywhere g(du, u, p, t) = du .= 1e-3
prob = DifferentialEquations.SDEProblem(f, g, [0.], (0., 100.))
ensembleprob = DifferentialEquations.EnsembleProblem(prob)
println("starting main simulation")
flush(stdout)
@time DifferentialEquations.solve(ensembleprob, DifferentialEquations.EnsembleThreads(), trajectories=1)

using Distributed
addprocs(4)
function func()
    return 123
end
sum_2 = @distributed (+) for i = 1:2^8
    func()
end

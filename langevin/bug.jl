import DifferentialEquations
f(du, u, p, t) = du .= 1.
g(du, u, p, t) = du .= 1.
prob = DifferentialEquations.SDEProblem(f, g, [0.], (0., 10.))
ensembleprob = DifferentialEquations.EnsembleProblem(prob)
sol = DifferentialEquations.solve(ensembleprob, DifferentialEquations.EnsembleThreads(), dt=0., trajectories=4)
cor = DifferentialEquations.EnsembleAnalysis.timeseries_steps_mean(sol)

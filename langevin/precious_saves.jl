module precious_saves
    using Parameters
    export langevin_1D_trajectories_v1

    @with_kw mutable struct langevin_1D_trajectories_v1
    rescale::Float64
    x0::Float64
    time::Float64
    multi::Int
    Omega::Float64
    Q_str::String
    F_str::String
    sol::Any
end
end

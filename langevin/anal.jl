import Statistics
import Plots
import LsqFit
using QuadGK

@enum gp_state gp_beginning gp_very_high gp_very_low gp_mid_high gp_mid_low
function guess_period_old(cor_t, cor)::Union{Nothing,Float64}
    @assert length(cor_t) == length(cor)
    border_high = maximum(cor) / 5
    border_low = minimum(cor) / 5
    state = gp_beginning
    last_zero = nothing
    periods = []
    # todo: What if one peak is too low to make the cut, but the next cut is higher and makes the cut -> Then one element of periods is double what it should be
    for i in 1:length(cor)
        new_state = state
        # @debug "" cor_t[i] cor[i] state
        if cor[i] < border_low
            new_state = gp_very_low
        elseif cor[i] > border_high
            new_state = gp_very_high
        end
        sign_change = false
        if state == gp_very_high && cor[i] < 0
            new_state = gp_mid_low
            sign_change = true
        elseif state == gp_very_low && cor[i] > 0
            new_state = gp_mid_high
            sign_change = true
        end
        if sign_change && last_zero != nothing
            push!(periods, 2 * (cor_t[i] - last_zero))
        end
        if sign_change
            last_zero = cor_t[i]
        end
        state = new_state
    end
    if length(periods) == 0
        return nothing
    else
        return Statistics.mean(periods)
    end
end

function guess_period(cor_t, cor)::Union{Nothing,Float64}
    @assert length(cor_t) == length(cor)
    absmax = min(abs(maximum(cor)), abs(minimum(cor)))
    threshold = absmax / 5

    border_low = -threshold
    border_high = threshold

    state = gp_beginning
    last_zero = nothing
    # todo: What if one peak is too low to make the cut, but the next cut is higher and makes the cut -> Then one element of periods is double what it should be
    for i in 1:length(cor)
        new_state = state
        # @debug "" cor_t[i] cor[i] state
        if cor[i] < border_low
            new_state = gp_very_low
        elseif cor[i] > border_high
            new_state = gp_very_high
        end
        sign_change = false
        if state == gp_very_high && cor[i] < 0
            new_state = gp_mid_low
            sign_change = true
        elseif state == gp_very_low && cor[i] > 0
            new_state = gp_mid_high
            sign_change = true
        end
        if sign_change && last_zero != nothing
            return 2 * (cor_t[i] - last_zero)
        end
        if sign_change
            last_zero = cor_t[i]
        end
        state = new_state
    end
    @debug "nothing"
    return nothing
end

# Only finds maximas, no minimas
# todo: make sure that period_estimate is not n * actual_estimate with n being an integer
function robust_find_peaks(cor, period_estimate, min_peak_height, min_peak_height_period, search_start, index_to_time, delta_time_to_delta_index, delta_index_to_delta_time)
    @assert period_estimate > 0
    @assert period_estimate < Inf

    period_estimate = delta_time_to_delta_index(period_estimate)
    delta = Int(round(period_estimate / 3))
    # delta = 32

    periods = []
    peaks_t = []
    peaks_u = []
    function add(i)
        push!(peaks_t, index_to_time(i))
        push!(peaks_u, cor[i])
    end

    if search_start
        pos = argmax(cor)
    else
        @assert argmax(cor[1:delta]) == 1
        pos = 1
    end
    @assert cor[pos] > 0
    add(pos)

    while pos + period_estimate + delta <= length(cor)
        next = -1 + pos + period_estimate - delta + argmax(cor[pos + period_estimate - delta:pos + period_estimate + delta])
        if ! ( cor[next - 1] <= cor[next] >= cor[next + 1])
            # println("warning: ! ( cor[next - 1] <= cor[next] >= cor[next + 1])")
        end
        if cor[next] < min_peak_height
            break
        end
        add(next)
        if cor[next] > min_peak_height_period
            push!(periods, next - pos)
        end
        if length(periods) >= 1
            period_estimate = periods[1]
        end
        pos = next
    end

    if length(peaks_t) < 2
        return (NaN, peaks_t, peaks_u, NaN)
    end
    if length(periods) != 0
        time_periods = map(i -> delta_index_to_delta_time(i), periods)
        return (Statistics.mean(time_periods), peaks_t, peaks_u, Statistics.std(time_periods))
    else
        return (peaks_t[2] - peaks_t[1], peaks_t, peaks_u, 0)
    end
end

ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
using ProgressMeter
@includeonce "../Brüsselator/attract.jl"
µ = 5.3
bc = basile_coeffs(0, µ)
dgl = get_limit_dgl(bc)

sol = limit_cycle_shape(bc)
T = sol.t[end] - sol.t[1]
q0 = sol.u[1]

function random_point()
    prob = DifferentialEquations.ODEProblem(dgl, q0, (0, rand() * T))
    sol = DifferentialEquations.solve(prob, DifferentialEquations.Tsit5(), reltol = 1e-17, abstol = 1e-17, maxiter = 1e6)
    sol.u[end]
end

sample = collect(@showprogress map(i -> random_point(), 1:2^16))

JSON.print(open("/data/weissmann/brüssel_sample_mu_" * string(µ) * ".json", "w"), Dict("mu" => µ, "sample" => sample))

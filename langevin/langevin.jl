ENV["JULIA_DEBUG"] = "all"
using BenchmarkTools: Statistics
import DifferentialEquations # , Test, StaticArrays
import FFTW
import StatsBase
using BenchmarkTools
using Caching
using DifferentialEquations.EnsembleAnalysis
using Serialization
include("../gaspard/simu.jl")
include("precious_saves.jl")
using .precious_saves
include("anal.jl")
import JSON

function solver_example()
    # # du = 2u dt + dW_t

    # f(du,u,p,t) = du .= 2.0 * u
    f(du, u, p, t) = du .= 0.0 * u

    g_i(du, u, p, t) = du .= 2
    function g(du, u, p, t)
        # g_i(du,u,p,t)
        du[1, 1] = 2
        du[2, 2] = 2
    end


    T = 17.0
    N = 200

    dim = 2

    sum_u = zeros(dim)
    sum_squares = zeros(dim)
    for _ = 1:N
        u0 = zeros(dim)
        tspan = (0.0, T)
        prob = DifferentialEquations.SDEProblem(f, g, u0, tspan, noise_rate_prototype = zeros(dim, dim))
        sol = DifferentialEquations.solve(prob, EM(), dt = T / N)
        sum_u += sol.u[end]
        sum_squares += sol.u[end] .^ 2
    end
    display(Statistics.mean(sum_u) / N)
    display(Statistics.mean(sum_squares) / N)
    display(17.0 * 2.0 * 2.0)
end


function brüssellator()
    bc = basile_coeffs(1000, 5.3)
    function F(du, u, bc, t)
        du = brüssel_F(bc, u)
        nothing
    end
    function g(du, u, bc, t)
        qx, qy = u
        du[1, 1] = sqrt(bc.k1p * bc.A + bc.k1m * qx) / sqrt(bc.OMEGA)
        du[2, 1] = 0
        du[1, 2] = 0
        du[2, 2] = sqrt(bc.k2p * bc.B + bc.k2m * qy) / sqrt(bc.OMEGA)
        du[1, 3] = sqrt(bc.k3p * qx^2 * qy + bc.k3m * qx^3) / sqrt(bc.OMEGA)
        du[2, 3] = -sqrt(bc.k3p * qx^2 * qy + bc.k3m * qx^3) / sqrt(bc.OMEGA)
    end
    time = 17.0
    dim = 2
    u0 = ones(dim)
    tspan = (0.0, time)
    prob = DifferentialEquations.SDEProblem(F, g, u0, tspan, bc, noise_rate_prototype = zeros(dim, 3))

    nothing
end

function calc_autocor(xs)
    xs = xs - ones(length(xs)) * Statistics.mean(xs)
    var = Statistics.var(xs)
    yft = FFTW.fft(xs)
    power_spektrum = abs2.(yft)
    cor = real(FFTW.ifft(power_spektrum))
    return cor ./ length(cor) #/ var
end

function verify_autocor()
    #testdata = [1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1]
    #@debug StatsBase.autocov(testdata, demean = false)
    #@debug StatsBase.autocor(testdata, demean = false)
    pers = 5
    dt = 2 * pi / 100
    testdata2 = sin.(0:dt:2*pi*pers) .+ 1
    testdata3 = 0.5 * sin.(0:dt:2*pi*pers)
    testdata1 = pi / 2 * sin.(0:dt:2*pi*pers) .+ 2
    p = Plots.plot()
    cor = calc_autocor(testdata1)
    Plots.plot!(p, cor)
    #Plots.plot!(p, cos.(0:dt:2*pi*pers))
    Plots.plot!(p, calc_autocor(testdata2))
    Plots.plot!(p, calc_autocor(testdata3))
    display(p)
end

function langevin_simulation(u0, F, Q, Omega, time, dt)
    function f(du, u, p, t)
        mdu = F(u)
        for i = 1:length(mdu)
            du[i] = mdu[i]
        end
    end
    #f(du, u, p, t) = du .= 2.0 * u
    g(du, u, p, t) = du .= sqrt(2 * Q(u) / Omega)
    tspan = (0.0, time)
    prob = DifferentialEquations.SDEProblem(f, g, u0, tspan)
    # We need a fixed step size, so that sol.u tells the whole truth
    @time sol = DifferentialEquations.solve(prob, DifferentialEquations.EM(), dt = dt)
    return sol
end

function post_benedikt()
    time = 2 * pi * 40000
    dt = 0.01
    Omega = 1000
    # Q(u) = 1.0 * 0
    # F(u) = [1.0] #+ 0.5 * sin(u)
    # u0 = [1]

    # Q(u) = 1.0
    # F(u) = [u[2] - u[1] * 0.01, -u[1] - u[2] * 0.01]
    # u0 = [1, 0]


    bc = basile_coeffs(Omega, 4.0)
    function F(du, u, p, t)
        mdu = our_brüssel_F(bc, u)
        du[1] = mdu[1]
        du[2] = mdu[2]
    end
    function g(du, u, p, t)
        qx, qy = u
        du[1, 1] = sqrt(bc.k1p * bc.A + bc.k1m * qx) / sqrt(bc.OMEGA)
        du[2, 1] = 0
        du[1, 2] = 0
        du[2, 2] = sqrt(bc.k2p * bc.B + bc.k2m * qy) / sqrt(bc.OMEGA)
        du[1, 3] = sqrt(bc.k3p * qx^2 * qy + bc.k3m * qx^3) / sqrt(bc.OMEGA)
        du[2, 3] = -sqrt(bc.k3p * qx^2 * qy + bc.k3m * qx^3) / sqrt(bc.OMEGA)
    end
    u0 = ones(2)
    tspan = (0.0, time)
    prob = DifferentialEquations.SDEProblem(F, g, u0, tspan, nothing, noise_rate_prototype = zeros(2, 3))
    sol = DifferentialEquations.solve(prob, DifferentialEquations.EM(), dt = dt)

    #sol = langevin_simulation(u0, F, Q, Omega, time, dt)
    function get_x(q)
        #mod2pi(q[1])
        q[1]
    end
    xs = get_x.(sol.u[length(sol.u)÷2:end])

    @time cor = calc_autocor(xs)
    cor = cor[1:20000]
    cor_t = 0:dt:(length(cor)-1)*dt

    @debug "" maximum(cor) minimum(cor)

    p = Plots.plot()
    Plots.plot!(p, cor_t, cor, label = "Eine lange")

    # js = JSON.parsefile(ENV["HOME"] * "/Sync/data/from_itp2/brüssel_sampled_mu_5.3.json")
    # dt = js["trajectory_writeout_timestep"]
    # cor = js["avg"]
    # cor = cor - ones(length(cor)) * Statistics.mean(cor)
    # Plots.plot!(p, cor, label = "Ensemble Mittel")

    display(p)
    Plots.savefig("autocor.pdf")

    return

    @. model(t, p) = exp(-t * p[1])
    p0 = [1e-3]
    skipper = 20
    fit = LsqFit.curve_fit(model, cor_t, cor, p0)
    gamma = LsqFit.coef(fit)[1]

    return

    println("finished stochastic simulation")
    # curve = []
    # curve_t = []
    # for i in 1:1000
    #     el = sol.u[i]
    #     push!(curve_t, sol.t[i])
    #     push!(curve, mod2pi(el[1]))
    # end
    # p = Plots.plot(curve_t, curve);
    # # Plots.savefig(p, "x(t).pdf")
    # display(p)
    # return

    theo_omega = 2 * pi * 1 / quadgk(x -> 1 / F(x), 0, Length)[1]
    theo_gamma = 1 / Omega * (2 * pi)^2 * quadgk(x -> 1 / F(x), 0, Length)[1]^-3 * quadgk(x -> Q(x) / F(x)^3, 0, Length)[1]

    @debug Int(round(rescale * 2 * pi / theo_omega))

    cor = sum(Iterators.map(j -> calc_autocor_2(sol, j, true), 1:multi)) / multi
    println("finished calculating correlation function")

    (period, peaks_t, peaks_u) = robust_find_peaks(cor, Int(round(rescale * 2 * pi / theo_omega)), 1e-2, false)
    omega = 2 * pi / period

    @debug "" omega * rescale theo_omega
    @debug "" gamma * rescale theo_gamma


    max = length(sol.t)
    p = Plots.plot()
    Plots.plot!(p, index_to_time(1):index_to_time(max), cor[1:max], label = "C(t)")
    Plots.plot!(p, peaks_t[skipper:end], peaks_u[skipper:end], label = "peaks")
    Plots.plot!(p, peaks_t, model(peaks_t, LsqFit.coef(fit)), label = "fit")
    Plots.savefig(p, "langevin.pdf")
    display(p)
end

function benchmark_em()
    multi = 256
    rescale = 100
    Q(u) = 1.0
    Omega = 1000.0
    F(u) = 1.1 + sin(u)
    time = 2 * pi * 100 * rescale
    dt = 1 # You cannot change this, change rescale instead
    f(du, u, p, t) = du .= F.(u) / rescale
    g(du, u, p, t) = du .= sqrt(2 * Q(u) / Omega) / sqrt(rescale)
    u0 = zeros(multi)
    tspan = (0.0, time)
    prob = DifferentialEquations.SDEProblem(f, g, u0, tspan)
    @time sol = DifferentialEquations.solve(prob, DifferentialEquations.EM(), dt = dt)
    return sol
end

function heavy_langevin(cmd::langevin_1D_trajectories_v1)
    if cmd.Q_str == "Q(u) = 1."
        Q(u) = 1.0
    end
    if cmd.F_str == "F(u) = 1.2 + sin(u)"
        F(u) = 1.2 + sin(u)
    end
    # eval(Meta.parse(cmd.F_str))
    # eval(Meta.parse(cmd.Q_str))

    f(du, u, p, t) = du .= F.(u) / cmd.rescale
    g(du, u, p, t) = du .= sqrt(2 * Q(u) / cmd.Omega) / sqrt(cmd.rescale)
    u0 = [cmd.x0]
    tspan = (0.0, cmd.time)
    prob = DifferentialEquations.SDEProblem(f, g, u0, tspan)
    ensembleprob = DifferentialEquations.EnsembleProblem(prob)
    println("starting main simulation")
    flush(stdout)
    @time cmd.sol = DifferentialEquations.solve(ensembleprob, DifferentialEquations.EnsembleThreads(), trajectories = cmd.multi)
    println("finished main simulation")
    flush(stdout)
    save_hydra_precious(cmd)
end

function hydra_1()
    # Length = 2 * pi
    for Omega in [1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7]
        rescale = 1
        x0 = 1.3
        time = 2 * pi * 4 * rescale
        multi = 2^20
        Q_str = "Q(u) = 1."
        F_str = "F(u) = 1.2 + sin(u)"

        cmd = langevin_1D_trajectories_v1(
            rescale = rescale,
            x0 = x0,
            time = time,
            multi = multi,
            Omega = Omega,
            Q_str = Q_str,
            F_str = F_str,
            sol = nothing,
        )
        heavy_langevin(cmd)
    end
end

function assym()
    time = 2 * pi * 10000
    dt = 0.01
    Omega = 1000
    Q(u) = 1.0
    F(u) = [1 + cos(u[1]) * 0.7]
    # function F(u)
    #     if mod2pi(u[1]) < 5
    #         [1.0]
    #     else
    #         [0.2]
    #     end
    # end
    u0 = [0]

    sol = langevin_simulation(u0, F, Q, Omega, time, dt)
    function get_x(q)
        #mod2pi(q[1])
        cos(q[1])
    end
    xs = get_x.(sol.u[length(sol.u)÷2:end])

    @time cor = calc_autocor(xs)
    cor = cor[1:40000]
    cor_t = 0:dt:(length(cor)-1)*dt

    @debug "" maximum(cor) minimum(cor)

    p = Plots.plot()
    Plots.plot!(p, cor)
    display(p)
end

#assym()
#verify_autocor()
post_benedikt()

# JULIA_EXCLUSIVE=1 julia-1.6.2/bin/julia --threads 128 langevin/langevin.jl &> out.txt &

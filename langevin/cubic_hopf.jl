using IncludeGuards
# using InverseLaplace
# using Interpolations
@includeonce "../Brüsselator/data_types.jl"
@includeonce "../Brüsselator/attract.jl"
@includeonce "from_json.jl"
function cubic_hopf_2()
    # p1dat = Dict()
    p2dat = Dict()
    p3dat = Dict()

    # p = Plots.plot(xlabel=L"t", ylabel="cor")
    # for Omega in [100, 1000, 10000]
    #     Omega = Int(Omega)
    #     for par_omega in [-2, -1, 1, 2, 3]
    #         (
    #             dt,
    #             Omega,
    #             avg,
    #             theo_omega,
    #             theo_gamma,
    #             border,
    #             index_to_time,
    #             delta_index_to_delta_time,
    #             delta_time_to_delta_index,
    #             cor,
    #             js,
    #         ) = read_rust_json(
    #             Printf.@sprintf "/home/volker/Sync/data/from_itp2/cubic_hopf_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega 1
    #         )
    #         wcor = cor[1:1000]
    #         Plots.plot!(
    #             p,
    #             0:dt:(length(wcor) - 1) * dt,
    #             wcor,
    #             label=L"\Omega = %$Omega,\,\omega = %$par_omega",
    #         )
    #         p1dat[par_omega] = (0:dt:(length(wcor) - 1) * dt, wcor)
    #     end
    # end
    # Plots.savefig(p, "/home/volker/Sync/data/plots/cubic_4.pdf")

    p2 = Plots.plot(
        xlabel = L"\mu",
        ylabel = L"R/\Omega",
        legend = :outertopleft,
        size = (800, 400),
    )
    p3 = Plots.plot(
        xlabel = L"R_{\mathrm{fit}}/\Omega",
        ylabel = L"R_{\mathrm{th}}/\Omega/2",
        legend = :outertopleft,
        size = (800, 400),
    )
    flag = true
    for Omega in [100, 1000, 10000]
        Omega = Int(Omega)
        for par_omega in [-2, -1, 1, 2, 3]
            a_ar = collect(0.4:0.2:4.2)
            Rs_fit = []
            Rs_fit_err = []
            Rs_theo = []
            borders = []
            Rs_gaspard = []
            for mu in a_ar
                if par_omega + 1 * mu == 0 # If the force vanishes, we get no oscillations
                    push!(Rs_fit, 0)
                    push!(Rs_fit_err, 0)
                    push!(Rs_theo, 0)
                    push!(borders, 0)
                    push!(Rs_gaspard, 0)
                    continue
                end
                path =
                # Printf.@sprintf "/home/volker/Sync/data/from_itp2/cubic_hopf_theta_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega mu
                    Printf.@sprintf "%s/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" ENV["HOME"] Omega par_omega mu
                (
                    Omega,
                    theo_omega,
                    omega,
                    theo_gamma,
                    gamma,
                    border,
                    cor_t,
                    cor,
                    js,
                    period_error,
                    gamma_error,
                    R_error,
                ) = from_json(path, cor_plot = false, fit_plot = false, cutoff = false)
                R = omega / gamma
                push!(Rs_fit, R)
                push!(Rs_fit_err, R_error)
                push!(Rs_theo, theo_omega / theo_gamma)
                push!(borders, border)

                if Omega == 100
                    R_fit = R
                    (R, omega, gamma) = gaspard_robustness_omega(steward_landau_conf(OMEGA = Omega, a = mu, b = 1.0, c = par_omega, d = 1.0)::gen_brüssel_conf)
                    push!(Rs_gaspard, R * Omega)
                    #println(R_fit / Omega, "\t", R)
                    if !isapprox(theo_omega / theo_gamma / Omega / 2, R, rtol = 1e-1)
                        println(mu, "\t", par_omega)
                        println(theo_omega / theo_gamma / Omega / 2, "\t", R)
                    end
                end
            end

            if flag
                p2dat["R_th_"*string(par_omega)] = (a_ar, Rs_theo ./ Omega)
                Plots.plot!(
                    p2,
                    a_ar,
                    Rs_theo ./ Omega ./ 2,
                    label = L"R_{\mathrm{th}}/2\,\mathrm{for}\,\Omega = %$Omega,\,\omega = %$par_omega",
                )
            end

            p2dat[par_omega] = (a_ar, Rs_fit ./ Omega, Rs_fit_err ./ Omega)
            @assert length(a_ar) == length(Rs_fit)
            @assert length(a_ar) == length(Rs_fit_err)
            Plots.plot!(
                p2,
                a_ar,
                Rs_fit ./ Omega,
                yerror = Rs_fit_err ./ Omega,
                label = L"R_{\mathrm{fit}}\,\mathrm{for}\,\Omega = %$Omega,\,\omega = %$par_omega",
            )

            if Omega == 100
                p2dat["gaspard_"*string(par_omega)] = (a_ar, Rs_gaspard ./ Omega)
                @assert length(a_ar) == length(Rs_gaspard)
                Plots.plot!(
                    p2,
                    a_ar,
                    Rs_gaspard ./ Omega,
                    label = L"R_{\mathrm{gaspard}}\,\mathrm{for}\,\omega = %$par_omega",
                )

                p3dat["gaspard_"*string(par_omega)] = (Rs_gaspard ./ Omega, Rs_theo ./ Omega)
                Plots.plot!(
                    p3,
                    Rs_gaspard ./ Omega,
                    Rs_theo ./ Omega ./ 2,
                    label = L"gaspard\,\,\omega = %$par_omega",
                    seriestype = :scatter,
                )
            end

            # begin # Mitigation for https://github.com/JuliaPlots/Plots.jl/issues/3059
            #     mid = 50
            #     for ar in [a_ar, Rs_fit, Rs_theo]
            #         temp = ar[1]
            #         ar[1] = ar[mid]
            #         ar[mid] = temp
            #     end
            # end


            p3dat[par_omega] = (Rs_fit ./ Omega, Rs_theo ./ Omega)
            Plots.plot!(
                p3,
                Rs_fit ./ Omega,
                Rs_theo ./ Omega ./ 2,
                label = L"\Omega = %$Omega,\,\omega = %$par_omega",
                seriestype = :scatter,
            )
        end
        flag = false
    end

    Plots.savefig(p2, ENV["HOME"] * "/Sync/data/plots/cubic_2.pdf")
    Plots.plot!(p3, [0, 15], [0, 15], label = "equality")
    Plots.savefig(p3, ENV["HOME"] * "/Sync/data/plots/cubic_3.pdf")


    # JSON.print(open("/home/volker/Sync/data/plots/cubic_1.json", "w"), p1dat)
    JSON.print(open(ENV["HOME"] * "/Sync/data/plots/cubic_2.json", "w"), p2dat)
    JSON.print(open(ENV["HOME"] * "/Sync/data/plots/cubic_3.json", "w"), p3dat)



    mu_0 = 0.4
    par_omega_0 = 1
    p = Plots.plot(xlabel = L"t", ylabel = "cor", title = L"\mu = %$mu_0,\,\omega = %$par_omega_0")
    for Omega in [100, 1000, 10000]
        (
            dt,
            Omega,
            avg,
            theo_omega,
            theo_gamma,
            border,
            index_to_time,
            delta_index_to_delta_time,
            delta_time_to_delta_index,
            cor,
            js,
        ) = read_rust_json(
            Printf.@sprintf "%s/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" ENV["HOME"] Omega par_omega_0 mu_0
        )
        wcor = cor[1:4000]
        Plots.plot!(
            p,
            0:dt:(length(wcor)-1)*dt,
            wcor,
            label = L"\Omega = %$Omega",
        )
    end
    Plots.savefig(p, ENV["HOME"] * "/Sync/data/plots/cubic_cor_Omega.pdf")

    Omega_0 = 100
    par_omega_0 = 1
    p = Plots.plot(xlabel = L"t", ylabel = "cor", title = L"\Omega = %$Omega_0,\,\omega = %$par_omega_0")
    for mu in [0.4, 1.0, 4.2]
        (
            dt,
            Omega,
            avg,
            theo_omega,
            theo_gamma,
            border,
            index_to_time,
            delta_index_to_delta_time,
            delta_time_to_delta_index,
            cor,
            js,
        ) = read_rust_json(
            Printf.@sprintf "%s/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" ENV["HOME"] Omega_0 par_omega_0 mu
        )
        wcor = cor[1:4000]
        Plots.plot!(
            p,
            0:dt:(length(wcor)-1)*dt,
            wcor / (-minimum(wcor)),
            label = L"\mu = %$mu",
        )
    end
    Plots.savefig(p, ENV["HOME"] * "/Sync/data/plots/cubic_cor_mu.pdf")

    Omega_0 = 100
    mu_0 = 0.4
    p = Plots.plot(xlabel = L"t", ylabel = "cor", title = L"\Omega = %$Omega_0,\,\mu = %$mu_0")
    for par_omega in [-1, 1, 2]
        (
            dt,
            Omega,
            avg,
            theo_omega,
            theo_gamma,
            border,
            index_to_time,
            delta_index_to_delta_time,
            delta_time_to_delta_index,
            cor,
            js,
        ) = read_rust_json(
            Printf.@sprintf "%s/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" ENV["HOME"] Omega_0 par_omega mu_0
        )
        wcor = cor[1:4000]
        Plots.plot!(
            p,
            0:dt:(length(wcor)-1)*dt,
            wcor / (-minimum(wcor)),
            label = L"\omega = %$par_omega",
        )
    end
    Plots.savefig(p, ENV["HOME"] * "/Sync/data/plots/cubic_cor_par_omega.pdf")
end

function cubic_hopf_C_r()
    # p1dat = Dict()
    p2dat = Dict()
    p3dat = Dict()

    # p = Plots.plot(xlabel=L"t", ylabel="cor")
    # for Omega in [100, 1000, 10000]
    #     Omega = Int(Omega)
    #     for par_omega in [-2, -1, 1, 2, 3]
    #         (
    #             dt,
    #             Omega,
    #             avg,
    #             theo_omega,
    #             theo_gamma,
    #             border,
    #             index_to_time,
    #             delta_index_to_delta_time,
    #             delta_time_to_delta_index,
    #             cor,
    #             js,
    #         ) = read_rust_json(
    #             Printf.@sprintf "/home/volker/Sync/data/from_itp2/cubic_hopf_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega 1
    #         )
    #         wcor = cor[1:1000]
    #         Plots.plot!(
    #             p,
    #             0:dt:(length(wcor) - 1) * dt,
    #             wcor,
    #             label=L"\Omega = %$Omega,\,\omega = %$par_omega",
    #         )
    #         p1dat[par_omega] = (0:dt:(length(wcor) - 1) * dt, wcor)
    #     end
    # end
    # Plots.savefig(p, "/home/volker/Sync/data/plots/cubic_4.pdf")

    p2 = Plots.plot(
        xlabel = L"\mu",
        ylabel = L"R/\Omega",
        legend = :outertopleft,
        size = (800, 400),
    )
    p3 = Plots.plot(
        xlabel = L"R_{\mathrm{fit}}/\Omega",
        ylabel = L"R_{\mathrm{th}}/\Omega/2",
        legend = :outertopleft,
        size = (800, 400),
    )
    flag = true
    # for Omega in [100, 1000, 10000]
    for Omega in [1000]
        Omega = Int(Omega)
        for par_omega in [-2, -1, 1, 2, 3]
            a_ar = collect(0.4:0.2:4.2)
            Rs_fit = []
            Rs_fit_err = []
            Rs_theo = []
            borders = []
            for mu in a_ar
                if par_omega + 1 * mu == 0 # If the force vanishes, we get no oscillations
                    push!(Rs_fit, 0)
                    push!(Rs_fit_err, 0)
                    push!(Rs_theo, 0)
                    push!(borders, 0)
                    continue
                end
                path =
                    Printf.@sprintf "/home/volker/Sync/data/from_itp2/wrong/cubic_hopf_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega mu
                (
                    Omega,
                    theo_omega,
                    omega,
                    theo_gamma,
                    gamma,
                    border,
                    cor_t,
                    cor,
                    js,
                    period_error,
                    gamma_error,
                    R_error,
                ) = from_json(path, cor_plot = false, fit_plot = false, cutoff = false)
                R = omega / gamma
                push!(Rs_fit, R)
                push!(Rs_fit_err, R_error)
                push!(Rs_theo, theo_omega / theo_gamma)
                push!(borders, border)

                if par_omega == 1 && mu == 1
                    @debug omega gamma * Omega

                    C = sqrt(mu)
                    Q = 1 / 2 * mu^2
                    @debug (par_omega + mu) (Q / mu)

                    # p = Plots.plot(cor_t, cor)
                    # display(p)
                end
            end

            # if flag
            #     p2dat["R_th_" * string(par_omega)] = (a_ar, Rs_theo ./ Omega)
            #     Plots.plot!(
            #         p2,
            #         a_ar,
            #         Rs_theo ./ Omega ./ 2,
            #         label=L"R_{\mathrm{th}}/2\,\mathrm{for}\,\Omega = %$Omega,\,\omega = %$par_omega",
            #     )
            # end

            p2dat[par_omega] = (a_ar, Rs_fit ./ Omega, Rs_fit_err ./ Omega)
            Plots.plot!(
                p2,
                a_ar,
                Rs_fit ./ Omega,
                yerror = Rs_fit_err ./ Omega,
                label = L"R_{\mathrm{fit}}\,\mathrm{for}\,\Omega = %$Omega,\,\omega = %$par_omega",
            )


            # begin # Mitigation for https://github.com/JuliaPlots/Plots.jl/issues/3059
            #     mid = 50
            #     for ar in [a_ar, Rs_fit, Rs_theo]
            #         temp = ar[1]
            #         ar[1] = ar[mid]
            #         ar[mid] = temp
            #     end
            # end


            p3dat[par_omega] = (Rs_fit ./ Omega, Rs_theo ./ Omega)
            Plots.plot!(
                p3,
                Rs_fit ./ Omega,
                Rs_theo ./ Omega ./ 2,
                label = L"\Omega = %$Omega,\,\omega = %$par_omega",
                seriestype = :scatter,
            )
        end
        flag = false
    end

    Plots.savefig(p2, "/home/volker/Sync/data/plots/cubic_wrong_2.pdf")
    Plots.plot!(p3, [0, 15], [0, 15], label = "equality")
    Plots.savefig(p3, "/home/volker/Sync/data/plots/cubic_wrong_3.pdf")


    # JSON.print(open("/home/volker/Sync/data/plots/cubic_wrong_2.json", "w"), p2dat)
    # JSON.print(open("/home/volker/Sync/data/plots/cubic_wrong_3.json", "w"), p3dat)

end

function curwork()
    par_omega = 1
    Omega = 1000
    mu = 3
    path =
        Printf.@sprintf "/home/volker/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega mu
    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(path, cor_plot = true, fit_plot = true, cutoff = false)
    println(path)
    @debug "" theo_omega omega theo_gamma gamma
end

function show_fitplot_and_vals(path)
    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(path, cor_plot = false, fit_plot = "temp.pdf", cutoff = 800)

    # Talbot

    # corf = Interpolations.LinearInterpolation(cor_t, cor)
    # ft = InverseLaplace.GWR(s -> corf(s), 16)
    # ft(theo_gamma)
    # p = Plots.plot()
    # Plots.plot!(p, ft)
    # display(p)

    println(path, "\t", theo_omega, "\t", omega, "\t", theo_gamma, "\t", gamma)
end

function hopf_meld()
    Omega = 1e3
    par_omega = 1
    mu = 1
    path = Printf.@sprintf "/home/volker/Sync/data/from_itp2/cubic_hopf_Q_1_Omega_%i_omega_%.2f_mu_%.2f.json" Omega par_omega mu
    # show_fitplot_and_vals(path)


    # show_fitplot_and_vals("/data/weissmann/precious/meld_theta.json")
    show_fitplot_and_vals("/home/volker/Sync/data/from_itp2/meld_long_x.json")


    # show_fitplot_and_vals("/data/weissmann/precious/meld_a_0.5.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_a_1.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_a_2.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_a_3.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_2.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_2_a_2.json")
    # show_fitplot_and_vals("/data/weissmann/precious/meld_3_a_2.json")




    return
    # (
    #     Omega,
    #     theo_omega,
    #     omega,
    #     theo_gamma,
    #     gamma,
    #     border,
    #     cor_t,
    #     cor,
    #     js,
    #     period_error,
    #     gamma_error,
    #     R_error,
    # ) = from_json("/data/weissmann/precious/meld_theta.json", cor_plot=false, fit_plot=true, cutoff=true)
    # @debug theo_gamma gamma

    # return

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/data/weissmann/precious/meld_x.json", cor_plot = false, fit_plot = true, cutoff = true)
    @debug theo_gamma gamma


    return

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/data/weissmann/precious/meld_1d.json", cor_plot = false, fit_plot = true, cutoff = true)

    @debug theo_gamma gamma

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/home/volker/Sync/data/from_itp2/meld_sin_100.json", cor_plot = false, fit_plot = true, cutoff = true)

    @debug theo_gamma gamma

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/home/volker/Sync/data/from_itp2/meld_sin_1000.json", cor_plot = false, fit_plot = true, cutoff = true)

    @debug theo_gamma gamma

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/home/volker/Sync/data/from_itp2/meld_sin_10000.json", cor_plot = false, fit_plot = true, cutoff = true)

    @debug theo_gamma gamma

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/home/volker/Sync/data/from_itp2/omega_x0_0_1000_a_0.00.json", cor_plot = false, fit_plot = true, cutoff = true)

    @debug theo_gamma gamma

    return



    return

    (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    ) = read_rust_json("/data/weissmann/precious/meld_1d.json")
    cor_t = 0:dt:(length(cor)-1)*dt
    p = Plots.plot(cor_t, cor)
    display(p)

    (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    ) = read_rust_json("/data/weissmann/precious/meld_theta.json")
    cor_t = 0:dt:(length(cor)-1)*dt
    # cor_x = sin.(cor)
    p = Plots.plot(cor_t, cor)
    display(p)


    return



    return

    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json("/data/weissmann/precious/meld_x.json", cor_plot = true, fit_plot = false, cutoff = true)
    @debug theo_omega omega
    @debug theo_gamma gamma

    (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    ) = read_rust_json("/data/weissmann/precious/meld_x.json")
    cor_t = 0:dt:(length(cor)-1)*dt
    p = Plots.plot(cor_t, cor)
    display(p)
end

# hopf_meld()
# nothing

# curwork()
cubic_hopf_2()
# cubic_hopf_C_r()

# path =
#     Printf.@sprintf "/home/volker/Sync/data/from_itp2/wrong/cubic_hopf_Omega_%i_omega_%.2f_mu_%.2f.json" 10000 -1 1.4
# (
#     Omega,
#     theo_omega,
#     omega,
#     theo_gamma,
#     gamma,
#     border,
#     cor_t,
#     cor,
#     js,
#     period_error,
#     gamma_error,
#     R_error,
# ) = from_json(path, cor_plot = false, fit_plot = false, cutoff = false)
# @debug "" theo_omega theo_gamma * Omega
#gaspard_robustness_omega(steward_landau_conf(OMEGA = 1, a = 1.4, b = 1.0, c = -1.0, d = 1.0)::gen_brüssel_conf)
#plot_limit_cycle(steward_landau_conf(OMEGA = 1, a = 1.4, b = 1.0, c = -1.0, d = 1.0)::gen_brüssel_conf)
#gaspard_robustness_omega(steward_landau_conf(OMEGA = 1, a = 1.0, b = 1.0, c = 1.0, d = 1.0)::gen_brüssel_conf)
#plot_limit_cycle(steward_landau_conf(OMEGA = 1, a = 1.0, b = 1.0, c = 1.0, d = 1.0)::gen_brüssel_conf)

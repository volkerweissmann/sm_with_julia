ENV["JULIA_DEBUG"] = "all"
using IncludeGuards
@includeonce "anal.jl"
import JSON
using LaTeXStrings
using Printf
using Memoize

# import DifferentialEquations
# using DifferentialEquations.EnsembleAnalysis

# function julia_calc(Omega, rescale)
#     modf(el, i) = mod2pi(el[i])

#     Q(u) = 2.
#     F(u) = 1.2 + sin(u)
#     x0 = 1.3
#     Length = 2 * pi

#     time = 2 * pi * 100 * rescale
#     multi = 2^10

#     f(du, u, p, t) = du .= F.(u) / rescale
#     g(du, u, p, t) = du .= sqrt(2 * Q(u) / Omega) / sqrt(rescale)
#     u0 = [x0]
#     tspan = (0.0, time)
#     prob = DifferentialEquations.SDEProblem(f, g, u0, tspan)

#     # function calc_avg_1()
#     #     sol = DifferentialEquations.solve(prob, DifferentialEquations.EM(), dt=dt)
#     #     avg_1 = sum(Iterators.map(j -> modf.(sol.u, j), 1:multi_1)) / multi_1
#     #     return avg_1
#     # end
#     # avg_2 = sum(Iterators.map(i -> calc_avg_1(), 1:multi_2)) / multi_2

#     ensembleprob = DifferentialEquations.EnsembleProblem(prob)
#     sol = DifferentialEquations.solve(ensembleprob, DifferentialEquations.EnsembleThreads(), trajectories=multi)
#     display("finished ensemble simulation")

#     cor_res = 1e-2
#     cor_t0 = 0.
#     cor_t = cor_t0:cor_res:time

#     index_to_time(i) = cor_t[i]
#     time_to_index(t) = Int(round((t - cor_t0) / cor_res)) + 1
#     delta_time_to_delta_index(t) = Int(round(t / cor_res))
#     delta_index_to_delta_time(i) = i * cor_res

#     cor = collect(map(t -> Statistics.mean(map(x -> mod2pi(x[1]), collect(get_timepoint(sol, t)))), cor_t))
#     cor = (cor - Statistics.mean(cor) * ones(length(cor))) * x0
#     return cor
# end

function get_coeff(js::Dict{String,Any}, key)
    for (k, v) in js["coeffs"]
        if k == key
            return v
        end
    end
    throw(KeyError)
end

function my_integral(Omega, F)
    Length = 2 * pi
    Q(u) = 1.0

    # f(du, u, p, t) = du .= F.(u) * dt
    # g(du, u, p, t) = du .= sqrt(2 * Q(u) / Omega) * sqrt(dt)

    theo_omega = 2 * pi * 1 / quadgk(x -> 1 / F(x), 0, Length)[1]
    theo_gamma =
        1 / Omega *
        (2 * pi)^2 *
        quadgk(x -> 1 / F(x), 0, Length)[1]^-3 *
        quadgk(x -> Q(x) / F(x)^3, 0, Length)[1]

    theo_omega = abs(theo_omega)
    theo_gamma = abs(theo_gamma)
    border = quadgk(x -> F(x) / Q(x), 0, Length)[1] / (2 * pi) * Omega

    return (theo_omega, theo_gamma, border)
end

function get_omega_gamma_border(js, Omega)
    if js["Force"] == "move |x: f64| a.mul_add(x.sin(), 1.)"
        a = get_coeff(js, "a")
        return my_integral(Omega, x -> 1.0 + a * sin(x))
    elseif js["Force"] == "move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]"
        a = get_coeff(js, "a")
        return my_integral(Omega, x -> 1.0 + a * sin(x))
    elseif js["Force"] in ["move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b)", "move |q: [f64; 2]| cubic_hopf_bifurcation_force_just_theta(q, mu, omega, b)"]
        b = get_coeff(js, "b")
        mu = get_coeff(js, "mu")
        par_omega = get_coeff(js, "omega")
        # return theta -> par_omega + b * mu  # just the 1D projection
        Q = 1
        omega = abs(par_omega + b * mu)
        gamma = Q / (Omega * mu)
        return (omega, gamma, omega / gamma)
    elseif js["Force"] == "move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a)"
        a = get_coeff(js, "a")
        b = get_coeff(js, "b")
        mu = get_coeff(js, "mu")
        par_omega = get_coeff(js, "omega")
        Q = 1
        omega = abs(par_omega + b * mu / a)
        gamma = a * Q / (Omega * mu)
        return (omega, gamma, omega / gamma)
    elseif js["Force"] in ["move |q: [f64; 1]| [omega + b]", "move |_: [f64; 1]| [omega + b]"]
        b = get_coeff(js, "b")
        mu = get_coeff(js, "mu")
        par_omega = get_coeff(js, "omega")
        Q = 1
        omega = abs(par_omega + b * mu)
        gamma = Q / (Omega * mu)
        return (omega, gamma, omega / gamma)
    elseif js["Force"] == "move |x: [f64; 1]| [2.]"
        return my_integral(Omega, x -> 2.0)
    elseif js["Force"] in ["move |x: [f64; 1]| [a]", "move |_: [f64; 1]| [a]"]
        a = get_coeff(js, "a")
        return my_integral(Omega, x -> a)
    elseif js["Force"] == "move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b)"
        b = get_coeff(js, "b")
        sigma = get_coeff(js, "sigma")
        par_omega = get_coeff(js, "omega")
        Q = 1
        r0 = sqrt(3) * sigma
        omega = abs(par_omega + b * r0^2)
        gamma = Q / (Omega * r0^2)
        return (omega, gamma, omega / gamma)
    elseif js["Force"] in ["move |q: [f64; 2]| brüssel_force(&bc, q)", "move |x: [f64; 1]| [lookup(&rinp.theta, &rinp.F_theta, x[0])]"]
        return (nothing, nothing, nothing)
    end
    println("Warning: unknown Force: ", js["Force"])
    return (nothing, nothing, nothing)
end

function get_steady_state_avg(js)
    Length = 2 * pi
    if js["Force"] == "move |x: f64| a.mul_add(x.sin(), 1.)"
        a = get_coeff(js, "a")
        return quadgk(x -> x / (1.0 + a * sin(x)), 0, Length)[1] / quadgk(x -> 1 / (1.0 + a * sin(x)), 0, Length)[1]
    elseif js["Force"] == "move |x: [f64; 1]| [a.mul_add(x[0].sin(), 1.)]"
        a = get_coeff(js, "a")
        return quadgk(x -> x / (1.0 + a * sin(x)), 0, Length)[1] / quadgk(x -> 1 / (1.0 + a * sin(x)), 0, Length)[1]
    elseif js["Force"] in ["move |q: [f64; 2]| cubic_hopf_bifurcation_force(q, mu, omega, b)", "move |q: [f64; 2]| cubic_hopf_bifurcation_force_just_theta(q, mu, omega, b)", "move |q: [f64; 2]| cubic_hopf_bifurcation_force_a(q, mu, omega, b, a)", "move |q: [f64; 2]| mexican_hat_force(q, sigma, omega, b)"]
        return 0
    elseif js["Force"] in ["move |q: [f64; 1]| [omega + b]", "move |_: [f64; 1]| [omega + b]", "move |x: [f64; 1]| [2.]", "move |x: [f64; 1]| [a]", "move |_: [f64; 1]| [a]"]
        return pi
    elseif js["Force"] in ["move |q: [f64; 2]| brüssel_force(&bc, q)", "move |x: [f64; 1]| [lookup(&rinp.theta, &rinp.F_theta, x[0])]"]
        return nothing
    end
    println("Warning: unknown Force: ", js["Force"])
    return nothing
end

function read_rust_json(path; special_flip = false, force_shift = nothing)
    js = JSON.parsefile(path)
    if "version" in keys(js)
        version = js["version"]
    else
        version = 1
    end
    dt = js["trajectory_writeout_timestep"]
    omegakey = "Omega"
    if version == 1
        omegakey = "omega"
    end
    Omega = js[omegakey]
    avg = js["avg"]
    if avg == nothing
        error(path * " is invalid")
    end

    (theo_omega, theo_gamma, border) = get_omega_gamma_border(js, Omega)

    index_to_time(i) = (i - 1) * dt
    delta_index_to_delta_time(i) = i * dt
    delta_time_to_delta_index(t) = Int(round(t / dt))

    x0 = 1.3 # todo: x0 is not always 1.3

    theo_avg = get_steady_state_avg(js)
    if force_shift != nothing
        #println("force shift")
        theo_avg = -force_shift
    end
    if special_flip
        avg = 2 * pi * ones(length(avg)) - avg
        theo_avg = 2 * pi - theo_avg
    end
    if any(x -> x == nothing, avg)
        error(path * " has an avg that contains nothing values")
    end
    if theo_avg == nothing
        if force_shift == nothing
            #println("time average")
        end
        cor = (avg - Statistics.mean(avg) * ones(length(avg))) # * x0
    else
        if force_shift == nothing
            #println("theoretical value")
        end
        cor = (avg - theo_avg * ones(length(avg))) # * x0
    end
    return (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    )
end

function fit_cos_exp(path)
    js = JSON.parsefile(path)
    if "version" in keys(js)
        version = js["version"]
    else
        version = 1
    end
    dt = js["trajectory_writeout_timestep"]
    omegakey = "Omega"
    if version == 1
        omegakey = "omega"
    end
    Omega = js[omegakey]
    avg = js["avg"]

    avg = avg

    cor_t = 0:dt:(length(avg)-1)*dt

    @. model(t, p) = p[2] * exp(-t * p[1]) * cos(p[4]t + p[5]) + p[3]
    p0 = [1 / Omega, 1, 0, 0.1, 0]
    fit = LsqFit.curve_fit(model, cor_t, avg, p0)
    @debug LsqFit.coef(fit)

    p = Plots.plot(xlabel = L"t", ylabel = "avg")
    Plots.plot!(p, cor_t, avg, label = "avg")
    #Plots.plot!(p, cor_t, model(cor_t, LsqFit.coef(fit)), label = "fit")
    Plots.savefig(p, "temp.pdf")
end

#@memoize
function from_json(path; cor_plot, fit_plot, cutoff = true, special_flip = false, force_shift = nothing)
    (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    ) = read_rust_json(path, special_flip = special_flip, force_shift = force_shift)

    cor_t = 0:dt:(length(cor)-1)*dt
    if cor_plot != false
        p = Plots.plot(cor_t, cor)
        if cor_plot != true
            if !isdir(dirname(cor_plot))
                println(dirname(cor_plot), " does not already exists, creating it.")
                mkdir(dirname(cor_plot))
            end
            Plots.savefig(p, cor_plot)
        else
            display(p)
        end
    end

    if theo_omega != nothing
        theo_period = 2 * pi / theo_omega
        guessed_period = theo_period
    else
        guessed_period = guess_period(cor_t, cor)
    end

    if guessed_period == nothing || guessed_period == Inf || guessed_period / dt > 1e19 # If guessed_period / dt, Int64 no longer workds
        return (
            Omega,
            theo_omega,
            NaN,
            theo_gamma,
            NaN,
            border,
            cor_t,
            cor,
            js,
            nothing,
            nothing,
            NaN,
        )
    end
    (period, peaks_t, peaks_u, period_error) = robust_find_peaks(
        cor,
        guessed_period,
        0,
        0.5 * maximum(cor),
        true,
        index_to_time,
        delta_time_to_delta_index,
        delta_index_to_delta_time,
    )
    # (_period, _peaks_t, peaks_u_m, _period_error) = robust_find_peaks(
    #     -cor,
    #     guessed_period,
    #     0,
    #     0.5 * maximum(cor),
    #     true,
    #     index_to_time,
    #     delta_time_to_delta_index,
    #     delta_index_to_delta_time,
    # )
    # minlen = min(length(peaks_u_p), length(peaks_u_m))
    # peaks_t = peaks_t[1:minlen]
    # peaks_u = peaks_u_p[1:minlen] + peaks_u_m[1:minlen]

    omega = 2 * pi / period

    # See https://github.com/JuliaNLSolvers/LsqFit.jl
    @. model(t, p) = p[2] * exp(-t * p[1]) # + p[3] * exp(-t * p[1] * 2)
    p0 = [1 / Omega, 1]
    # @. model(t, p) = p[2] * exp(-t * p[1]) + p[3]
    # p0 = [1e-3, 1, 0]

    if cutoff == true
        # skipper = min(max(Int(round(log(1.4) / theo_gamma / theo_period)), 1), Int(round(0.7 * length(peaks_t))))
        skipper = min(
            max(Int(round(log(2.0) / theo_gamma / theo_period)), 1),
            Int(round(0.7 * length(peaks_t))),
        )
    elseif cutoff == false
        skipper = 1
    elseif cutoff isa Number
        skipper = findfirst(t -> t > cutoff, peaks_t)
    elseif cutoff[1] == "ratio"
        skipper = findfirst(u -> u < peaks_u[1] * cutoff[2], peaks_u)
    else
        throw(ValueError)
    end
    if skipper == nothing
        return (
            Omega,
            theo_omega,
            NaN,
            theo_gamma,
            NaN,
            border,
            cor_t,
            cor,
            js,
            nothing,
            nothing,
            NaN,
        )
    end
    @assert skipper isa Number
    @assert !isnan(skipper)
    @assert skipper < Inf
    if length(peaks_t) - skipper + 1 < 2
        return (Omega, theo_omega, NaN, theo_gamma, NaN, border, cor_t, cor, js, nothing, nothing, NaN)
    end
    # right_skipper_t = min(
    #     Int(round(log(20.0) / theo_gamma)),
    #     length(peaks_t),
    # )
    # right_skipper = findfirst(t -> t > right_skipper_t, peaks_t)
    # fit = LsqFit.curve_fit(model, peaks_t[skipper:right_skipper], peaks_u[skipper:right_skipper], p0)
    fit = LsqFit.curve_fit(model, peaks_t[skipper:end], peaks_u[skipper:end], p0)
    gamma_error = nothing
    try
        gamma_error = LsqFit.margin_error(fit, 0.05)[1]
    catch err
        isa(err, ArgumentError) # || rethrow(err)
        println("warning: gamma_error could not be determined")
        gamma_error = NaN
    end
    gamma = LsqFit.coef(fit)[1]

    # @debug "" omega  theo_omega
    # @debug "" gamma  theo_gamma

    # @debug "" abs(1 - omega / theo_omega)
    # @debug "" abs(1 - gamma / theo_gamma)

    if fit_plot != false
        p = Plots.plot(xlabel = L"t", ylabel = "cor")
        # Plots.plot!(p, 0:dt:(length(cor) - 1) * dt, cor, label="C(t)")
        Plots.plot!(p, peaks_t, peaks_u, label = "peaks")
        # Plots.plot!(p, peaks_t[skipper:end], peaks_u[skipper:end], label="peaks")
        Plots.plot!(p, peaks_t, model(peaks_t, LsqFit.coef(fit)), label = "fit")

        # Plots.plot!(p, peaks_t, model(peaks_t, [theo_gamma, peaks_u[1]]), label="theo")

        # Plots.savefig(p, "plots/cor/omega_" * string(Omega) * ".pdf")
        if skipper != 1
            Plots.vline!(p, [peaks_t[skipper]], label = "cutoff")
        end
        #Plots.vline!(p, right_skipper_t, label = "right cutoff")
        if fit_plot == true
            display(p)
        else
            if !isdir(dirname(fit_plot))
                println(dirname(fit_plot), " does not already exists, creating it.")
                mkdir(dirname(fit_plot))
            end
            Plots.savefig(p, fit_plot)
        end
    end

    # R = omega/gamma = 2 pi /(period * gamma)
    @assert period_error >= 0 || isnan(period_error)
    @assert gamma_error >= 0 || isnan(gamma_error)
    omega_error = 2 * pi / period^2 * period_error
    R_error = omega/gamma * sqrt((omega_error / omega)^2 + (gamma_error / gamma)^2)
    return (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    )
end

function traversing()
    filelist = readdir("rust/from_hydra/")

    res = []
    for el in filelist
        if isfile("rust/from_hydra/" * el)
            push!(res, from_json("rust/from_hydra/" * el, false, savefitplot = true))
        end
    end

    res = sort(collect(res), by = x -> x[1])
    Omegas = []
    theo_omegas = []
    omegas = []
    theo_gammas = []
    gammas = []
    for el in res
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error = el
        # println("Omega = ", Omega)
        # println("integral omega = ", theo_omega)
        # println("langevin fit omega = ", omega)
        # println("integral gamma = ", theo_gamma)
        # println("langevin fit gamma = ", gamma)
        # println("")
        push!(Omegas, Omega)
        push!(theo_omegas, theo_omega)
        push!(omegas, omega)
        push!(theo_gammas, theo_gamma)
        push!(gammas, gamma)
    end

    p = Plots.plot(xlabel = L"\Omega", ylabel = L"1/\gamma")
    Plots.plot!(p, Omegas, 1 ./ gammas, label = "Langevin fit")
    Plots.plot!(p, Omegas, 1 ./ theo_gammas, label = "Theoretical")
    Plots.savefig(p, "plots/gamma.pdf")

    p = Plots.plot(xlabel = L"\Omega", ylabel = L"\omega")
    Plots.plot!(p, Omegas, omegas, label = "Langevin fit")
    Plots.plot!(p, Omegas, theo_omegas, label = "Theoretical")
    Plots.savefig(p, "plots/omega.pdf")

    p = Plots.plot(xlabel = L"\Omega", ylabel = L"R")
    Plots.plot!(p, Omegas, omegas ./ gammas, label = "Langevin fit")
    Plots.plot!(p, Omegas, theo_omegas ./ theo_gammas, label = "Theoretical")
    Plots.savefig(p, "plots/R.pdf")
end

function basile_plot_old()
    # traversing()
    for µ in [3.5, 3.8, 3.9, 4.0]
        from_json(
            "/data/weissmann/precious/basile_mu_" * string(µ) * ".json",
            "/data/weissmann/plots/basile_mu_" * string(µ) * ".pdf",
            true,
            savefitplot = false,
        )
    end
end

function cubic_hopf()
    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(
        "/data/weissmann/precious/cubic_hopf.json",
        cor_plot = false,
        fit_plot = "/home/volker/Sync/data/plots/cubic_hopf.pdf",
        cutoff = false,
    )

    p = Plots.plot(xlabel = L"t", ylabel = "cor")
    Plots.plot!(p, cor_t, cor, label = "1234567890")
    Plots.savefig(p, "/home/volker/Sync/data/plots/cubic_hopf_cor.pdf")

    @debug Omega theo_omega omega theo_gamma gamma border
end

# brüssel_plots()
# (Omega, theo_omega, omega, theo_gamma, gamma, border, cor_t, cor, js, period_error, gamma_error, R_error) = from_json("/home/volker/Sync/data/from_itp2/omega_1000_a_0.00.json", cor_plot=true, fit_plot=true, cutoff=true)

# @debug theo_omega / theo_gamma
# @debug omega / gamma

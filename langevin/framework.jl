using IncludeGuards
@includeonce "from_json.jl"
@includeonce "../Brüsselator/data_types.jl"
ENV["JULIA_DEBUG"] = "all"

function get_coeff(fs::FrameState, key)
    for (k, v) in fs.coeffs
        if k == key
            return v
        end
    end
    throw(KeyError)
end

function rust_json_to_frame(path; cor_plot = false, special_flip = false)
    (
        dt,
        Omega,
        avg,
        theo_omega,
        theo_gamma,
        border,
        index_to_time,
        delta_index_to_delta_time,
        delta_time_to_delta_index,
        cor,
        js,
    ) = read_rust_json(path, special_flip = special_flip)

    cor_t = 0:dt:(length(cor)-1)*dt
    if cor_plot != false
        p = Plots.plot(cor_t, cor)
        if cor_plot != true
            Plots.savefig(p, cor_plot)
        else
            display(p)
        end
    end

    if theo_omega != nothing
        theo_period = 2 * pi / theo_omega
        guessed_period = theo_period
    else
        guessed_period = guess_period(cor_t, cor)
    end
    @assert guessed_period != nothing

    (period, peaks_t, peaks_u, period_error) = robust_find_peaks(
        cor,
        guessed_period,
        0,
        0.5,
        true,
        index_to_time,
        delta_time_to_delta_index,
        delta_index_to_delta_time,
    )
    omega = 2 * pi / period

    return FrameState(coeffs = js["coeffs"], size = Omega, theo_omega = theo_omega, theo_gamma = theo_gamma, fit_omega = omega, fit_gamma = nothing, cor_t = cor_t, cor = cor, peaks_t = peaks_t, peaks_u = peaks_u)
end

function myfit(xdat, ydat, model, p0, plot; skipper_left = 1, skipper_right = nothing)
    @assert length(xdat) == length(ydat)
    if skipper_right == nothing
        skipper_right = length(xdat)
    end
    fit = LsqFit.curve_fit(model, xdat[skipper_left:skipper_right], ydat[skipper_left:skipper_right], p0)
    fit_error = nothing
    try
        fit_error = LsqFit.margin_error(fit, 0.05)[1]
    catch err
        println("warning: fit_error could not be determined")
        fit_error = NaN
    end
    coeffs = LsqFit.coef(fit)

    if plot != false
        p = Plots.plot()
        Plots.plot!(p, xdat, ydat, label = "data")
        Plots.plot!(p, xdat[skipper_left:skipper_right], model(xdat[skipper_left:skipper_right], coeffs), label = "fit")
        if skipper_left != 1
            Plots.vline!(p, [xdat[skipper_left]], label = "cutoff")
        end
        if skipper_right != length(xdat)
            Plots.vline!(p, [xdat[skipper_right]], label = "cutoff")
        end
        if plot == true
            display(p)
        else
            Plots.savefig(p, plot)
        end
    end

    return (coeffs, fit_error)
end

function get_raw_avg(path)
    js = JSON.parsefile(path)
    dt = js["trajectory_writeout_timestep"]
    cor = js["avg"]
    # cor_t = 0:dt:(length(cor) - 1) * dt
    return cor, dt
end

function plot_cor_func(path)
    js = JSON.parsefile(path)
    dt = js["trajectory_writeout_timestep"]
    cor = js["avg"]
    cor_t = 0:dt:(length(cor)-1)*dt

    p = Plots.plot()
    Plots.plot!(p, cor_t, cor)
    return p
end

function new_plot(args...; kwargs...)
    PlotData(plot = Plots.plot(args...; kwargs...), data = Dict())
end

function plot_line!(pd::PlotData, x, y, args...; kwargs...)
    @assert length(x) == length(y)
    Plots.plot!(pd.plot, x, y, args...; kwargs...)
    @assert !haskey(pd.data, String(kwargs[:label]))
    if haskey(kwargs, :yerror)
        pd.data[String(kwargs[:label])] = (x, y, kwargs[:yerror])
    else
        pd.data[String(kwargs[:label])] = (x, y)
    end
end

function save_plot(pd::PlotData, path)
    Plots.savefig(pd.plot, path * ".pdf")
    of = open(path * ".json", "w")
    JSON.print(of, pd.data)
    close(of)
end

using IncludeGuards
@includeonce "from_json.jl"
@includeonce "framework.jl"
@includeonce "../Brüsselator/attract.jl"

function main()
    for prefix in ["mail_10_12_x_x0", "mail_10_12_x", "mexican_hat_x"]
        parname = if prefix != "mexican_hat_x"
            "b"
        else
            "sigma"
        end
        # for prefix in ["mail_10_12_x"]

        sep = ";\t"
        #println("par" * sep * "paromega" * sep * "theo_omega" * sep * "theo_gamma" * sep * "fit_omega" * sep * "fit_gamma" * sep * "gamma_error" * sep * "period_error" * sep * "R_error")

        p1 = new_plot(xlabel = L"R_\mathrm{theo}", ylabel = L"R_\mathrm{fit}", legend = :bottomright)
        p2 = new_plot(xlabel = L"\omega", ylabel = L"R_\mathrm{fit}", legend = :bottomright, ylim = (0, 10000))
        b_ar = if prefix != "mexican_hat_x"
            [1e-3, 1e-2, 1e-1, 1, 2, 0]
        else
            [1e-3, 1e-2, 1e-1, 1, 2]
            # [1e-1, 1, 2]
        end
        for b in b_ar
            paromega_ar = collect(-3.0:0.1:2.9)
            Rs_theo = []
            Rs_fit = []
            Rs_error = []
            Rs_gaspard = []

            for paromega in paromega_ar
                bstr = if b == 0
                    "0"
                elseif b == 1
                    "1"
                elseif b == 2
                    "2"
                else
                    string(b)
                end
                path = "/home/volker/Sync/data/from_itp2/" * prefix * "_" * parname * "_" * bstr * "_paromega_" * string(paromega) * ".json"
                plot_path = "/home/volker/Sync/data/plots/" * prefix * "/" * parname * "_" * bstr * "_paromega_" * string(paromega) * ".pdf"
                (
                    Omega,
                    theo_omega,
                    omega,
                    theo_gamma,
                    gamma,
                    border,
                    cor_t,
                    cor,
                    js,
                    period_error,
                    gamma_error,
                    R_error,
                ) = from_json(path, cor_plot = false, fit_plot = plot_path, cutoff = false)
                # println(b, sep, paromega, sep, theo_omega, sep, theo_gamma, sep, omega, sep, gamma, sep, gamma_error, sep, period_error, sep, R_error)
                push!(Rs_theo, theo_omega / theo_gamma)
                push!(Rs_fit, omega / gamma)
                push!(Rs_error, R_error)

                if prefix != "mexican_hat_x"
                    R = NaN
                    if (paromega, b) == (0.0, 0.001)
                        @debug "test"
                    end
                    Rfit = omega / gamma / Omega
                    if -b != paromega && (paromega, b) != (0.0, 0.001)
                        (R, omega, gamma) = gaspard_robustness_omega(steward_landau_conf(OMEGA = Omega, a = 1.0, b = 1.0, c = paromega, d = b)::gen_brüssel_conf)
                    end
                    if abs(1 - R / Rfit) > 0.1
                        println(Rfit, "\t", R / Rfit, "\t", paromega, "\t", b)
                    end
                    push!(Rs_gaspard, R * Omega)
                end
            end
            # yerror=Rs_error,
            if prefix != "mexican_hat_x"
                plot_line!(p1, Rs_theo, Rs_gaspard, label = "gaspard " * parname * "=" * string(b))
                plot_line!(p2, paromega_ar, Rs_gaspard, label = "gaspard " * parname * "=" * string(b))
            end
            plot_line!(p1, Rs_theo, Rs_fit, yerror = Rs_error, label = parname * "=" * string(b))
            plot_line!(p2, paromega_ar, Rs_fit, yerror = Rs_error, label = "fit " * parname * "=" * string(b))
            plot_line!(p2, paromega_ar, Rs_theo, label = "theo " * parname * "=" * string(b))
        end
        # Plots.plot!(p1.plot, xlim=(0, 1e4), ylim=(0, 1e4))
        # Plots.plot!(p2.plot, ylim=(0, 1e4))
        if prefix != "mexican_hat_x"
            plot_line!(p1, [0, 3000], [0, 3000], label = "ident")
        else
            plot_line!(p1, [0, 40000], [0, 40000], label = "ident")
        end
        println("/home/volker/Sync/data/plots/" * prefix * "_post_radial_1")
        save_plot(p1, "/home/volker/Sync/data/plots/" * prefix * "_post_radial_1")
        save_plot(p2, "/home/volker/Sync/data/plots/" * prefix * "_post_radial_2")
    end
end

function merge_mexican_hat_factor()
    # display(plot_cor_func("/home/volker/Sync/data/from_itp2/verify_mexican_hat_r.json"))
    # return

    # b = 0
    # paromega = 2.9
    # prefix = "mexican_hat"

    # bstr = if b == 0
    #     "0"
    # else
    #     string(b)
    # end
    path = ENV["HOME"] * "/Sync/data/from_itp2/meld_sombrero_4.json"
    (
        Omega,
        theo_omega,
        omega,
        theo_gamma,
        gamma,
        border,
        cor_t,
        cor,
        js,
        period_error,
        gamma_error,
        R_error,
    ) = from_json(path, cor_plot = false, fit_plot = true, cutoff = false)
    @debug "" theo_omega omega theo_gamma gamma
end


function mail_19_01()
    p1 = new_plot(xlabel = L"R_\mathrm{theo}", ylabel = L"R_\mathrm{fit}", legend = :bottomright)
    p2 = new_plot(xlabel = L"\mu", ylabel = L"R_\mathrm{fit}", legend = :bottomright, ylim = (0, 10000))
    b_ar = [1e-3, 1e-2, 1e-1, 1, 2, 0]
    for b in b_ar
        mu_ar = collect(0.0:0.1:2.0)
        Rs_theo = []
        Rs_fit = []
        Rs_error = []
        Rs_gaspard = []

        for mu in mu_ar
            paromega = 1.0
            bstr = if b == 0
                "0"
            elseif b == 1
                "1"
            elseif b == 2
                "2"
            else
                string(b)
            end
            path = ENV["HOME"] * "/Sync/data/from_itp2/mail_19_01_x_x0_b_" * bstr * "_mu_" * string(mu) * ".json"
            plot_path = ENV["HOME"] * "/Sync/data/plots/mail_19_01_x_x0_b_" * bstr * "_mu_" * string(mu) * ".pdf"
            (
                Omega,
                theo_omega,
                omega,
                theo_gamma,
                gamma,
                border,
                cor_t,
                cor,
                js,
                period_error,
                gamma_error,
                R_error,
            ) = from_json(path, cor_plot = false, fit_plot = plot_path, cutoff = false)
            push!(Rs_theo, theo_omega / theo_gamma)
            push!(Rs_fit, omega / gamma)
            push!(Rs_error, R_error)

            gaspard_R = NaN
            Rfit = omega / gamma / Omega
            if -b != paromega && mu != 0
                println(mu, "\t", b)
                (gaspard_R, gaspard_omega, gaspard_gamma) = gaspard_robustness_omega(steward_landau_conf(OMEGA = Omega, a = mu, b = 1.0, c = paromega, d = b)::gen_brüssel_conf)
            end
            # if abs(1 - gaspard_R / Rfit) > 0.1
            #     println(Rfit, "\t", gaspard_R / Rfit, "\t", paromega, "\t", b)
            # end

            @debug omega / gamma / Omega gaspard_R

            push!(Rs_gaspard, gaspard_R * Omega)
        end

        plot_line!(p1, Rs_theo, Rs_gaspard, label = "gaspard b =" * string(b))
        plot_line!(p2, mu_ar, Rs_gaspard, label = "gaspard b =" * string(b))

        plot_line!(p1, Rs_theo, Rs_fit, yerror = Rs_error, label = "b =" * string(b))
        plot_line!(p2, mu_ar, Rs_fit, yerror = Rs_error, label = "fit b =" * string(b))
        plot_line!(p2, mu_ar, Rs_theo, label = "theo b =" * string(b))
    end
    # Plots.plot!(p1.plot, xlim=(0, 1e4), ylim=(0, 1e4))
    # Plots.plot!(p2.plot, ylim=(0, 1e4))
    plot_line!(p1, [0, 3000], [0, 3000], label = "ident")
    save_plot(p1, ENV["HOME"] * "/Sync/data/plots/mail_19_01_1")
    save_plot(p2, ENV["HOME"] * "/Sync/data/plots/mail_19_01_2")
end

#main()
mail_19_01()

function temp_theo(mu, a, par_omega, b)
    Omega = 1

    Q = 1
    omega = abs(par_omega + b * mu / a)
    gamma = a * Q / (Omega * mu)
    return (omega / gamma, omega, gamma)
end

# @debug inner_gaspard_robustness_omega(steward_landau_conf(OMEGA = 1, a = 2.0, b = 1.0, c = 1.0, d = 0.0)::gen_brüssel_conf)
# @debug inner_gaspard_robustness_omega(steward_landau_conf(OMEGA = 1, a = 20.0, b = 10.0, c = 10.0, d = 0.0)::gen_brüssel_conf)
# @debug temp_theo(2.0, 1.0, 1.0, 0.0)
# @debug temp_theo(20.0, 10.0, 10.0, 0.0)

#inner_gaspard_robustness_omega(steward_landau_conf(OMEGA = 1, a = 0.1, b = 1.0, c = 1.0, d = 0.0)::gen_brüssel_conf)

# merge_mexican_hat_factor()

# todo: q[0] instead of q[1].atan2(q[0])

#gaspard_robustness_omega(steward_landau_conf(OMEGA = Omega, a = 2.0, b = 1.0, c = 1.0, d = 0.0)::gen_brüssel_conf)

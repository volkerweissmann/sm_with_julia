using IncludeGuards
@includeonce "framework.jl"
@includeonce "../Brüsselator/utils.jl"

function curwork()
    fs = rust_json_to_frame("/home/volker/Sync/data/from_itp2/meld_long_x.json")
    peaks_ln = log.(fs.peaks_u)

    # See https://github.com/JuliaNLSolvers/LsqFit.jl
    # @. model(t, p) = p[2] * exp(-t * p[1])
    # p0 = [1 / fs.size, 1]
    # myfit(fs.peaks_t, fs.peaks_u, model, p0, true, skipper_left=1, skipper_right=nothing)

    @. model(t, p) = p[2] + (-t * p[1])
    p0 = [1 / fs.size, 1]
    myfit(fs.peaks_t, peaks_ln, model, p0, true, skipper_left=400, skipper_right=800)
end

# Comparison with (36) in robustness_v4.pdf
function compare_36()
    fs = rust_json_to_frame("/home/volker/Sync/data/from_itp2/meld_long_x.json")

    p = Plots.plot()
    right = 50000
    pt = fs.cor_t[1:right]
    Plots.plot!(p, pt, fs.cor[1:right], label="langevin")

    mu = get_coeff(fs, "mu")
    omega_0 = get_coeff(fs, "omega") + get_coeff(fs, "b") * mu

    c36(t) = cos(omega_0 * t + 0) * exp(- t / (fs.size * mu))
    # cor_theo = cos.(omega_0 * pt) .* exp.(- pt / (fs.size * mu))
    cor_theo = c36.(pt)
    Plots.plot!(p, pt, cor_theo, label="(36) robustness_v4")

    display(p)
    return


    p35(theta, t) = 1 / sqrt(4 * pi * t * 1 * 1 / (fs.size * mu)) * exp(-1 / 2 * (theta - omega_0 * t - 0)^2 / (2 * t * 1 * 1 / (fs.size * mu)))

    t1 = 100
    @assert approx(quadgk(theta -> p35(theta, t1), t1 * omega_0 - 10, t1 * omega_0 + 10)[1], 1, 1e-6)
    @assert approx(quadgk(theta -> theta^2 * p35(theta, t1), t1 * omega_0 - 10, t1 * omega_0 + 10)[1] - (quadgk(theta -> theta * p35(theta, t1), t1 * omega_0 - 10, t1 * omega_0 + 10)[1])^2, 2 * t1 / fs.size, 1e-6)

    @assert approx(quadgk(theta -> cos(theta) * p35(theta, t1), t1 * omega_0 - 10, t1 * omega_0 + 10)[1], c36(t1), 1e-6)
end

function nan_to_zero(x)
    if isnan(x)
        return 0
    else
        return x
    end
end

function compare_theta_35()
    fs = rust_json_to_frame("/home/volker/Sync/data/from_itp2/meld_long_theta.json")

    p = Plots.plot()
    right = 5000
    pt = fs.cor_t[1:right]
    Plots.plot!(p, pt, fs.cor[1:right], label="langevin")

    mu = get_coeff(fs, "mu")
    omega_0 = get_coeff(fs, "omega") + get_coeff(fs, "b") * mu

    p35(theta, t) = 1 / sqrt(4 * pi * t * 1 * 1 / (fs.size * mu)) * exp(-1 / 2 * (theta - omega_0 * t - 0)^2 / (2 * t * 1 * 1 / (fs.size * mu)))

    t1 = 100

    @assert approx(quadgk(theta -> p35(theta, t1), t1 * omega_0 - 10, t1 * omega_0 + 10)[1], 1, 1e-6)

    cor_theo = map(t -> quadgk(theta -> (rem(theta + pi, 2 * pi) - pi) * nan_to_zero(p35(theta, t)), t * omega_0 - 10, t * omega_0 + 10)[1], pt)



    Plots.plot!(p, pt, cor_theo, label="(35) robustness_v4")

    display(p)
end

function verify_1()
    # Free Diffusion: p(x,t) = ... e^(-x^2/(4Dt))

    #  -> <x^2> = 2Dt

    js = JSON.parsefile("/data/weissmann/precious/verify_1.json")
    dt = js["trajectory_writeout_timestep"]
    cor = js["avg"]
    cor_t = 0:dt:(length(cor) - 1) * dt

    p = Plots.plot()
    Plots.plot!(p, cor_t, cor)
    Plots.plot!(p, cor_t, 2 * 1 / js["Omega"] * cor_t)
    display(p)
end

function theta_writout()
    fs = rust_json_to_frame("/home/volker/Sync/data/from_itp2/meld_theta.json")
    peaks_ln = log.(fs.peaks_u)

    @. model(t, p) = p[2] + (-t * p[1])
    p0 = [1 / fs.size, 1]
    myfit(fs.peaks_t, peaks_ln, model, p0, true, skipper_left=200, skipper_right=nothing)

    # @. model(t, p) = p[2] * exp(-t * p[1])
    # p0 = [1 / fs.size, 1]
    # myfit(fs.peaks_t, fs.peaks_u, model, p0, true, skipper_left=1, skipper_right=nothing)
end

function verify_2()
    js = JSON.parsefile("/data/weissmann/precious/temp_theta_avg.json")
    dt = js["trajectory_writeout_timestep"]
    cor = js["avg"]
    cor_t = 0:dt:(length(cor) - 1) * dt

    p = Plots.plot()
    Plots.plot!(p, cor_t[1:400], cor[1:400])
    # Plots.plot!(p, cor_t, 2 * 1 / js["Omega"] * cor_t)
    display(p)
end

function check_free_diffusion()
    # display(plot_cor_func("/data/weissmann/precious/free_theta_-1.2_var.json"))
    # display(plot_cor_func("/data/weissmann/precious/free_theta_-2_var.json"))
    # display(plot_cor_func("/data/weissmann/precious/free_theta_2.3_var.json"))
end

function hack_back()
    dirname = "/home/volker/Sync/data/from_itp2"

    for el in readdir(dirname)
        m = match(r"back_paromega_(-)?[0-9].[0-9]_b_[0-9].[0-9].json", el)
        if m == nothing
            continue
        end
        path = dirname * "/" * el

        js = JSON.parsefile(path)
        dt = js["trajectory_writeout_timestep"]
        cor = js["avg"]
        cor_t = 0:dt:(length(cor) - 1) * dt

        skipper = findfirst(c -> c > 0.5, cor)

        outpath = "/home/volker/Sync/data/plots/back_paromega_" * string(get_coeff(js, "omega")) * "_b_" * string(get_coeff(js, "b")) * ".pdf"

        @. model(t, p) = p[1] * t
        p0 = [2e-3]
        (coeffs, fit_error) = myfit(cor_t, cor, model, p0, outpath, skipper_left=1, skipper_right=skipper)

        println(el, "\t", coeffs[1] / 0.002 - 1)
    end
end

function hack_back_2()
    dirname = "/home/volker/Sync/data/from_itp2"

    for prefix in ["back", "line"]
        println(prefix)
        println("omega\tb\ta")
        for el in readdir(dirname)
            m = match(prefix * r"_paromega_(-)?[0-9].[0-9]_b_[0-9].[0-9]_a_(-)?[0-9]?[0-9].[0-9].json", el)
            if m == nothing
                continue
            end
            path = dirname * "/" * el

            js = JSON.parsefile(path)
            dt = js["trajectory_writeout_timestep"]
            cor = js["avg"]
            cor_t = 0:dt:(length(cor) - 1) * dt

            skipper = findfirst(c -> c > 0.2, cor)

            outpath = "/home/volker/Sync/data/plots/" * prefix * "/" * prefix * "_paromega_" * string(get_coeff(js, "omega")) * "_b_" * string(get_coeff(js, "b")) * "_a_" * string(get_coeff(js, "a")) * ".pdf"

            @. model(t, p) = p[1] * t
            p0 = [2e-3]
            (coeffs, fit_error) = myfit(cor_t, cor, model, p0, outpath, skipper_left=1, skipper_right=skipper)

            additional_diffusion = coeffs[1] / 0.002 - 1
            println(get_coeff(js, "omega"), "\t", get_coeff(js, "b"), "\t", get_coeff(js, "a"), "\t", additional_diffusion)
        end
    end
end

# hack_back()
hack_back_2()
# curwork()
# compare_36()
# compare_theta_35()
# verify_1()
# plot_cor_func("/home/volker/Sync/data/from_itp2/meld_r.json")
# plot_cor_func("/home/volker/Sync/data/from_itp2/verify_free_theta_avg.json")
# plot_cor_func("/home/volker/Sync/data/from_itp2/verify_free_theta_var.json")

# plot_cor_func("/data/weissmann/precious/temp_free_theta_var.json")

# plot_cor_func("/data/weissmann/precious/back_paromega_0.0_b_3.0.json")

# plot_cor_func("/data/weissmann/precious/temp_back_theta_var.json")

# verify_2()
# theta_writout()
# check_free_diffusion()

# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_theta.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_theta^2.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_r.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_r^2.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_(r-1)^2.json"))

# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_htr_theta.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_htr_theta^2.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_htr_r.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_htr_(r-1)^2.json"))

# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_xdif.json"))
# display(plot_cor_func("/home/volker/Sync/data/from_itp2/3m3_ydif.json"))
